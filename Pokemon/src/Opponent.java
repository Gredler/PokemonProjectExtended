import java.util.ArrayList;
import java.util.Random;

public class Opponent 
{
	private ArrayList<Integer> stats = new ArrayList<>(6);
	private Element type1, type2;
	private String name;
	private int level;
	private ArrayList<MoveList> moves = new ArrayList<>(4);
	
	public void setOpponent()
	{
		name = "Pikachu";
		type1 = Element.ELECTRIC;
		type2 = Element.NONE;
		
		level = 100;
		
		stats.add(211);
		stats.add(146);
		stats.add(116);
		stats.add(136);
		stats.add(136);
		stats.add(216);
		
		moves.add(MoveList.Thunderbolt);
		moves.add(MoveList.IronTail);
		moves.add(MoveList.Tackle);
		moves.add(MoveList.Thunder);
	}
	
	public String getName()
	{
		return name;
	}
	public int getLvl()
	{
		return level;
	}
	public int getHP()
	{
		return stats.get(0);
	}
	public double getAttack()
	{
		return stats.get(1);
	}
	public double getDef()
	{
		return stats.get(2);
	}
	public double getSpAttack()
	{
		return stats.get(3);
	}
	public double getSpDef()
	{
		return stats.get(4);
	}
	public int getSpeed()
	{
		return stats.get(5);
	}
	public Element getElement1()
	{
		return type1;
	}
	public Element getElement2()
	{
		return type2;
	}
	public MoveList chooseMove(Element enemy)
	{
		Random rnd = new Random();
		
		int chooseMove = rnd.nextInt(moves.size());
		
		MoveList chosenMove = moves.get(chooseMove);
		
		return chosenMove;
	}
}
