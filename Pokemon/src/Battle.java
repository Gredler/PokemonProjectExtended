import java.util.Scanner;

public class Battle
{
	Scanner input = new Scanner(System.in);
	private Opponent opponent = new Opponent();
	
	public void combat(StarterPokemon starter, Player player)
	{	
		int opHP;
		int plHP;
		boolean run = false;
		int plSPE = starter.getSpeed();
		
		opponent.setOpponent();
		int opSPE = opponent.getSpeed();
		
		System.out.println("Testbattle");
		System.out.println("==========");
		
		if (opponent.getName().contentEquals("Pikachu")) pikachuASCII();
		
		System.out.println("A wild " + opponent.getName() + " appeared!");
		
		opHP = opponent.getHP();
		
		System.out.println(player.getName() + " sent out " + starter.getName() + "!\n");
		plHP = starter.getHP();
		
		do
		{
			System.out.println("What will you do? ");
			System.out.print("	[A]ttack");
			System.out.println("	[B]ag");
			System.out.print("	[S]witch");
			System.out.println("	[R]un\n");
			
			String text = input.next();
			
			
			if (text.contentEquals("a") || text.contentEquals("A"))
			{	
				int moveChoice = 4;
				starter.showMoves();
				try
				{
					moveChoice = input.nextInt();
				}
				catch (Exception exc)
				{
					exc.getMessage();
				}
				if (moveChoice == 0 || moveChoice == 1 || moveChoice == 2 || moveChoice == 3)
				{
					if (opSPE > plSPE)
					{
						plHP = oppAttack(plHP, starter);
						if (plHP > 0) opHP = playerAttack(opHP, starter, moveChoice);
					}
					else
					{
						opHP = playerAttack(opHP, starter, moveChoice);
						if (opHP > 0) plHP = oppAttack(plHP, starter);
					}
				}
			}
			
			else if (text.contentEquals("b") || text.contentEquals("B")) 
			{
				player.showBag();
				String item = input.next();
				
				plHP = useItem(player, plHP, starter, opponent, item);
				
				oppAttack(plHP, starter);
			}
			
			else if (text.contentEquals("r") || text.contentEquals("R")) 
			{
				run = true;
				System.out.println("You got away safely!");
			}
			else if (text.contentEquals("s") || text.contentEquals("S"))
			{
				System.out.println("Sorry! Switching has not been implemented yet!\n");
			}
		}
		while (opHP > 0 && plHP > 0 && run == false);
	}
	
	public int playerAttack(int opHP, StarterPokemon starter, int moveChoice)
	{
		double damageCalc;
		boolean crit = false;
		int move = starter.getMoveDamage(moveChoice);
		double m = (Math.random());
		System.out.println(starter.getName() + " used " + starter.getMoveName(moveChoice) + "!");
		if (m > starter.getMoveAccuracy(moveChoice)) System.out.println("The attack missed!\n");
		else
		{
			int r = (int) (Math.random() * (100 - 85)) + 85;
			double rnd = r / 100.0;
			
			if (starter.getMoveType(moveChoice).contentEquals("physical"))
			{
				damageCalc = (((2.0 * starter.getLvl() + 10.0)/250.0) * (starter.getAttack() / opponent.getDef()) * move + 2.0) * rnd;
			}
			else
			{
				damageCalc = (((2.0 * starter.getLvl() + 10.0)/250.0) * (starter.getSpAttack() / opponent.getSpDef()) * move + 2.0) * rnd;
			}
			
			double moveTypeMultiplier = getMoveTypeMultiplier(starter.getMoveElement(moveChoice), opponent.getElement1(), opponent.getElement2());
			
			if (moveTypeMultiplier == 4.0)
			{
				damageCalc *= moveTypeMultiplier;
				System.out.println("It's super effective!");
			}
			else if (moveTypeMultiplier == 2.0)
			{
				damageCalc *= moveTypeMultiplier;
				System.out.println("It's super effective!");
			}
			else if (moveTypeMultiplier == 0.5)
			{
				damageCalc *= moveTypeMultiplier;
				System.out.println("It's not very effective!");
			}
			else if (moveTypeMultiplier == 0.25)
			{
				damageCalc *= moveTypeMultiplier;
				System.out.println("It's not very effective!");
			}
			else if (moveTypeMultiplier == 0)
			{
				damageCalc *= moveTypeMultiplier;
				System.out.println("The attack had no effect!");
			}
			else
				damageCalc *= moveTypeMultiplier;
		
			if (starter.getElement1() == starter.getMoveElement(moveChoice) || starter.getElement2() == starter.getMoveElement(moveChoice)) damageCalc *= 1.5;
			
			if (Math.random() < 0.065) crit = true;
			
			if (crit == true) 
			{
				damageCalc *= 1.5; 
				System.out.println("A critical hit!");
			}
			
			int damage = (int) damageCalc;
			
			opHP -= damage;
			
			System.out.println("The opposing " + opponent.getName() + " took " + damage + " damage!");
			
			if (opHP > 0) System.out.println("The opposing " + opponent.getName() + " has " + opHP + " HP left!\n");
			else System.out.println("The opposing " + opponent.getName() + " fainted!");
		}
		return opHP;
	}
	
	public int oppAttack(int plHP, StarterPokemon starter)
	{
		MoveList move;
		double damageCalc;
		boolean crit = false;
		
		move = opponent.chooseMove(starter.getElement1());
		
		double m = (Math.random());
		
		System.out.println("The opposing " + opponent.getName() + " used " + move.name + "!");
		if (m > move.accuracy) System.out.println("The attack missed!");
		else
		{
			int r = (int) (Math.random() * (100 - 85)) + 85;
			double rnd = r / 100.0;
			
			if (move.moveType == "physical")
			{
				damageCalc = (((2.0 * opponent.getLvl() + 10.0)/250.0) * (opponent.getAttack() / starter.getDef()) * move.damage + 2.0) * rnd;
			}
			else
			{
				damageCalc = (((2.0 * opponent.getLvl() + 10.0)/250.0) * (opponent.getSpAttack() / starter.getSpDef()) * move.damage + 2.0) * rnd;
			}
				
			double moveTypeMultiplier = getMoveTypeMultiplier(move.element, starter.getElement1(), starter.getElement2());
			
			if (moveTypeMultiplier == 4.0)
			{
				damageCalc *= moveTypeMultiplier;
				System.out.println("It's super effective!");
			}
			else if (moveTypeMultiplier == 2.0)
			{
				damageCalc *= moveTypeMultiplier;
				System.out.println("It's super effective!");
			}
			else if (moveTypeMultiplier == 0.5)
			{
				damageCalc *= moveTypeMultiplier;
				System.out.println("It's not very effective!");
			}
			else if (moveTypeMultiplier == 0.25)
			{
				damageCalc *= moveTypeMultiplier;
				System.out.println("It's not very effective!");
			}
			else if (moveTypeMultiplier == 0)
			{
				damageCalc *= moveTypeMultiplier;
				System.out.println("The attack had no effect!");
			}
			else
				damageCalc *= moveTypeMultiplier;
		
			if (move.element == opponent.getElement1() || move.element == opponent.getElement1()) damageCalc *= 1.5;
			
			if (Math.random() < 0.065) crit = true;
			
			if (crit == true) 
			{
				damageCalc *= 1.5; 
				System.out.println("A critical hit!");
			}
			
			int damage = (int) damageCalc;
			
			plHP -= damage;
			
			
			System.out.println(starter.getName() + " took " + damage + " damage!");
			
			if (plHP > 0) System.out.println(starter.getName() + " has " + plHP + " HP left!\n");
			else System.out.println(starter.getName() + " fainted!");
		}
		return plHP;
	}
	
	public static int useItem(Player player, int plHP, StarterPokemon starter, Opponent opponent, String item)
	{
		int itemNumber = Integer.parseInt(item);
		Items chosenItem = player.getItem(itemNumber);
		
		if (chosenItem.name.contentEquals("Full Restore"))
		{
			plHP = starter.getHP();
			System.out.println(starter.getName() + " was fully healed!");
			player.removeItem(itemNumber);
		}
		else
		{
			if ((plHP + chosenItem.heal) <= starter.getHP())
			{
				plHP += chosenItem.heal;
				System.out.println(starter.getName() + " was healed by " + chosenItem.heal + " HP!\n");
				player.removeItem(itemNumber);
			}
			else
			{
				int heal = starter.getHP() - plHP;
				plHP += heal;
				System.out.println(starter.getName() + " was healed by " + heal + " HP!\n");
				player.removeItem(itemNumber);
			}
		}
		
		return plHP;
	}
	
	public double getMoveTypeMultiplier(Element attack, Element enemy1, Element enemy2)
	{
		if (attack.isSuperEffective(attack, enemy1) && attack.isSuperEffective(attack, enemy2)) return 4.0;
		else if ((attack.isSuperEffective(attack, enemy1) && enemy2 == Element.NONE) 
				|| (attack.isSuperEffective(attack, enemy1) && (attack.isNormalEffective(attack, enemy2)) 
				|| (attack.isNormalEffective(attack, enemy1) && (attack.isSuperEffective(attack, enemy2))))) return 2.0;
		else if ((attack.isNormalEffective(attack, enemy1) && enemy2 == Element.NONE) 
				|| (attack.isNormalEffective(attack, enemy1) && (attack.isNormalEffective(attack, enemy2))) 
				|| (attack.isSuperEffective(attack, enemy1) && attack.isResistant(attack, enemy2))
				|| (attack.isResistant(attack, enemy1) && (attack.isSuperEffective(attack, enemy2)))) return 1.0;
		else if ((attack.isResistant(attack, enemy1) && enemy2 == Element.NONE)
				|| (attack.isResistant(attack, enemy1) && (attack.isNormalEffective(attack, enemy2)))
				|| (attack.isNormalEffective(attack, enemy1) && (attack.isResistant(attack, enemy2)))) return 0.5;
		else if (attack.isResistant(attack, enemy1) && attack.isResistant(attack, enemy2)) return 0.25;
		else return 0;
	}
	
	public void pikachuASCII()
	{
		System.out.println("quu..__");
		System.out.println(" $$$b  `---.__");
		System.out.println("  $$b        `--.                          ___.---uuudP");
		System.out.println("   `$$b           `.__.------.__     __.---'      $$$$             .");
		System.out.println("     $b          -'            `-.-'            $$$              .'|");
		System.out.println("       .                                       d$             _.'  |");
		System.out.println("         `.   /                              ...             .'     |");
		System.out.println("           `./                           ..::-'            _.'       |");
		System.out.println("            /                         .:::-'            .-'         .'");
		System.out.println("           :                          ::''\\          _.'            |");
		System.out.println("          .' .-.             .-.           `.      .'               |");
		System.out.println("          : /'$$|           .@$\\           `.   .'              _.-'");
		System.out.println("         .'|$u$$|          |$$,$$|           |  <            _.-'");
		System.out.println("         | `:$$:'          :$$$$$:           `.  `.       .-'");
		System.out.println("         :                  `--'             |    `-.     \\");
		System.out.println("        :##.       ==             .###.       `.      `.    `\\");
		System.out.println("        |##:                      :###:        |        >     >");
		System.out.println("        |#'     `..'`..'          `###'        x:      /     /");
		System.out.println("         \\                                   xXX|     /    ./");
		System.out.println("          \\                                xXXX'|    /   ./");
		System.out.println("          /`-.                                  `.  /   /");
		System.out.println("         :    `-  ...........,                   | /  .'");
		System.out.println("         |         ``:::::::'       .            |<    `.");
		System.out.println("         |             ```          |           x| \\ `.:``.");
		System.out.println("         |                         .'    /'   xXX|  `:`M`M':.");
		System.out.println("         |    |                    ;    /:' xXXX'|  -'MMMMM:'");
		System.out.println("         `.  .'                   :    /:'       |-'MMMM.-'");
		System.out.println("          |  |                   .'   /'        .'MMM.-'");
		System.out.println("          `'`'                   :  ,'          |MMM<");
		System.out.println("            |                     `'            |tbap\\");
		System.out.println("             \\                                  :MM.-'");
		System.out.println("              \\                 |              .''");
		System.out.println("               \\.               `.            /");
		System.out.println("                /     .:::::::.. :           /");
		System.out.println("               |     .:::::::::::`.         /");
		System.out.println("               |   .:::------------\\       /");
		System.out.println("              /   .''               >::'  /");
		System.out.println("              `',:                 :    .'");
		System.out.println("                                   `:.:' ");
	}
}
