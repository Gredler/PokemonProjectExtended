import java.util.ArrayList;

public class Player 
{
	private String name;
	private String gender;
	private ArrayList<Items> bag = new ArrayList<>();
	
	public Player(String name, String gender)
	{
		this.name = name;
		this.gender = gender;
		
		bag.add(Items.Potion);
		bag.add(Items.SuperPotion);
		bag.add(Items.UltraPotion);
		bag.add(Items.HyperPotion);
		bag.add(Items.FullRestore);
	}

	public String getName() 
	{
		return name;
	}

	public void setName(String name) 
	{
		this.name = name;
	}

	public String getGender() 
	{
		return gender;
	}

	public void setGender(String gender) 
	{
		this.gender = gender;
	}
	
	public void showBag()
	{
		System.out.println("Bag:");
		for (int i = 0; i < bag.size(); i++)
		{
			System.out.println("	[" + i + "]" + bag.get(i).name);
		}
		System.out.println("Exit");
	}
	
	public Items getItem(int item)
	{
		return bag.get(item);
	}
	
	public void removeItem(int item)
	{
		bag.remove(item);
	}
}
