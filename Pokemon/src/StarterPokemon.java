import java.util.ArrayList;
import java.util.Random;

public class StarterPokemon 
{
	private ArrayList<Integer> base = new ArrayList<>(6);
	private ArrayList<Integer> iv = new ArrayList<>(6);
	private ArrayList<Integer> ev = new ArrayList<>(6);
	private int level;
	private ArrayList<Integer> stats = new ArrayList<>(6);
	private String nickname;
	private String heldItem;
	private String ability;
	private Element type1, type2;
	private ArrayList<MoveList> moves = new ArrayList<>(4);
	private boolean shininess;
	private String gender;
	Nature nature;
	
	public void chooseSquirtle()
	{
		nickname = "Squirtle";
		heldItem = "none";
		type1 = Element.WATER;
		type2 = Element.NONE;
		
		Random rnd = new Random();
		int a = rnd.nextInt(2);
		if(a == 0) ability = "Torrent";
		else ability = "Rain Dish";
		
		double g = rnd.nextDouble()*100;
		if (g > 12.5) gender = "male";
		else gender = "female";
		
		level = 100;
		
		base.add(44);
		base.add(48);
		base.add(65);
		base.add(50);
		base.add(64);
		base.add(43);
		
		newPokemon(level);
		
		moves.add(MoveList.HydroPump);
		moves.add(MoveList.Earthquake);
		moves.add(MoveList.WaterGun);
		moves.add(MoveList.Tackle);
	}
	
	public void chooseCharmander()
	{
		nickname = "Charmander";
		heldItem = "none";
		type1 = Element.FIRE;
		type2 = Element.NONE;
		
		Random rnd = new Random();
		int a = rnd.nextInt(2);
		if(a == 0) ability = "Blaze";
		else ability = "Solar Power";
		
		double g = rnd.nextDouble()*100;
		if (g > 12.5) gender = "male";
		else gender = "female";
		
		level = 100;
		
		base.add(39);
		base.add(52);
		base.add(43);
		base.add(60);
		base.add(50);
		base.add(65);
		
		newPokemon(level);
		
		moves.add(MoveList.Earthquake);
		moves.add(MoveList.FireBlast);
		moves.add(MoveList.Scratch);
		moves.add(MoveList.MetalClaw);
	}
	
	public void chooseBulbasaur()
	{
		
		nickname = "Bulbasaur";
		heldItem = "none";
		type1 = Element.GRASS;
		type2 = Element.POISON;

		Random rnd = new Random();
		int a = rnd.nextInt(2);
		if(a == 0) ability = "Overgrow";
		else ability = "Chlorophyll";
		
		double g = rnd.nextDouble()*100;
		if (g > 12.5) gender = "male";
		else gender = "female";
		
		level = 100;
		
		base.add(45);
		base.add(49);
		base.add(49);
		base.add(65);
		base.add(65);
		base.add(45);
		
		newPokemon(level);
		
		moves.add(MoveList.SeedBomb);
		moves.add(MoveList.Earthquake);
		moves.add(MoveList.RazorLeaf);
		moves.add(MoveList.Tackle);
	}
	
	public void setNickname(String nickname)
	{
		this.nickname = nickname;
	}
	
	private void newPokemon(int level)
	{
		this.level = level;
		
		for (int i = 0; i < 6; i++)
		{
			ev.add(0);
		}
		
		Random rnd = new Random();
		iv.add(rnd.nextInt(32));		
		iv.add(rnd.nextInt(32));		
		iv.add(rnd.nextInt(32));		
		iv.add(rnd.nextInt(32));		
		iv.add(rnd.nextInt(32));		
		iv.add(rnd.nextInt(32));
		
		generateNature();
		
		setStats();
		
		calculateNature();
		
		shininess = setShininess();
	}
	
	private void setStats()
	{
		
		double x;
		x = ((2 * base.get(0) + iv.get(0) + (ev.get(0)/4)) * level)/100 + level + 10;
		stats.add((int)x);
		
		for (int i = 1; i < 6; i++)
		{
			x = (((2 * base.get(i) + iv.get(i) + ev.get(i) / 4) * level) / 100) + 5;
			stats.add((int)x);
		}
	}
	
	private void generateNature()
	{
		Random rnd = new Random();
		int n = rnd.nextInt(24);
		switch(n)
		{
		case 0: this.nature = Nature.Hardy;
		break;
		case 1: this.nature = Nature.Lonely;
		break;
		case 2: this.nature = Nature.Brave;
		break;
		case 3: this.nature = Nature.Adamant;
		break;
		case 4: this.nature = Nature.Naughty;
		break;
		case 5: this.nature = Nature.Bold;
		break;
		case 6: this.nature = Nature.Docile;
		break;
		case 7: this.nature = Nature.Relaxed;
		break;
		case 8: this.nature = Nature.Impish;
		break;
		case 9: this.nature = Nature.Lax;
		break;
		case 10: this.nature = Nature.Timid;
		break;
		case 11: this.nature = Nature.Hasty;
		break;
		case 12: this.nature = Nature.Serious;
		break;
		case 13: this.nature = Nature.Jolly;
		break;
		case 14: this.nature = Nature.Naive;
		break;
		case 15: this.nature = Nature.Modest;
		break;
		case 16: this.nature = Nature.Mild;
		break;
		case 17: this.nature = Nature.Quiet;
		break;
		case 18: this.nature = Nature.Bashful;
		break;
		case 19: this.nature = Nature.Rash;
		break;
		case 20: this.nature = Nature.Calm;
		break;
		case 21: this.nature = Nature.Gentle;
		break;
		case 22: this.nature = Nature.Sassy;
		break;
		case 23: this.nature = Nature.Careful;
		break;
		case 24: this.nature = Nature.Quirky;
		break;
		default: this.nature = Nature.Serious;
		}
	}
	
	private void calculateNature()
	{		
		switch(this.nature)
		{
		case Lonely: stats.set(1, positiveNature(1));
			stats.set(2, negativeNature(2));
			break;
		case Brave: stats.set(1, positiveNature(1));
			stats.set(5, negativeNature(5));
			break;
		case Adamant: stats.set(1, positiveNature(1));
			stats.set(3, negativeNature(3));
			break;
		case Naughty: stats.set(1, positiveNature(1));
			stats.set(4, negativeNature(4));
			break;
		case Bold: stats.set(2, positiveNature(2));
			stats.set(1, negativeNature(1));
			break;
		case Relaxed: stats.set(2, positiveNature(2));
			stats.set(5, negativeNature(5));
			break;
		case Impish: stats.set(2, positiveNature(2));
			stats.set(3, negativeNature(3));
			break;
		case Lax: stats.set(2, positiveNature(2));
			stats.set(4, negativeNature(4));
			break;
		case Timid: stats.set(5, positiveNature(5));
			stats.set(1, negativeNature(1));
			break;
		case Hasty: stats.set(5, positiveNature(5));
			stats.set(2, negativeNature(2));
			break;
		case Jolly: stats.set(5, positiveNature(5));
			stats.set(3, negativeNature(3));
			break;
		case Naive: stats.set(5, positiveNature(5));
			stats.set(4, negativeNature(4));
			break;
		case Modest: stats.set(3, positiveNature(3));
			stats.set(1, negativeNature(1));
			break;
		case Mild: stats.set(3, positiveNature(3));
			stats.set(2, negativeNature(2));
			break;
		case Quiet: stats.set(3, positiveNature(3));
			stats.set(5, negativeNature(5));
			break;
		case Rash: stats.set(3, positiveNature(3));
			stats.set(4, negativeNature(4));
			break;
		case Calm: stats.set(4, positiveNature(4));
			stats.set(1, negativeNature(1));
			break;
		case Gentle: stats.set(4, positiveNature(4));
			stats.set(2, negativeNature(2));
			break;
		case Sassy: stats.set(4, positiveNature(4));
			stats.set(5, negativeNature(5));
			break;
		case Careful: stats.set(4, positiveNature(4));
			stats.set(3, negativeNature(3));
			break;
		default: 
		}
	}
	
	private int positiveNature(int i)
	{
		double x = ((((2 * base.get(i) + iv.get(i) + ev.get(i) / 4) * level) / 100) + 5) * 1.1;
		return (int)x;
	}
	
	private int negativeNature(int i)
	{
		double x = ((((2 * base.get(i) + iv.get(i) + ev.get(i) / 4) * level) / 100) + 5) * 0.9;
		return (int)x;
	}
	
	private boolean setShininess()
	{
		Random rnd = new Random();
		int r = rnd.nextInt(700) + 1;
		if (r == 1) return true;
		else return false;
	}
	
	public void getStats()
	{
		System.out.println("\n==========================");
		System.out.println(nickname + "(" + gender + ")" + " @ " + heldItem);
		if (shininess == true) System.out.println("Shiny: yes");
		System.out.println("Ability: " + ability);
		System.out.println(nature + " Nature");
		System.out.println("HP: 		" + stats.get(0) + " (" + ev.get(0) + "/" + iv.get(0) + ")");
		System.out.println("Attack: 	" + stats.get(1) + " (" + ev.get(1) + "/" + iv.get(1) + ")");
		System.out.println("Defence: 	" + stats.get(2) + " (" + ev.get(2) + "/" + iv.get(2) + ")");
		System.out.println("Sp. Attack: 	" + stats.get(3) + " (" + ev.get(3) + "/" + iv.get(3) + ")");
		System.out.println("Sp. Defence:	" + stats.get(4) + " (" + ev.get(4) + "/" + iv.get(4) + ")");
		System.out.println("Speed: 		" + stats.get(5) + " (" + ev.get(5) + "/" + iv.get(5) + ")");
		System.out.println("==========================\n");
	}
	public int getLvl()
	{
		return level;
	}
	public int getHP()
	{
		return stats.get(0);
	}
	public double getAttack()
	{
		return stats.get(1);
	}
	public double getDef()
	{
		return stats.get(2);
	}
	public double getSpAttack()
	{
		return stats.get(3);
	}
	public double getSpDef()
	{
		return stats.get(4);
	}
	public int getSpeed()
	{
		return stats.get(5);
	}
	public String getName()
	{
		return nickname;
	}
	public void showMoves()
	{
		for (int i = 0; i < moves.size(); i++)
		{
			System.out.println("	[" + i + "] " + moves.get(i));
		}
	}
	public int getMoveDamage(int index)
	{
		return moves.get(index).damage;
	}
	public String getMoveName(int index)
	{
		return moves.get(index).name;
	}
	public double getMoveAccuracy(int index)
	{
		return moves.get(index).accuracy;
	}
	public String getMoveType(int index)
	{
		return moves.get(index).moveType;
	}
	public Element getMoveElement(int index)
	{
		return moves.get(index).element;
	}
	public Element getElement1()
	{
		return type1;
	}
	public Element getElement2()
	{
		return type2;
	}
}
