
public enum MoveList 
{
	Tackle("Tackle", 50, Element.NORMAL, 1, 35, "physical"),
	Scratch("Scratch", 40, Element.NORMAL, 1, 35, "physical"),
	Ember("Ember", 40, Element.FIRE, 1, 25, "special"),
	WaterGun("Water Gun", 40, Element.WATER, 1, 25, "special"),
	RazorLeaf("Razor Leaf", 55, Element.GRASS, 0.95, 25, "physical"),
	Earthquake("Earthquake", 100, Element.GROUND, 1, 10, "physical"),
	MetalClaw("Metal Claw", 55, Element.STEEL, 0.95, 35, "physical"),
	FireBlast("Fire Blast", 110, Element.FIRE, 0.85, 5, "special"),
	Thunderbolt("Thunderbolt", 90, Element.ELECTRIC, 1, 15, "special"),
	IronTail("Iron Tail", 100, Element.STEEL, 0.75, 15, "physical"),
	HydroPump("Hydro Pump", 110, Element.WATER, 0.8, 5, "special"),
	SeedBomb("Seed Bomb", 80, Element.GRASS, 1, 15, "physical"),
	PoisonJab("Poison Jab", 80, Element.POISON, 1, 20, "physical"),
	Thunder("Thunder", 110, Element.ELECTRIC, 0.7, 10, "special");
	
	
    public String name;
	int damage;
	Element element;
	double accuracy;
	int pp;
	String moveType;

    MoveList(String name, int damage, Element element, double accuracy, int pp, String moveType) 
    {
        this.name = name;
        this.damage = damage;
        this.element = element;
        this.accuracy = accuracy;
        this.pp = pp;
        this.moveType = moveType;
    }
    
    public int getDamage(MoveList move)
    {
    	return move.damage;
    }
}
