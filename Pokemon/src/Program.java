import java.util.Scanner;

public class Program 
{
	public static void main(String[] args) 
	{	
		Player player = new Player(null, null);
		player = intro();
		StarterPokemon starter = new StarterPokemon();
		starter = starterChoice();
		
		Scanner input = new Scanner(System.in);
		
		System.out.print("Would you like to have a Testbattle right now? ");
		String questions = input.nextLine();
		if (questions.contentEquals("yes"))
		{		
			Battle battle = new Battle();
			battle.combat(starter, player);
		}
		else
		{
			do
			{
				System.out.print("Are you sure? ");
				questions = input.nextLine();
			}
			while (!questions.contains("yes") && !questions.contentEquals("no") && !questions.contentEquals("Yes") && !questions.contentEquals("No"));
			
			if (questions.contentEquals("yes"))
				System.out.println("Aww that's too bad!");
			else
			{
				System.out.println("Awesome! Enjoy your battle!");
				
				Battle battle = new Battle();
				battle.combat(starter, player);
			}
		}
		
		input.close();
	}
	
	/**
	 * intro that triggers at the Start of the game. Lets the player choose their gender, name and starter. 
	 */
	public static Player intro()
	{
		logoASCII();
		
		Player player = new Player(null, null);
		@SuppressWarnings("resource")
		Scanner input = new Scanner(System.in);
		System.out.println("Hello and welcome to the World of Pokemon! \nNow let's get your registration out of the way. \nFirst I'll need you to tell me your gender. \n");
		
		do
		{ 
			System.out.print("Are you a boy or a girl? ");
			player.setGender(input.nextLine());
		}
		while (!player.getGender().contentEquals("boy") && !player.getGender().contentEquals("girl") && !player.getGender().contentEquals("neither"));
		if (player.getGender().contentEquals("boy") || player.getGender().contentEquals("girl")) System.out.println("So you're a " + player.getGender() + "!");
		else System.out.println("That's Great!");
		
		System.out.print("So what's your name? ");
		player.setName(input.nextLine()); ;

		System.out.println("Good now we have finished your Registration.\n");
		
		return player;		
	}	
	
	public static StarterPokemon starterChoice()
	{
		StarterPokemon starter = new StarterPokemon();
		String questions = "";
		@SuppressWarnings("resource")
		Scanner input = new Scanner(System.in);
		
		do
		{
			System.out.println("Which Starter Pokemon would you like to choose?");
			System.out.print("Choices: Bulbasaur / Charmander / Squirtle: ");		
			questions = input.nextLine();
			if (!questions.contentEquals("Bulbasaur") && !questions.contentEquals("Charmander") && !questions.contentEquals("Squirtle"))
					System.out.println("Come on! I don't have all day.\n");
		}
		while(!questions.contentEquals("Bulbasaur") && !questions.contentEquals("Charmander") && !questions.contentEquals("Squirtle"));
		
		if (questions.contentEquals("Bulbasaur")) starter.chooseBulbasaur();
		else if (questions.contentEquals("Charmander")) starter.chooseCharmander();
		else starter.chooseSquirtle();
		
		do
		{
			System.out.print("Would you like to give your Pokemon a Nickname? ");
			questions = input.nextLine();
		}
		while (!questions.contentEquals("yes") && !questions.contentEquals("no"));
		
		if (questions.contentEquals("yes")) 
		{
			System.out.print("How will you name your Pokemon? ");
			String nick = input.nextLine();
			starter.setNickname(nick);
		}
		
		System.out.print("Would you like to look at your Pokemon's stats? ");
		questions = input.nextLine();
		if (questions.contentEquals("yes")) starter.getStats();
		return starter;
	}
	
	public static void logoASCII()
	{
		System.out.println("                                  ,'\\");
		System.out.println("    _.----.        ____         ,'  _\\   ___    ___     ____");
		System.out.println("_,-'       `.     |    |  /`.   \\,-'    |   \\  /   |   |    \\  |`.");
		System.out.println("\\      __    \\    '-.  | /   `.  ___    |    \\/    |   '-.   \\ |  |");
		System.out.println(" \\.    \\ \\   |  __  |  |/    ,','_  `.  |          | __  |    \\|  |");
		System.out.println("   \\    \\/   /,' _`.|      ,' / / / /   |          ,' _`.|     |  |");
		System.out.println("    \\     ,-'/  /   \\    ,'   | \\/ / ,`.|         /  /   \\  |     |");
		System.out.println("     \\    \\ |   \\_/  |   `-.  \\    `'  /|  |    ||   \\_/  | |\\    |");
		System.out.println("      \\    \\ \\      /       `-.`.___,-' |  |\\  /| \\      /  | |   |");
		System.out.println("       \\    \\ `.__,'|  |`-._    `|      |__| \\/ |  `.__,'|  | |   |");
		System.out.println("        \\_.-'       |__|    `-._ |              '-.|     '-.| |   |");
		System.out.println("                                `'                            '-._|\n");

	}
}
