
public enum Items 
{
    Potion("Potion", 20),
    SuperPotion("Super Potion", 60),
    UltraPotion("Ultra Potion", 120),
    HyperPotion("Hyper Potion", 200),
    FullRestore("Full Restore", 999);

    public String name;
    public int heal;

    Items(String name, int heal) 
    {
        this.name = name;
        this.heal = heal;
    }
}
