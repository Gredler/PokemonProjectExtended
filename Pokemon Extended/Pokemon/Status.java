/**
 * Enum for all the possible status effects a Pokemon can have.
 * @author Andi
 * @version 2.1
 */
public enum Status 
{
	NORMAL("Normal"),
	BURNED("Burned"),
	PARALYSIS("Paralysis"),
	SLEEP("Sleep"),
	FROZEN("Frozen"),
	POISON("Poison"),
	TOXIC("Toxic"),
	FAINTED("Fainted"),
	CONFUSION("Confusion"),
	REST("Rest");
	
	
	private final String name;

    Status(String name) 
    {
        this.name = name;
    }
    
    public String getName()
    {
    	return this.name;
    }
}
