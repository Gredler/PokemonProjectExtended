/**
 * Enum for all possible gender rates a Pokemon can have.
 * @author Andi
 * @version 1.1
 */
public enum GenderRate 
{
	ALWAYS_MALE("AlwaysMale"),
	FEMALE_ONE_EIGHTH("FemaleOneEighth"),
	FEMALE_25_PERCENT("Female25Percent"),
	FEMALE_50_PERCENT("Female50Percent"),
	FEMALE_75_PERCENT("Female75Percent"),
	ALWAYS_FEMALE("AlwaysFemale"),
	GENDERLESS("Genderless");
	
	private final String name;
	
	GenderRate(String name)
	{
		this.name = name;
	}
	
	public String getName()
	{
		return this.name;
	}
}
