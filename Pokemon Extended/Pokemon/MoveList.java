/**
 * Enum for all the implemented moves in this game.
 * @author Andi 
 * @version 5.2
 */
public enum MoveList 
{
	TACKLE("Tackle", 50, Element.NORMAL, 1, 35, "physical", 0, MovesExtra.NONE, true, "A physical attack in which the user charges and slams into the target with its body."),
	SCRATCH("Scratch", 40, Element.NORMAL, 1, 35, "physical", 0, MovesExtra.NONE, true, "Hard, pointed, and sharp claws rake the target to inflict damage."),
	EMBER("Ember", 40, Element.FIRE, 1, 25, "special", 0, MovesExtra.BURN_10_PERCENT, true, "The target is attacked with small flames. It may also burn the target."),
	WATER_GUN("Water Gun", 40, Element.WATER, 1, 25, "special", 0, MovesExtra.NONE, true, "The target is blasted with a forceful shot of water."),
	RAZOR_LEAF("Razor Leaf", 55, Element.GRASS, 0.95, 25, "physical", 0, MovesExtra.NONE, true, "Sharp-edged leaves launch at the opposing team. Critical hits land more easily."),
	EARTHQUAKE("Earthquake", 100, Element.GROUND, 1, 10, "physical", 0, MovesExtra.NONE, true, "The user sets off an earthquake that strikes every Pokemon around it."),
	METAL_CLAW("Metal Claw", 55, Element.STEEL, 0.95, 35, "physical", 0, MovesExtra.ATTACK_UP_10_PERCENT_ATT, true, "The target is raked with steel claws. It may also raise the user's Attack stat."),
	FIRE_BLAST("Fire Blast", 110, Element.FIRE, 0.85, 5, "special", 0, MovesExtra.BURN_30_PERCENT, true, "The target is attacked with an intense blast of fire. It may burn the target."),
	THUNDERBOLT("Thunderbolt", 90, Element.ELECTRIC, 1, 15, "special", 0, MovesExtra.PARALYSIS_10_PERCENT, true, "A strong electric blast is loosed at the target. It may paralyse the target."),
	IRON_TAIL("Iron Tail", 100, Element.STEEL, 0.75, 15, "physical", 0, MovesExtra.DEF_DOWN_30_PERCENT_OP, true, "The target is slammed with a steel-hard tail. It may also lower the target's Defense stat."),
	HYDDRO_PUMP("Hydro Pump", 110, Element.WATER, 0.8, 5, "special", 0, MovesExtra.NONE, true, "The target is blasted by a huge volume of water launched under great pressure."),
	SEED_BOMB("Seed Bomb", 80, Element.GRASS, 1, 15, "physical", 0, MovesExtra.NONE, true, "The user slams a barrage of hard-shelled seeds down on the target from above."),
	POISON_JAB("Poison Jab", 80, Element.POISON, 1, 20, "physical", 0, MovesExtra.POISON_30_PERCENT, true, "The target is jabbed with a tentacle or arm covered in poison. It may poison the target."),
	THUNDER("Thunder", 110, Element.ELECTRIC, 0.7, 10, "special", 0, MovesExtra.PARALYSIS_10_PERCENT, true, "A wicked thunderbolt is dropped on the target. It may paralyse the target."),
	DRAGON_RAGE("Dragon Rage", 0, Element.DRAGON, 1, 10, "special", 0, MovesExtra.FLAT_DAMAGE_40, true, "This attack hits with a shock wave of pure rage. This attack always inflicts 40 damage."),
	FIRE_FANG("Fire Fang", 65, Element.FIRE, 0.95, 15, "physical", 0, MovesExtra.FIRE_FANG, true, "The user bites with flaming fangs. It may also burn the target or make it flinch."),
	FLAME_BURST("Flame Burst", 70, Element.FIRE, 1, 15, "special", 0, MovesExtra.NONE, true, "The user attacks the target with big flames. The fire damages adjacent Pokemon also."),
	SLASH("Slash", 70, Element.NORMAL, 1, 15, "physical", 0, MovesExtra.HIGH_CRIT, true, "The target is attacked with a slash. Critical hits land more easily."),
	FLAMETHROWER("Flamethrower", 90, Element.FIRE, 1, 15, "special", 0, MovesExtra.BURN_10_PERCENT, true, "The target is scorched with an intense blast of fire. It may burn the target."),
	FIRE_SPIN("Fire Spin", 35, Element.FIRE, 0.85, 15, "special", 0, MovesExtra.FIRE_SPIN, true, "The target is trapped within a fierce vortex of fire that rages for four to five turns."),
	INFERNO("Inferno", 100, Element.FIRE, 0.5, 5, "special", 0, MovesExtra.BURN_100_PERCENT, true, "The user engulfs the target in intense fire. It burns the target."),
	VINE_WHIP("Vine Whip", 45, Element.GRASS, 1, 25, "physical", 0, MovesExtra.NONE, true, "The target is struck with slender, whiplike vines to inflict damage."),
	POISON_POWDER("Poison Powder", 0, Element.POISON, 0.75, 15, "status", 0, MovesExtra.POISON_100_PERCENT, true, "The user scatters a cloud of poisonous dust on the target. It may poison the target."),
	SLEEP_POWDER("Sleep Powder", 0, Element.GRASS, 0.75, 15, "status", 0, MovesExtra.SLEEP_100_PERCENT, true, "The user scatters a big cloud of sleep-inducing dust around the target."),
	TAKE_DOWN("Take Down", 90, Element.NORMAL, 0.85, 20,"physical", 0, MovesExtra.RECOIL_25_PERCENT, true, "A reckless charge into the target. It also damages the user a little."),
	DOUBLE_EDGE("Double Edge", 120, Element.NORMAL, 1, 15, "physical", 0, MovesExtra.RECOIL_25_PERCENT, true, "A reckless, life-risking tackle. It also damages the user."),
	SYNTHESIS("Synthesis", 0, Element.GRASS, 100, 5, "status", 0, MovesExtra.SUN_HEAL, false, "The user restores its own HP. The amount of HP regained varies with the weather."),
	BUBBLE("Bubble", 40, Element.WATER, 1, 30, "special", 0, MovesExtra.SPEED_DOWN_10_PERCENT_OP, true, "A spray of countless bubbles is jetted at the opposing team. It may also lower the targets' Speed stats."),
	BITE("Bite", 60, Element.DARK, 1, 25, "physical", 0, MovesExtra.FLINCH_30_PERCENT, true, "The target is bitten with viciously sharp fangs. It may make the target flinch."),
	RAPID_SPIN("Rapid Spin", 20, Element.NORMAL, 1, 40, "physical", 0, MovesExtra.REOVE_HAZARDS, true, "A spin attack that can also eliminate such moves as Bind, Wrap, Leech Seed, and Spikes."),
	PROTECT("Protect", 0, Element.NORMAL, 100, 10, "status", 4, MovesExtra.PROTECT, false, "It lets the user evade a move. Its chance of failing rises if used in succession."),
	WATER_PULSE("Water Pulse", 60, Element.WATER, 1, 20, "special", 0, MovesExtra.CONFUSION_20_PERCENT, true, "The user attacks the target with a pulsing blast of water. It may also confuse the target."),
	AQUA_TAIL("Aqua Tail", 90, Element.WATER, 0.9, 10, "physical", 0, MovesExtra.NONE, true, "The user attacks by swinging its tail as if it were a vicious wave in a raging storm."),
	THUNDER_SHOCK("Thunder Shock",40 , Element.ELECTRIC, 1, 30, "special", 0, MovesExtra.PARALYSIS_10_PERCENT, true, "A jolt of electricity is hurled at the target. It may also paralyze the target."),
	QUICK_ATTACK("Quick Attack", 40, Element.NORMAL, 1, 30, "physical", 1, MovesExtra.NONE, true, "The user slams the target at a tremendous speed. It is sure to strike first."),
	FEINT("Feint", 30, Element.NORMAL, 1, 10, "physical", 2, MovesExtra.FEINT, true, "An attack that hits a target using Protect or Detect. It lifts the effects of said moves."),
	SPARK("Spark", 60, Element.ELECTRIC, 1, 20, "physical", 0, MovesExtra.PARALYSIS_30_PERCENT, true, "The user electrically tackles at the target. It may also paralyze the target."),
	NUZZLE("Nuzzle", 20, Element.ELECTRIC, 1, 20, "physical", 0, MovesExtra.PARALYSIS_100_PERCENT, true, "The user attacks by nuzzling its electrified cheeks. It paralyzes the target."),
	DISCHARGE("Discharge", 80, Element.ELECTRIC, 1, 15, "special", 0, MovesExtra.PARALYSIS_30_PERCENT, true, "Electricity is loosed to strike the area around the user. It may also cause paralysis."),
	SLAM("Slam", 80, Element.NORMAL, 0.75, 20, "physical", 0, MovesExtra.NONE, true, "The target is slammed with a long tail, vines, etc., to inflict damage."),
	WILD_CHARGE("Wild Charge", 90, Element.ELECTRIC, 1, 15, "physical", 0, MovesExtra.RECOIL_25_PERCENT, true, "The user shrouds itself in electricity and smashes into its target. It damages the user."),
	THUNDER_WAVE("Thunder Wave", 0, Element.ELECTRIC, 0.9, 20, "status", 0, MovesExtra.PARALYSIS_100_PERCENT, true, "A weak electric charge is launched at the target. It causes paralysis if it hits."),
	WING_ATTACK("Wing Attack", 60, Element.FLYING, 1, 35, "physical", 0, MovesExtra.NONE, true, "The target is struck with large, imposing wings spread wide to inflict damage."),
	FLARE_BLITZ("Flare Blitz", 120, Element.FIRE, 1, 15, "physical", 0, MovesExtra.RECOIL_ONE_THIRD, true, "The user sets fire and charges. It sustains serious damage and may burn the target."),
	HEAT_WAVE("Heat Wave", 95, Element.FIRE, 0.9, 10, "special", 0, MovesExtra.BURN_10_PERCENT, true, "The user blasts hot breath on the opposing team. It may burn the targets."),
	DRAGON_CLAW("Dragon Claw", 80, Element.DRAGON, 1, 15, "physical", 0, MovesExtra.NONE, true, "The user slashes the target with huge, sharp claws."),
	SHADOW_CLAW("Shadow Claw", 70, Element.GHOST, 1, 15, "physical", 0, MovesExtra.HIGH_CRIT, true, "The user slashes with a sharp claw made from shadows. Critical hits land more easily."),
	AIR_SLASH("Air Slash", 75, Element.FLYING, 0.95, 15, "special", 0, MovesExtra.FLINCH_30_PERCENT, true, "The user attacks with a blade of air that slices the sky. It may make the target flinch."),
	PETAL_BLIZZARD("Petal Blizzard", 90, Element.GRASS, 1, 15, "physical", 0, MovesExtra.NONE, true, "The user stirs up a violent petal blizzard and attacks everything around it."),
	FLASH_CANNON("Flash Cannon", 80, Element.STEEL, 1, 10, "special", 0, MovesExtra.SPDEF_DOWN_10_PERCENT_OP, true, "The user gathers all its light energy and releases it at once. It may also lower the target's Sp. Def stat."),
	TAIL_WHIP("Tail Whip", 0, Element.NORMAL, 1, 30, "status", 0, MovesExtra.DEF_DOWN_100_PERCENT_OP, true, "The user wags its tail cutely, lowering foes' Defense stat."),
	BABY_DOLL_EYES("Baby-Doll Eyes", 0, Element.FAIRY, 1, 30, "status", 1, MovesExtra.ATTACK_DOWN_100_PERCENT_OP, true, "Using its baby-doll eyes, the user lowers the opponent Atk stat. It always goes first."),
	SAND_ATTACK("Sand Attack", 0, Element.GROUND, 1, 15, "status", 0, MovesExtra.ACCURACY_DOWN_100_PERCENT_OP, true, "Sand is hurled in the target's face, reducing its accuracy."),
	SCARY_FACE("Scary Face", 0, Element.NORMAL, 1, 10, "status", 0, MovesExtra.SPEED_DOWN_2_100_PERCENT_OP, true, "The user frightens the target with a scary face to harshly reduce its Speed stat."),
	SMOG("Smog", 30, Element.POISON, 0.7, 20, "special", 0, MovesExtra.POISON_40_PERCENT, true, "The target is attacked with a discharge of filthy gases. It may also poison the target."),
	LAVA_PLUME("Lava Plume", 80, Element.FIRE, 1, 15, "special", 0, MovesExtra.BURN_30_PERCENT, true, "An inferno torches everything around the user. It may burn the targets."),
	THUNDER_FANG("Thunder Fang", 65, Element.ELECTRIC, 0.95, 15, "physical", 0, MovesExtra.THUNDER_FANG, true, "The user bites with electrified fangs. It may make the target flinch or paralyze it."),
	AGILITY("Agility", 0, Element.PSYCHIC, 100, 30, "status", 0, MovesExtra.SPEED_UP_2_100_PERCENT_ATT, false, "The user lightens its body to move faster. It sharply boosts the Speed stat."),
	AURORA_BEAM("Aurora Beam", 65, Element.ICE, 1, 20, "special", 0, MovesExtra.ATTACK_DOWN_10_PERCENT_OP, true, "The target is hit by a colored ray. This may lower the target's Atk stat."),
	ACID_ARMOR("Acid Armor", 0, Element.POISON, 1, 20, "status", 0, MovesExtra.DEF_UP_2_100_PERCENT_ATT, false, "The user alters its cell structure to liquefy itself, sharply raising its Defense."),
	MUDDY_WATER("Muddy Water", 90, Element.WATER, 0.85, 10, "special", 0, MovesExtra.ACCURACY_DOWN_30_PERCENT_OP, true, "The user attacks by shooting muddy water at the opposing team. It may also lower the target's accuracy."),
	HAZE("Haze", 0, Element.ICE, 1, 30, "status", 0, MovesExtra.CLEAR_STATUS_CHANGES_FIELD, false, "A haze eliminates every stat change among all the Pok�mon in the battle."),
	SWIFT("Swift", 60, Element.NORMAL, 100, 20, "special", 0, MovesExtra.NONE, true, "Star-shaped rays are shot at the opposing team. This attack never misses."),
	REFRESH("Refresh", 0, Element.NORMAL, 100, 20, "status", 0, MovesExtra.REFRESH, false, "The user rests to cure itself of a poisoning, burn, or paralysis."),
	CHARM("Charm", 0, Element.FAIRY, 1, 20, "status", 0, MovesExtra.ATTACK_DOWN_2_100_PERCENT_OP, true, "The user gazes at the target, making it less wary. The target's Attack is harshly lowered."),
	GROWL("Growl", 0, Element.NORMAL, 1, 40, "status", 0, MovesExtra.ATTACK_DOWN_100_PERCENT_OP, true, "The user growls, making the opposing team less wary. The foes' Attack stats are lowered."),
	SMOKESCREEN("Smokescreen", 0, Element.NORMAL, 1, 20, "status", 0, MovesExtra.ACCURACY_DOWN_100_PERCENT_OP, true, "The user releases an obscuring cloud of smoke or ink. It reduces the target's accuracy."),
	WITHDRAW("Withdraw", 0, Element.WATER, 100, 40, "status", 0, MovesExtra.DEF_UP_100_PERCENT_ATT, false, "The user withdraws its body into its hard shell, raising its Defense stat."),
	IRON_DEFENSE("Iron Defense", 0, Element.STEEL, 100, 15, "status", 0, MovesExtra.DEF_UP_2_100_PERCENT_ATT, false, "The user hardens its body's surface like iron, sharply raising its Defense stat."),
	COVET("Covet", 60, Element.NORMAL, 1, 25, "physical", 0, MovesExtra.THIEF, true, "The user approaches the target, then steals the target's held item."),
	SWEET_SCENT("Sweet Scent", 0, Element.NORMAL, 1, 20, "status", 0, MovesExtra.EVASION_DOWN_100_PERCENT_OP, true, "A sweet scent that lowers the opposing team's evasiveness. It also lures wild Pok�mon."),
	WORRY_SEED("Worry Seed", 0, Element.GRASS, 1, 10, "status", 0, MovesExtra.WORRY_SEED, true, "Plants a seed that causes worry. It prevents sleep by making its Ability Insomnia."),
	LEECH_SEED("Leech Seed", 0, Element.GRASS, 0.9, 10, "status", 0, MovesExtra.LEECH_SEED, true, "A seed is planted on the target. It steals some HP from the target every turn."),
	BLIZZARD("Blizzard", 110, Element.ICE, 0.7, 5, "special", 0, MovesExtra.FREEZE_10_PERCENT, true, "A howling blizzard is blasted at the opposing team. It may also freeze them solid."),
	HURRICANE("Hurricane", 110, Element.FLYING, 0.7, 10, "special", 0, MovesExtra.CONFUSION_30_PERCENT, true, "The user wraps its opponent in a fierce circular wind. It may confuse the target."),
	SOLAR_BEAM("SolarBeam", 120, Element.GRASS, 1, 10, "special", 0, MovesExtra.CHARGE_UP_TURN, true, "A two-turn attack. The user gathers light, then blasts a beam on the second turn."),
	RAIN_DANCE("Rain Dance", 0, Element.WATER, 100, 5, "status", 0, MovesExtra.RAIN_DANCE, false, "The user summons a heavy rain that falls for five turns, powering up Water-type moves."),
	GROWTH("Growth", 0, Element.NORMAL, 100, 20, "status", 0, MovesExtra.GROWTH, false, "The user's body grows all at once, raising the Atk and Sp. Atk stats."),
	HYPER_BEAM("Hyper Beam", 150, Element.NORMAL, 0.9, 5, "special", 0, MovesExtra.RECHARGE_TURN, true, "The target is attacked with a powerful beam. The user must rest after."),
	DOUBLE_KICK("Double Kick", 30, Element.FIGHTING, 1, 30, "physical", 0, MovesExtra.DOUBLE_HIT, true, "The target is quickly kicked twice in succession using both feet."),
	PIN_MISSILE("Pin Missile", 20, Element.BUG, 0.95, 20, "physical", 0, MovesExtra.MULTI_HIT, true, "Sharp spikes are shot at the target in succession. They hit two to five times in a row."),
	DOUBLE_TEAM("Double Team", 0, Element.NORMAL, 100, 15, "status", 0, MovesExtra.EVASION_UP_100_PERCENT_ATT, false, "By moving rapidly, the user makes illusory copies of itself to raise its evasiveness."),
	PLAY_NICE("Play Nice", 0, Element.NORMAL, 100, 20, "status", 0, MovesExtra.ATTACK_DOWN_100_PERCENT_OP, true, "The user and the target become friends. This lowers the target's Atk."),
	ELECTRO_BALL("Electro Ball", 60, Element.ELECTRIC, 1, 10, "special", 0, MovesExtra.ELECTRO_BALL, true, "The faster the user is than the target, the greater the damage this move deals."),
	LIGHT_SCREEN("Light Screen", 0, Element.PSYCHIC, 100, 30, "status", 0, MovesExtra.LIGHT_SCREEN, false, "A wondrous wall of light is put up to suppress damage from special attacks for five turns."),
	ICE_FANG("Ice Fang", 65, Element.ICE, 0.95, 15, "physical", 0, MovesExtra.ICE_FANG, true, "	The user bites with cold-infused fangs. This may also make the target flinch or leave it frozen.");
	
	
    private final String name;
	private final int power;
	private final Element element;
	private final double accuracy;
	private final int pp;
	private final String moveType;
	private final MovesExtra extra;
	private final int priority;
	private final boolean attacksOpponent;
	private final String description;

    MoveList(String name, int power, Element element, double accuracy, int pp, String moveType, int priority, MovesExtra extra, boolean attacksOpponent, String description) 
    {
        this.name = name;
        this.power = power;
        this.element = element;
        this.accuracy = accuracy;
        this.pp = pp;
        this.moveType = moveType;
        this.priority = priority;
        this.extra = extra;
        this.attacksOpponent = attacksOpponent;
        this.description = description;
    }
    
    public String getName()
    {
    	return this.name;
    }
    
    public int getPower()
    {
    	return this.power;
    }
    
    public Element getElement()
    {
    	return this.element;
    }
    
    public double getAccuracy()
    {
    	return this.accuracy;
    }
    
    public int getPp()
    {
    	return this.pp;
    }
    
    public String getMoveType()
    {
    	return this.moveType;
    }
    
    public MovesExtra getExtra()
    {
    	return this.extra;
    }
    
    public int getPriority()
    {
    	return this.priority;
    }
    
    /**
     * Returns true if the move affects the opposing Pokemon.
     * @return bolean
     */
    public boolean attacksOpponent()
    {
    	return this.attacksOpponent;
    }
    
    public String getDescription()
    {
    	return this.description;
    }
}
