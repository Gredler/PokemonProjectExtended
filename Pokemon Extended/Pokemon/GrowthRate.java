/**
 * Enum for all the different growth rates of Pokemon.
 * @author Andi
 * @version 1.1
 */
public enum GrowthRate 
{
	MEDIUM("Medium"),
	ERRATIC("Erratic"),
	FLUCTUATING("Fluctuating"),
	PARABOLIC("Parabolic"),
	FAST("Fast"),
	SLOW("Slow");
	
	private final String name;
	
	GrowthRate(String name)
	{
		this.name = name;
	}
	
	public String getName()
	{
		return this.name;
	}
}
