/**
 * Enum for all the extra effects a move can have.
 * @author Andi
 * @version 3.3
 */
public enum MovesExtra 
{
	NONE("None", "none", 0),
	
	BURN_100_PERCENT("Burn100Percent", "Burn", 1),
	BURN_30_PERCENT("Burn30Percent", "Burn", 0.3),
	BURN_10_PERCENT("Burn10Percent", "Burn", 0.1),
	
	POISON_100_PERCENT("Poison100Percent", "Poison", 1),
	POISON_40_PERCENT("Poison40Percent", "Poison", 0.4),
	POISON_30_PERCENT("Poison30Percent", "Poison", 0.3),
	POISON_10_PERCENT("Poison10Percent", "Poison", 0.1),
	
	PARALYSIS_100_PERCENT("Paralysis100Percent", "Paralysis", 1),
	PARALYSIS_30_PERCENT("Paralysis30Percent", "Paralysis", 0.3),
	PARALYSIS_10_PERCENT("Paralysis10Percent", "Paralysis", 0.1),
	
	SLEEP_100_PERCENT("Sleep100Percent", "Sleep", 1),
	SLEEP_30_PERCENT("Sleep30Percent", "Sleep", 0.3),
	SLEEP_10_PERCENT("Sleep10Percent", "Sleep", 0.1),
	
	CONFUSION_100_PERCENT("Confusion100Percent", "Confusion", 1),
	CONFUSION_30_PERCENT("Confusion30Percent", "Confusion", 0.3),
	CONFUSION_20_PERCENT("Confusion20Percent", "Confusion", 0.2),
	
	FREEZE_10_PERCENT("Freeze10Percent", "Freeze", 0.1),
	
	THUNDER_FANG("ThunderFang" , "FangMove", 0.1),
	FIRE_FANG("FireFang" , "FangMove" , 0.1),
	ICE_FANG("IceFang", "FangMove", 0.1),
	
	FLAT_DAMAGE_40("FlatDamage40", "FlatDamage", 40),
	
	RECOIL_25_PERCENT("Recoil25Percent", "Recoil", 0.25),
	RECOIL_ONE_THIRD("RecoilOneThird", "Recoil", 1.0/3.0),
	
	FLINCH_30_PERCENT("Flinch30Percent", "Flinch", 0.3),
		
	ATTACK_UP_10_PERCENT_ATT("AttackUp10PercentATT", "Attack+1Attacker", 0.1),
	ATTACK_DOWN_100_PERCENT_OP("AttackDown100PercentOP", "Attack-1Opponent", 1),
	ATTACK_DOWN_10_PERCENT_OP("AttackDown10PercentOP", "Attack-1Opponent", 0.1),
	ATTACK_DOWN_2_100_PERCENT_OP("AttackDown2100PercentOP", "Attack-2Opponent", 1),
	
	DEF_UP_100_PERCENT_ATT("DefUp100PercentAtt", "Defence+1Attacker", 1),
	DEF_UP_2_100_PERCENT_ATT("DefUp2100PercentAtt", "Defence+2Attacker", 1),
	DEF_DOWN_100_PERCENT_OP("DefDown100PercentOP", "Defence-1Opponent", 1),
	DEF_DOWN_30_PERCENT_OP("DefDown30PercentOP", "Defence-1Opponent", 0.3),
	
	SPDEF_DOWN_10_PERCENT_OP("SpDefDown10PercentOP", "SpD-1Opponent", 0.1),
	
	SPEED_DOWN_2_100_PERCENT_OP("Speedown2100PercentOP", "Speed-2Opponent", 1),
	SPEED_DOWN_10_PERCENT_OP("SpeedDown10PercentOP", "Speed-1Opponent", 0.1),
	SPEED_UP_2_100_PERCENT_ATT("SpeedUp2100PercentUpATT", "Speed+2Attacker", 1),
	
	ACCURACY_DOWN_100_PERCENT_OP("AccuracyDown100PercentOP", "Accuracy-1Opponent", 1),
	ACCURACY_DOWN_30_PERCENT_OP("AccuracyDown30PercentOP", "Accuracy-1Opponent", 0.3),
	
	EVASION_DOWN_100_PERCENT_OP("EvasionDown100PercentOP", "Evasion-1Opponent", 1),
	EVASION_UP_100_PERCENT_ATT("EvasionUp100PercentAtt", "Evasion+1Attacker", 1),
	
	GROWTH("Growth", "Growth", 1),
	
	RAIN_DANCE("RainDance", "RainDance", 1),
	
	FEINT("Feint", "Feint", 1),
	PROTECT("Protect", "Protect", 1),
	THIEF("Thief", "Thief", 1),
	HIGH_CRIT("HighCrit", "HighCrit", 1),
	HEAL_50_PERCENT("Heal50Percent", "Heal", 0.5),
	SUN_HEAL("SunHeal", "SunHeal", 0.5),
	FIRE_SPIN("FireSpin", "FireSpin", 1.0/8.0),
	WORRY_SEED("WorrySeed", "WorrySeed", 1),
	LEECH_SEED("LeechSeed", "LeechSeed", 1),
	
	CHARGE_UP_TURN("ChargeUpTurn", "ChargeUpTurn", 1),
	RECHARGE_TURN("RechargeTurn", "RechargeTurn", 1),
	DOUBLE_HIT("DoubleHit", "DoubleHit", 2),
	MULTI_HIT("MultiHit", "MultiHit", 5),
	ELECTRO_BALL("ElectroBall", "ElectroBall", 1),
	
	REOVE_HAZARDS("RemoveHazards", "RemoveHazards", 1),
	CLEAR_STATUS_CHANGES_FIELD("ClearStatusChangesField", "ClearStatusField", 1),
	REFRESH("Refresh", "Refresh", 1),
	
	REFLECT("Reflect", "Reflect", 1),
	LIGHT_SCREEN("LightScreen", "LightScreen", 1);
	
	
	
	private final String name;
	private final String effect;
	private final double value;
	
	MovesExtra(String name, String effect, double value)
	{
		this.name = name;
		this.effect = effect;
		this.value = value;
	}
	
	public String getName()
	{
		return this.name;
	}
	
	public String getEffect()
	{
		return this.effect;
	}
	
	public double getValue()
	{
		return this.value;
	}
}
