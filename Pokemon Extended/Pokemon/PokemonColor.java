/**
 * Enum for all the colors a Pokemon can have (in the Pokedex).
 * @author Andi
 * @version 1.1
 */
public enum PokemonColor 
{
	RED("Red"),
	BLUE("Blue"),
	YELLOW("Yellow"),
	GREEN("Green"),
	BLACK("Black"),
	BROWN("Brown"),
	PURPLE("Purple"),
	GRAY("Gray"),
	WHITE("White"),
	PINK("Pink");
	
	private final String name;
	
	PokemonColor(String name)
	{
		this.name = name;
	}
	
	public String getName()
	{
		return this.name;
	}
}
