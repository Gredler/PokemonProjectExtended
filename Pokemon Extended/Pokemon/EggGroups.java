/**
 * Enum for all the existing Egg Groups in Pokemon.
 * @author Andi
 * @version 1.3
 */
public enum EggGroups 
{
	UNDEFINED("Undefined"),
	MONSTER("Monster"),
	WATER_1("Water1"),
	WATER_2("Water2"),
	WATER_3("Water3"),
	BUG("Bug"),
	FLYING("Flying"),
	GROUND("Ground"),
	FAIRY("Fairy"),
	PLANT("Plant"),
	HUMANSHAPE("Humanshape"),
	MINERAL("Mineral"),
	INDETERMINATE("Indeterminate"),
	DITTO("Ditto"),
	DRAGON("Dragon"),
	NO_EGGS("NoEggs");
	
	
	private final String name;
	
	EggGroups(String name)
	{
		this.name = name;
	}
	
	public String getName()
	{
		return this.name;
	}
}
