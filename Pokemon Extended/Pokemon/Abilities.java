/**
 * Enum for every implemented Ability.
 * @author Andi
 * @version 1.3
 */
public enum Abilities 
{
	BLAZE("Blaze", 0),
	OVERGROW("Overgrow", 1),
	TORRENT("Torrent", 2),
	SWARM("Swarm", 3),
	STATIC("Static", 4),
	LIGHTNING_ROD("Lightning Rod", 5),
	VOLT_ABSORB("Volt Absorb", 6),
	WATER_ABSORB("Water Absorb", 7),
	RAIN_DISH("Rain Dish", 8),
	FLASH_FIRE("Flash Fire", 9),
	GUTS("Guts", 10),
	QUICK_FEET("Quick Feet", 11),
	CHLOROPHYLL("Chlorophyll", 12),
	HYDRATION("Hydration", 13),
	SOLAR_POWER("Solar Power", 14),
	ADAPTABILITY("Adaptability", 15),
	RUN_AWAY("Run Away", 16),
	ANTICIPATION("Anticipation", 17),
	INSOMNIA("Insomnia", 18),
	DROUGHT("Drought", 19),
	DRIZZLE("Drizzle", 20),
	SAND_STREAM("Sand Stream", 21),
	SNOW_WARNING("Snow Warning", 22),
	DESOLATE_LAND("Desolate Land", 23),
	PRIMORDIAL_SEA("Primordial Sea", 24),
	DELTA_STREAM("Delta Stream", 25);
	
	private final String name;
	private final int id;
	
	Abilities(String name, int id)
	{
		this.name = name;
		this.id = id;
	}
	
	public String getName()
	{
		return this.name;
	}
	
	public int getId()
	{
		return this.id;
	}
}
