/**
 * Enum for all the implmented habitats of Pokemon.
 * @author Andi
 * @version 2.1
 */
public enum PokemonHabitat 
{
	NONE("None"),
	GRASSLAND("Grassland"),
	FOREST("Forest"),
	WATERS_EDGE("WatersEdge"),
	SEA("Sea"),
	CAVE("Cave"),
	MOUNTAIN("Mountain"),
	ROUGH_TERRAIN("RoughTerrain"),
	URBAN("Urban"),
	RARE("Rare");
	
	private final String name;
	
	PokemonHabitat(String name)
	{
		this.name = name;
	}
	
	public String getName()
	{
		return this.name;
	}
}
