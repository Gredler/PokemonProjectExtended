/**
 * Enum for every existing Element in the Pokemon universe.
 * @author Andi
 * @version 2.0
 */
public enum Element 
{
    NONE("NONE"),
    NORMAL("NORMAL"),
    FIRE("FIRE"),
    WATER("WATER"),
    ELECTRIC("ELECTRIC"),
    GRASS("GRASS"),
    BUG("BUG"),
    POISON("POISON"),
    ICE("ICE"),
    DRAGON("DRAGON"),
    FAIRY("FAIRY"),
    STEEL("STEEL"),
    ROCK("ROCK"),
    GROUND("GROUND"),
    FIGHTING("FIGHTING"),
    FLYING("FLYING"),
    PSYCHIC("PSYCHIC"),
    DARK("DARK"),
    GHOST("GHOST");

    private final String name;

    Element(String name) 
    {
        this.name = name;
    }
    
    public String getName()
    {
    	return this.name;
    }
    
    /**
     * Returns true if the attack is super effective.
     * @param attack
     * @param enemy
     * @return boolean
     */
    public boolean isSuperEffective(Element attack, Element enemy)
    {
    	switch(attack)
    	{
	    	case NORMAL: return false;
	    	case FIRE: if (enemy.name.contentEquals("GRASS") || enemy.name.contentEquals("ICE") || enemy.name.contentEquals("STEEL") || enemy.name.contentEquals("BUG")) return true;
	    		else return false;
	    	case WATER: if (enemy.name.contentEquals("FIRE") || enemy.name.contentEquals("ROCK") || enemy.name.contentEquals("GROND")) return true;
	    		else return false;
	    	case ELECTRIC: if (enemy.name.contentEquals("WATER") || enemy.name.contentEquals("FLYING")) return true;
	    		else return false;
	    	case GRASS: if (enemy.name.contentEquals("WATER") || enemy.name.contentEquals("GROUND") || enemy.name.contentEquals("ROCK")) return true;
	    			else return false;
	    	case BUG: if (enemy.name.contentEquals("PSYCHIC") || enemy.name.contentEquals("GRASS") || enemy.name.contentEquals("DARK")) return true;
	    		else return false;
	    	case POISON: if (enemy.name.contentEquals("GRASS") || enemy.name.contentEquals("FAIRY")) return true;
	    		else return false;
	    	case ICE: if (enemy.name.contentEquals("GRASS") || enemy.name.contentEquals("FLYING") || enemy.name.contentEquals("DRAGON") || enemy.name.contentEquals("GROUND")) return true;
	    		else return false;
	    	case DRAGON: if (enemy.name.contentEquals("DRAGON")) return true;
	    		else return false;
	    	case FAIRY: if (enemy.name.contentEquals("DRAGON") || enemy.name.contentEquals("DARK") || enemy.name.contentEquals("FIGHTING")) return true;
	    		else return false;
	    	case STEEL: if (enemy.name.contentEquals("ROCK") || enemy.name.contentEquals("FAIRY") || enemy.name.contentEquals("ICE")) return true;
	    		else return false;
	    	case ROCK: if (enemy.name.contentEquals("FLYING") || enemy.name.contentEquals("FIRE") || enemy.name.contentEquals("ICE") || enemy.name.contentEquals("BUG")) return true;
	    		else return false;
	    	case GROUND: if (enemy.name.contentEquals("FIRE") || enemy.name.contentEquals("ROCK") || enemy.name.contentEquals("ELECTRIC") || enemy.name.contentEquals("STEEL") || enemy.name.contentEquals("POISON")) return true;
	    		else return false;
	    	case FIGHTING: if (enemy.name.contentEquals("NORMAL") || enemy.name.contentEquals("ROCK") || enemy.name.contentEquals("STEEL") || enemy.name.contentEquals("ICE") || enemy.name.contentEquals("DARK")) return true;
	    		else return false;
	    	case FLYING: if (enemy.name.contentEquals("BUG") || enemy.name.contentEquals("GRASS") || enemy.name.contentEquals("FIGHTING")) return true;
	    		else return false;
	    	case PSYCHIC: if (enemy.name.contentEquals("POISON") || enemy.name.contentEquals("FIGHTING")) return true;
	    		else return false;
	    	case DARK: if (enemy.name.contentEquals("PSYCHIC") || enemy.name.contentEquals("GHOST")) return true;
	    		else return false;
	    	case GHOST: if (enemy.name.contentEquals("GHOST") || enemy.name.contentEquals("PSYCHIC")) return true;
	    		else return false;
	    	default: return false;
    	}
    }
    
    /**
     * Returns true if the opponent resists the attack.
     * @param attack
     * @param enemy
     * @return boolean
     */
    public boolean isResistant(Element attack, Element enemy)
    {
    	switch (attack)
    	{
	    	case NORMAL: if (enemy.name.contentEquals("ROCK") || enemy.name.contentEquals("STEEL")) return true;
	    	else return false;
	    	case FIRE: if (enemy.name.contentEquals("WATER") || enemy.name.contentEquals("ROCK") || enemy.name.contentEquals("FIRE") || enemy.name.contentEquals("DRAGON")) return true;
	    		else return false;
	    	case WATER: if (enemy.name.contentEquals("WATER") || enemy.name.contentEquals("DRAGON") || enemy.name.contentEquals("GRASS")) return true;
	    		else return false;
	    	case ELECTRIC: if (enemy.name.contentEquals("ELECTRIC") || enemy.name.contentEquals("GRASS") || enemy.name.contentEquals("DRAGON")) return true;
	    		else return false;
	    	case GRASS: if (enemy.name.contentEquals("GRASS") || enemy.name.contentEquals("FIRE") || enemy.name.contentEquals("BUG") || enemy.name.contentEquals("DRAGON") || enemy.name.contentEquals("POISON") || enemy.name.contentEquals("FLYING") || enemy.name.contentEquals("STEEL")) return true;
	    		else return false;
	    	case BUG: if (enemy.name.contentEquals("FLYING") || enemy.name.contentEquals("FIGHTING") || enemy.name.contentEquals("GHOST") || enemy.name.contentEquals("STEEL") || enemy.name.contentEquals("POISON") || enemy.name.contentEquals("FIRE")) return true;
	    		else return false;
	    	case POISON: if (enemy.name.contentEquals("POISON") || enemy.name.contentEquals("GROUND") || enemy.name.contentEquals("ROCK") || enemy.name.contentEquals("GHOST")) return true;
	    		else return false;
	    	case ICE: if (enemy.name.contentEquals("ICE") || enemy.name.contentEquals("WATER") || enemy.name.contentEquals("STEEL") || enemy.name.contentEquals("FIRE")) return true;
	    		else return false;
	    	case DRAGON: if (enemy.name.contentEquals("STEEL")) return true;
	    		else return false;
	    	case FAIRY: if (enemy.name.contentEquals("POISON") || enemy.name.contentEquals("STEEL") || enemy.name.contentEquals("FIRE")) return true;
	    		else return false;
	    	case STEEL: if (enemy.name.contentEquals("STEEL") || enemy.name.contentEquals("FIRE") || enemy.name.contentEquals("WATER") || enemy.name.contentEquals("ELECTRIC")) return true;
	    		else return false;
	    	case ROCK: if (enemy.name.contentEquals("FIGHTING") || enemy.name.contentEquals("STEEL") || enemy.name.contentEquals("GROUND")) return true;
	    		else return false;
	    	case GROUND: if (enemy.name.contentEquals("BUG") || enemy.name.contentEquals("GRASS")) return true;
	    		else return false;
	    	case FIGHTING: if (enemy.name.contentEquals("BUG") || enemy.name.contentEquals("PSYCHIC") || enemy.name.contentEquals("FLYING") || enemy.name.contentEquals("POISON") || enemy.name.contentEquals("FAIRY")) return true;
	    		else return false;
	    	case FLYING: if (enemy.name.contentEquals("ROCK") || enemy.name.contentEquals("STEEL") || enemy.name.contentEquals("ELECTRIC")) return true;
	    		else return false;
	    	case PSYCHIC: if (enemy.name.contentEquals("PSYCHIC") || enemy.name.contentEquals("STEEL")) return true;
	    		else return false;
	    	case DARK: if (enemy.name.contentEquals("FAIRY") || enemy.name.contentEquals("DARK") || enemy.name.contentEquals("FIGHTING")) return true;
	    		else return false;
	    	case GHOST: if (enemy.name.contentEquals("DARK")) return true;
	    		else return false;
	    	default: return false;
    	}
    }
    
    /**
     * Returns true if the opponent is immune to the attack.
     * @param attack
     * @param enemy
     * @return boolean
     */
    public boolean isImmune(Element attack, Element enemy)
    {
    	switch(attack)
    	{
	    	case GHOST: if (enemy.name.contentEquals("NORMAL")) return true;
	    		else return false;
	    	case FIGHTING: if (enemy.name.contentEquals("GHOST")) return true;
	    		else return false;
	    	case DRAGON: if (enemy.name.contentEquals("FAIRY")) return true;
	    		else return false;
	    	case PSYCHIC: if (enemy.name.contentEquals("DARK")) return true;
	    		else return false;
	    	case NORMAL: if (enemy.name.contentEquals("GHOST")) return true;
	    		else return false;
	    	case POISON: if (enemy.name.contentEquals("STEEL")) return true;
	    		else return false;
	    	case GROUND: if (enemy.name.contentEquals("FLYING")) return true;
	    		else return false;
	    	default: return false;
    	}
    }
    
    public boolean isNormalEffective(Element attack, Element enemy)
    {
    	switch(attack)
    	{
    		case NORMAL: if (enemy.name.contentEquals("ROCK") || enemy.name.contentEquals("GHOST") || enemy.name.contentEquals("STEEL")) return false;
    		else return true;
    		case FIRE: if (enemy.name.contentEquals("WATER") || enemy.name.contentEquals("ROCK") || enemy.name.contentEquals("FIRE") || enemy.name.contentEquals("DRAGON") || enemy.name.contentEquals("GRASS") || enemy.name.contentEquals("ICE") || enemy.name.contentEquals("BUG") || enemy.name.contentEquals("STEEL")) return false;
    		else return true;
    		case WATER: if (enemy.name.contentEquals("WATER") || enemy.name.contentEquals("DRAGON") || enemy.name.contentEquals("GRASS") || enemy.name.contentEquals("FIRE") || enemy.name.contentEquals("ROCK") || enemy.name.contentEquals("GROUND")) return false;
    			else return true;
    		case ELECTRIC: if (enemy.name.contentEquals("ELECTRIC") || enemy.name.contentEquals("GRASS") || enemy.name.contentEquals("DRAGON") || enemy.name.contentEquals("WATER") || enemy.name.contentEquals("FLYING") || enemy.name.contentEquals("GROUND")) return false;
    			else return true;
    		case GRASS: if (enemy.name.contentEquals("GRASS") || enemy.name.contentEquals("FIRE") || enemy.name.contentEquals("BUG") || enemy.name.contentEquals("DRAGON") || enemy.name.contentEquals("POISON") || enemy.name.contentEquals("FLYING") || enemy.name.contentEquals("STEEL") || enemy.name.contentEquals("WATER") || enemy.name.contentEquals("ROCK") || enemy.name.contentEquals("GROUND")) return false;
    			else return true;
    		case BUG: if (enemy.name.contentEquals("FLYING") || enemy.name.contentEquals("FIGHTING") || enemy.name.contentEquals("GHOST") || enemy.name.contentEquals("STEEL") || enemy.name.contentEquals("POISON") || enemy.name.contentEquals("FIRE") || enemy.name.contentEquals("PSYCHIC") || enemy.name.contentEquals("DARK") || enemy.name.contentEquals("GRASS")) return false;
    			else return true;
    		case POISON: if (enemy.name.contentEquals("POISON") || enemy.name.contentEquals("GROUND") || enemy.name.contentEquals("ROCK") || enemy.name.contentEquals("GHOST") || enemy.name.contentEquals("STEEL") || enemy.name.contentEquals("GRASS") || enemy.name.contentEquals("FAIRY")) return false;
    			else return true;
    		case ICE: if (enemy.name.contentEquals("ICE") || enemy.name.contentEquals("WATER") || enemy.name.contentEquals("STEEL") || enemy.name.contentEquals("FIRE") || enemy.name.contentEquals("DRAGON") || enemy.name.contentEquals("FLYING") || enemy.name.contentEquals("GROUND") || enemy.name.contentEquals("GRASS")) return false;
    			else return true;
    		case DRAGON: if (enemy.name.contentEquals("STEEL") || enemy.name.contentEquals("FAIRY") || enemy.name.contentEquals("DRAGON")) return false;
    			else return true;
    		case FAIRY: if (enemy.name.contentEquals("POISON") || enemy.name.contentEquals("STEEL") || enemy.name.contentEquals("FIRE") || enemy.name.contentEquals("DRAGON") || enemy.name.contentEquals("DARK") || enemy.name.contentEquals("FIGHTING")) return false;
    			else return true;
    		case STEEL: if (enemy.name.contentEquals("STEEL") || enemy.name.contentEquals("FIRE") || enemy.name.contentEquals("WATER") || enemy.name.contentEquals("ELECTRIC") || enemy.name.contentEquals("ROCK") || enemy.name.contentEquals("ICE") || enemy.name.contentEquals("FAIRY")) return false;
    			else return true;
    		case ROCK: if (enemy.name.contentEquals("FIGHTING") || enemy.name.contentEquals("STEEL") || enemy.name.contentEquals("GROUND") || enemy.name.contentEquals("FLYING") || enemy.name.contentEquals("ICE") || enemy.name.contentEquals("FIRE") || enemy.name.contentEquals("BUG")) return false;
    			else return true;
    		case GROUND: if (enemy.name.contentEquals("BUG") || enemy.name.contentEquals("GRASS") || enemy.name.contentEquals("FLYING") || enemy.name.contentEquals("STEEL") || enemy.name.contentEquals("FIRE") || enemy.name.contentEquals("ROCK") || enemy.name.contentEquals("ELECTRIC") || enemy.name.contentEquals("POISON")) return false;
    			else return true;
    		case FIGHTING: if (enemy.name.contentEquals("BUG") || enemy.name.contentEquals("PSYCHIC") || enemy.name.contentEquals("FLYING") || enemy.name.contentEquals("POISON") || enemy.name.contentEquals("FAIRY") || enemy.name.contentEquals("NORMAL") || enemy.name.contentEquals("ROCK") || enemy.name.contentEquals("ICE") || enemy.name.contentEquals("STEEL") || enemy.name.contentEquals("DARK")) return false;
    			else return true;
    		case FLYING: if (enemy.name.contentEquals("ROCK") || enemy.name.contentEquals("STEEL") || enemy.name.contentEquals("ELECTRIC") || enemy.name.contentEquals("GRASS") || enemy.name.contentEquals("BUG") || enemy.name.contentEquals("FIGHTING")) return false;
    			else return true;
    		case PSYCHIC: if (enemy.name.contentEquals("PSYCHIC") || enemy.name.contentEquals("STEEL") || enemy.name.contentEquals("FIGHTING") || enemy.name.contentEquals("POISON") || enemy.name.contentEquals("DARK")) return false;
    			else return true;
    		case DARK: if (enemy.name.contentEquals("FAIRY") || enemy.name.contentEquals("DARK") || enemy.name.contentEquals("FIGHTING") || enemy.name.contentEquals("GHOST") || enemy.name.contentEquals("PSYCHIC")) return false;
    			else return true;
    		case GHOST: if (enemy.name.contentEquals("DARK") || enemy.name.contentEquals("NORMAL") || enemy.name.contentEquals("GHOST") || enemy.name.contentEquals("PSYCHIC")) return false;
    			else return true;
    		default: return false;
    	}
    }
}
