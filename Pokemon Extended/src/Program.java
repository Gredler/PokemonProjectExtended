import java.io.File;
import java.util.InputMismatchException;
import java.util.Scanner;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.FloatControl;

//6837 lines - 23.01.2017

/**
 * name: Program
 * This is the Main class of the program itself. It contains the main method and a few extra methods to be able to do what its supposed to.
 * 
 * @version 2.32
 * @author Andi
 *
 */
public class Program
{
	/**
	 * The main method.
	 * Creates a new Player object, starts the music and accesses the rest of the Program class.
	 * @param args
	 */
	public static void main(String[] args) 
	{	
		Clip clip = playMusic();
				
		@SuppressWarnings("resource")
		Scanner input = new Scanner(System.in);
		int question = -1;
		
		Player player = new Player(null, null);
		player = intro(player);
		
		starterChoice(player);
		
		do
		{
			System.out.println("==========================");
			System.out.println("What would you like to do?");
			System.out.println("	[0] Battle");
			System.out.println("	[1] Pokemon");
			System.out.println("	[2] Bag");
			System.out.println("	[3] Exit");
			try 
			{
				question = input.nextInt();
			}
			catch (InputMismatchException exc)
			{
				question = -1;
	
				//consume scanner input
				input.nextLine();
			}
			
			switch (question)
			{
				case 0: clip.stop();
					doBattle(player);
					player.healPokemon();
					break;
				case 1: 
					player.managePokemonTeam();
					break;
				case 2: 
					openBag(player);
					break;
				case 3:
					gameOverASCII();
				default: break;
			}
			clip.start();
		}
		while(question != 3);
		clip.close();
	}
	
	/**
	 * Creates a new Battle object and starts its combat method.
	 * @param player
	 */
	private static void doBattle(Player player)
	{
		Battle battle = new Battle();
		battle.combat(player);
	}
	
	/**
	 * Starts the intro to the game. The user will be asked their gender and name.
	 * The anwered values will be saved in the player object.
	 * @param player
	 * @return Player
	 */
	@SuppressWarnings("resource")
	public static Player intro(Player player)
	{
		logoASCII();
		Scanner input = new Scanner(System.in);
		System.out.println("Hello and welcome to the World of Pokemon! \nNow let's get your registration out of the way. \nFirst I'll need you to tell me your gender. \n");
		
		do
		{ 
			System.out.print("Are you a boy or a girl? ");
			player.setGender(input.nextLine().toLowerCase());
		}
		while (!player.getGender().equals("boy") && !player.getGender().equals("girl") && !player.getGender().equals("neither"));
		if (player.getGender().equals("boy") || player.getGender().equals("girl")) System.out.println("So you're a " + player.getGender() + "!");
		else System.out.println("That's Great!");
		
		System.out.print("So what's your name? ");
		player.setName(input.nextLine()); ;

		System.out.println("Good now we have finished your Registration.\n");
		
		return player;		
	}	
	
	/**
	 * Asks the player which starter they want to choose and adds said Pokemon to their team.
	 * @param player
	 */
	@SuppressWarnings("resource")
	public static void starterChoice(Player player)
	{
		Pokemon starter;
		Scanner input = new Scanner(System.in);
		
		String questions;
		do
		{
			System.out.println("Which Starter Pokemon would you like to choose?");
			System.out.print("Choices: Bulbasaur / Charmander / Squirtle: ");		
			questions = input.nextLine().toLowerCase();
			if (!questions.equals("bulbasaur") && !questions.equals("charmander") && !questions.equals("squirtle"))
					System.out.println("Come on! I don't have all day.\n");
		}
		while(!questions.equals("bulbasaur") && !questions.equals("charmander") && !questions.equals("squirtle"));
		if (questions.equals("charmander")) starter = new Charmander();
		else if (questions.equals("bulbasaur")) starter = new Bulbasaur();
		else if (questions.equals("squirtle")) starter = new Squirtle();
		else starter = new Charmander();
		starter.generatePokemon(20);
		
		do
		{
			System.out.print("Would you like to give your Pokemon a Nickname? ");
			questions = input.nextLine().toLowerCase();
		}
		while (!questions.equals("yes") && !questions.equals("no"));
		
		if (questions.equals("yes")) 
		{
			System.out.print("How will you name your Pokemon? ");
			String nick = input.nextLine();
			starter.setNickname(starter, nick);
		}
		
		System.out.print("Would you like to look at your Pokemon's stats? ");
		questions = input.nextLine();
		if (questions.equals("yes")) starter.getStats();
		
		
		player.addPokemon(starter);
		
		
	}
	
	/**
	 * Opens the bag of the player.
	 * @param player
	 */
	public static void openBag(Player player)
	{
		@SuppressWarnings("resource")
		Scanner input = new Scanner(System.in);
		
		player.showBag();
		
		int item = -1;
		try
		{
			item = input.nextInt();
		}
		catch(Exception exc)
		{
			input.nextLine();
		}
		Items itemChoice = null;
		if (item > -1 && item < player.getBagSize()) 				
		{
			itemChoice = player.getItem(item);
			
			System.out.println("What would you like to do?");
			System.out.println("	[0] Use");
			System.out.println("	[1] Give");
			System.out.println("	[2] Info");
			System.out.println("	[3] Cancel");
			int usage = -1;
			try
			{
				usage = input.nextInt();
			}
			catch(Exception exc)
			{
				input.nextLine();
			}
			
			switch(usage)
			{
			case 0: useItem(player, itemChoice, item);
				break;
			case 1: giveItem(player, itemChoice, item);
				break;
			case 2: System.out.println(itemChoice.getDescription());
				break;
			default: 
				break;
			}
		}
	}
	
	/**
	 * Uses an item on a Pokemon.
	 * @param player
	 * @param item
	 * @param itemNumber
	 */
	public static void useItem(Player player, Items item, int itemNumber)
	{
		@SuppressWarnings("resource")
		Scanner input = new Scanner(System.in);
		int pokeChoice = -1;
		
		System.out.println("Which Pokemon would you like to use the " + item.getName() + " on?");
		for(int i = 0; i < player.getTeamPokemon().size(); i++)
		{
			System.out.println("	[" + i + "] " + player.getPokemon(player, i).name);
		}
		try
		{
			pokeChoice = input.nextInt();
		}
		catch(Exception exc)
		{
			input.nextLine();
		}
		if (pokeChoice != -1)
		{
			if(item.getValue() != 5) System.out.println("This won't have any effect!\n");
			else 
			{
				if(player.getPokemon(player, pokeChoice).canEvolve(item)) 
				{
					Pokemon pokemon = player.getPokemon(player, pokeChoice);
					
					pokemon = pokemon.evolve(item);
					player.setPokemon(pokemon, pokeChoice);
					player.removeItem(itemNumber);
				}
				else System.out.println("This won't have any effect!\n");
			}
		}
	}
	
	/**
	 * Asks the player which pokemon they want to give an item to and (if they choose an acceptable value) gives the item to said pokemon.
	 * @param player
	 * @param item
	 * Item to give to the pokemon.
	 * @param itemIndex
	 * Will be removed from the bag if the item is given to a pokemon.
	 */
	public static void giveItem(Player player, Items item, int itemIndex)
	{
		System.out.println("Which Pokemon would you like to give the " + item.getName() + " to?");
		for(int i = 0; i < player.getTeamPokemon().size(); i++)
		{
			System.out.println("	[" + i + "] " + player.getPokemon(player, i).name);
		}
		@SuppressWarnings("resource")
		Scanner input = new Scanner(System.in);
		int pokeChoice = -1;
		
		try
		{
			pokeChoice = input.nextInt();
		}
		catch(Exception exc)
		{
			input.nextLine();
		}
		
		if (pokeChoice >= 0 && pokeChoice < player.getTeamPokemon().size())
		{
			Pokemon pokemon = player.getPokemon(player, pokeChoice);
			pokemon.giveItem(item, player);
			player.removeItem(itemIndex);
		}
	}
	
	/**
	 * Plays music for the intro and team management. 
	 * Song: Black and White Route 10 dubstep remix by GlitchxCity.
	 * @return Clip
	 */
	private static Clip playMusic() 
	{
		Clip clip = null;
	    try 
	    {
	        AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File("bgm/intro.wav").getAbsoluteFile());
	        clip = AudioSystem.getClip();
	        clip.open(audioInputStream);
	        clip.loop(Clip.LOOP_CONTINUOUSLY);
	        FloatControl gainControl = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
	        gainControl.setValue(-10.0f); // Reduce volume by 10 dB
	        clip.start();
	    } 
	    catch(Exception ex) 
	    {
	        System.out.println("Error with playing sound.");
	        ex.printStackTrace();
	    }
	    return clip;
	}

	/**
	 * Prints "Pokemon" in ASCII - because a logo is important for a game!
	 */
	public static void logoASCII()
	{
		System.out.println("                                  ,'\\");
		System.out.println("    _.----.        ____         ,'  _\\   ___    ___     ____");
		System.out.println("_,-'       `.     |    |  /`.   \\,-'    |   \\  /   |   |    \\  |`.");
		System.out.println("\\      __    \\    '-.  | /   `.  ___    |    \\/    |   '-.   \\ |  |");
		System.out.println(" \\.    \\ \\   |  __  |  |/    ,','_  `.  |          | __  |    \\|  |");
		System.out.println("   \\    \\/   /,' _`.|      ,' / / / /   |          ,' _`.|     |  |");
		System.out.println("    \\     ,-'/  /   \\    ,'   | \\/ / ,`.|         /  /   \\  |     |");
		System.out.println("     \\    \\ |   \\_/  |   `-.  \\    `'  /|  |    ||   \\_/  | |\\    |");
		System.out.println("      \\    \\ \\      /       `-.`.___,-' |  |\\  /| \\      /  | |   |");
		System.out.println("       \\    \\ `.__,'|  |`-._    `|      |__| \\/ |  `.__,'|  | |   |");
		System.out.println("        \\_.-'       |__|    `-._ |              '-.|     '-.| |   |");
		System.out.println("                                `'                            '-._|\n");
	}
	
	/**
	 * Prints "GAME OVER" in ASCII - mostly to look cool.
	 */
	public static void gameOverASCII()
	{
		System.out.println(" ██████╗  █████╗ ███╗   ███╗███████╗     ██████╗ ██╗   ██╗███████╗██████╗ ");
		System.out.println(" ██╔════╝ ██╔══██╗████╗ ████║██╔════╝    ██╔═══██╗██║   ██║██╔════╝██╔══██╗");
		System.out.println(" ██║  ███╗███████║██╔████╔██║█████╗      ██║   ██║██║   ██║█████╗  ██████╔╝");
		System.out.println(" ██║   ██║██╔══██║██║╚██╔╝██║██╔══╝      ██║   ██║╚██╗ ██╔╝██╔══╝  ██╔══██╗");
		System.out.println(" ╚██████╔╝██║  ██║██║ ╚═╝ ██║███████╗    ╚██████╔╝ ╚████╔╝ ███████╗██║  ██║");
		System.out.println("  ╚═════╝ ╚═╝  ╚═╝╚═╝     ╚═╝╚══════╝     ╚═════╝   ╚═══╝  ╚══════╝╚═╝  ╚═╝");                                                                          
	}
}
