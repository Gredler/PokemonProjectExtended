import java.util.ArrayList;
import java.util.Scanner;

/**
 * The Player class is used to determine your player character. Gender, name, your bag and your Pokemon list are inside this class.
 * 
 * @version 1.7
 * @author Andi
 */
public class Player 
{
	private String name;
	private String gender;
	private ArrayList<Items> bag = new ArrayList<>();
	private ArrayList<Pokemon> teamPokemon = new ArrayList<>();
	
	/**
	 * Construktor of this Class. Adds a bunch of items to the player's bag.
	 * @param name
	 * @param gender
	 */
	public Player(String name, String gender)
	{
		this.name = name;
		this.gender = gender;
		
		bag.add(Items.POTION);
		bag.add(Items.SUPERPOTION);
		bag.add(Items.ULTRAPOTION);
		bag.add(Items.HYPERPOTION);
		bag.add(Items.FULLRESTORE);
		bag.add(Items.POKEBALL);
		bag.add(Items.GREATBALL);
		bag.add(Items.ULTRABALL);
		bag.add(Items.MASTERBALL);
		bag.add(Items.THUNDERSTONE);
	}

	/**
	 * Returns the name of the Player.
	 * @return String
	 */
	public String getName() 
	{
		return name;
	}

	/**
	 * Sets the name of the player to the give String.
	 * @param name
	 */
	public void setName(String name) 
	{
		this.name = name;
	}

	/**
	 * Returns the gender of the Player.
	 * @return String
	 */
	public String getGender() 
	{
		return gender;
	}

	/**
	 * Sets the gender of the Player to the give String.
	 * @param gender
	 */
	public void setGender(String gender) 
	{
		this.gender = gender;
	}
	
	/**
	 * Prints out all the items inside the player's bag with their index in brackets [index].
	 */
	public void showBag()
	{
		System.out.println("Bag:");
		for (int i = 0; i < bag.size(); i++)
		{
			System.out.println("	[" + i + "] " + bag.get(i).getName());
		}
		System.out.println("Exit");
	}
	
	/**
	 * Returns the item of the given index.
	 * @param item
	 * @return Items
	 */
	public Items getItem(int item)
	{
		return bag.get(item);
	}
	
	/**
	 * Returns the size of the player's bag.
	 * @return int
	 */
	public int getBagSize()
	{
		return bag.size();
	}
	
	/**
	 * Removes the item with the given index from the player's bag.
	 * @param item
	 */
	public void removeItem(int item)
	{
		bag.remove(item);
	}
	
	/**
	 * Adds a Pokemon to the player's Pokemon list. If the player has more than 6 pokemon they will have to remove one of their Pokemon.
	 * @param pokemon
	 */
	public void addPokemon(Pokemon pokemon)
	{
		if (teamPokemon.size() < 6) teamPokemon.add(pokemon);
		else 
		{
			String replacement;
			
			@SuppressWarnings("resource")
			Scanner input = new Scanner(System.in);
			System.out.println("Which Pokemon would you like to replace? ");
			for(int i = 0; i < teamPokemon.size(); i++)
			{
				if (teamPokemon.get(i).name.contentEquals(teamPokemon.get(i).speciesName)) System.out.println("[" + i + "] " + teamPokemon.get(i).name + " Level " + teamPokemon.get(i).level);
				else System.out.println("[" + i + "] " + teamPokemon.get(i).speciesName + " (" + teamPokemon.get(i).name + ")" + " Level " + teamPokemon.get(i).level);
			}
			do
			{
				replacement = input.nextLine();
			}
			while (!replacement.contentEquals("0") && !replacement.contentEquals("1") && !replacement.contentEquals("2") && !replacement.contentEquals("3") && !replacement.contentEquals("4") && !replacement.contentEquals("5"));
			int rep = Integer.parseInt(replacement);
			teamPokemon.remove(rep);
			teamPokemon.add(pokemon);
		}
	}
	
	/**
	 * Manages the player's Pokemon team. This includes: Showing their status, giving/taking/using items and changing the lead Pokemon.
	 * @param player
	 */
	public void managePokemonTeam()
	{
//		for (Pokemon pokemon : teamPokemon)
//		{
//			if (pokemon.name.equals(pokemon.speciesName)) System.out.println(pokemon.speciesName + " Level " + pokemon.level);
//			else System.out.println(pokemon.speciesName + " (" + pokemon.name + ") Level " + pokemon.level);
//		}
		Player player = this;
		
		int pokeChoice = 10;
		do
		{
			for (int i = 0; i < teamPokemon.size(); i++)
			{
				if (teamPokemon.get(i).name.contentEquals(teamPokemon.get(i).speciesName)) System.out.println("[" + i + "] " + teamPokemon.get(i).speciesName + " Level " + teamPokemon.get(i).level);
				else System.out.println("[" + i + "] " + teamPokemon.get(i).speciesName + " (" + teamPokemon.get(i).name + ") Level " + teamPokemon.get(i).level);
			}
			System.out.println("[" + player.teamPokemon.size() + "] Cancel");
			
			@SuppressWarnings("resource")
			Scanner input = new Scanner(System.in);
			
			try
			{
				pokeChoice = input.nextInt();
			}
			catch(Exception exc)
			{
				input.nextLine();
			}
			if (pokeChoice < teamPokemon.size() && pokeChoice >= 0)
			{
				Pokemon chosenPokemon = player.getPokemon(player, pokeChoice);
				
				int q = -1;
				
				System.out.println("What would you like to do?");
				System.out.println("	[0] Status");
				System.out.println("	[1] Move info");
				System.out.println("	[2] Item");
				System.out.println("	[3] Make Lead");
				System.out.println("	[4] Cancel");
				
				try
				{
					q = input.nextInt();
				}
				catch(Exception exc)
				{
					input.nextLine();
				}
				
				switch(q)
				{
					case 0: chosenPokemon.getStats();
						break;
					case 1:
						for(MoveList move : chosenPokemon.moves)
						{
							System.out.println(move.getName() + ":");
							System.out.println("	Element: " + move.getElement());
							System.out.println("	Type: " + move.getMoveType());
							if (move.getPower() != 0) System.out.println("	Power: " + move.getPower());
							if (move.getAccuracy() <= 1) System.out.println("	Accuracy: " + (int)(move.getAccuracy() * 100) + "%");
							else System.out.println("	Accuracy: -");
							System.out.println("	PP: " + move.getPp());
							System.out.println("	" + move.getDescription());
						}
						break;
					case 2: System.out.println("	[0] Give");
						System.out.println("	[1] Take");
						System.out.println("	[2] Info");
						System.out.println("	[3] Use");
						System.out.println("	[4] Cancel");
						
						int it = -1;
						try
						{
							it = input.nextInt();
						}
						catch(Exception exc)
						{
							input.nextLine();
						}
						
						if(it == 0) 
						{
							System.out.println("Which item would you like to give to " + chosenPokemon.name + "?");
							for(int i = 0; i < player.bag.size(); i++)
							{
								System.out.println("	[" + i + "] " + player.bag.get(i));
							}
							
							int itemChoice = -1;
							try
							{
								itemChoice = input.nextInt();
							}
							catch(Exception exc)
							{
								input.nextLine();
							}
							if(itemChoice >= 0 && itemChoice < player.bag.size()) 
							{
								chosenPokemon.giveItem(player.getItem(itemChoice), player);
								player.removeItem(itemChoice);
							}
						}
						else if (it == 1)
						{
							chosenPokemon.takeItem(player);
						}
						else if (it == 2)
						{
							if (chosenPokemon.heldItem != Items.NONE)
							{
								System.out.print(chosenPokemon.heldItem.getName());
								System.out.println(": " + chosenPokemon.heldItem.getDescription());
							}
						}
						else if (it == 3)
						{
							System.out.println("Which item would you like to use on " + chosenPokemon.name + "?");
							for(int i = 0; i < player.bag.size(); i++)
							{
								System.out.println("	[" + i + "] " + player.bag.get(i));
							}
							
							int itemChoice = -1;
							try
							{
								itemChoice = input.nextInt();
							}
							catch(Exception exc)
							{
								input.nextLine();
							}
							if(itemChoice >= 0 && itemChoice < player.bag.size()) 
							{
								useItem(player.getItem(itemChoice), chosenPokemon, itemChoice);
							}
						}
						
						break;
					case 3: Pokemon currentLead = player.getPokemon(player, 0);
						player.addPokemon(currentLead);
						player.setPokemon(player.getPokemon(player, pokeChoice), 0);				
						player.removePokemon(pokeChoice);
						break;
					default:
						break;
				}
			}
		}
		while(pokeChoice < player.teamPokemon.size() && pokeChoice >= 0);
		
//		System.out.print("\nWould you like to look at your Pokemon's stats? ");
//		String q = input.nextLine();
//		if(q.equals("yes"))
//		{
//			for (Pokemon pokemon : teamPokemon)
//			{
//				pokemon.getStats(pokemon);
//			}
//		}
	}
	
	private void useItem(Items item, Pokemon pokemon, int itemNumber)
	{
		if(item.getValue() != 5) System.out.println("This won't have any effect!\n");
		else 
		{
			if(pokemon.canEvolve(item)) 
			{
				int pokeChoice = -1;
				
				for(int i = 0; i < this.getTeamPokemon().size(); i++)
				{
					if(this.getPokemon(this, i) == pokemon) pokeChoice = i;
				}
				
				if(pokeChoice != -1)
				{
					pokemon = pokemon.evolve(item);
					this.setPokemon(pokemon, pokeChoice);
					this.removeItem(itemNumber);	
				}
			}
			else System.out.println("This won't have any effect!\n");
		}
	}
	
	/**
	 * Adds an Item to the player's bag.
	 * @param item
	 */
	public void addItem(Items item)
	{
		this.bag.add(item);
	}
	
	/**
	 * Switches Pokemon in combat.
	 * @param player
	 * @return Pokemon
	 */
	public Pokemon switchPokemon(Player player)
	{
		@SuppressWarnings("resource")
		Scanner input = new Scanner(System.in);
		
		if (player.teamPokemon.size() > 1)
		{
			System.out.println("\nWhich Pokemon would you like to switch into? ");
			
			for (int i = 0; i < player.teamPokemon.size(); i++)
			{
				if (teamPokemon.get(i).status != Status.FAINTED)
				{
					if (teamPokemon.get(i).name.equals(teamPokemon.get(i).speciesName)) System.out.println("	[" + i + "] " + teamPokemon.get(i).speciesName + " Level " + teamPokemon.get(i).level + " | Status: " + teamPokemon.get(i).status + " | " + teamPokemon.get(i).currentHP + "/" + teamPokemon.get(i).stats.get(0) + " HP");
					else System.out.println("	[" + i + "] " + teamPokemon.get(i).speciesName + " (" + teamPokemon.get(i).name + ") Level " + teamPokemon.get(i).level + " | Status: " + teamPokemon.get(i).status + " | " + teamPokemon.get(i).currentHP + "/" + teamPokemon.get(i).stats.get(0) + " HP");
				}
			}
			
			Pokemon chosenPokemon = null;
			
			int pokeChoice = -1;
			try
			{
				pokeChoice = input.nextInt();
			}
			catch(Exception exc)
			{
				input.nextLine();
			}
			
			if (pokeChoice != -1)
			{
				try
				{
					chosenPokemon = player.getPokemon(player, pokeChoice);	
				}
				catch (Exception exc)
				{
					return null;
				}
			}
			return chosenPokemon;
		}
		else 
		{
			System.out.println("You only have 1 Pokemon!");
			return null;
		}
	}
	
	/**
	 * Returns the player's list of Pokemon.
	 * @return ArrayList<Pokemon>
	 */
	public ArrayList<Pokemon> getTeamPokemon()
	{
		return teamPokemon;
	}
	
	/**
	 * Removes the Pokemon with the given index.
	 * @param index
	 */
	public void removePokemon(int index)
	{
		teamPokemon.remove(index);
	}
	
	/**
	 * Sets the Pokemon on the given index to the new Pokemon. (The old Pokemon will be deleted.)
	 * @param pokemon
	 * @param index
	 */
	public void setPokemon(Pokemon pokemon, int index)
	{
		teamPokemon.set(index, pokemon);
	}
	
	/**
	 * Returns the current lead Pokemon.
	 * @param player
	 * @return Pokemon
	 */
	public Pokemon getLeadPokemon(Player player)
	{
		return player.teamPokemon.get(0);
	}
	
	/**
	 * Returns the Pokemon with the index pokeChoice.
	 * @param player
	 * @param pokeChoice
	 * @return Pokemon
	 */
	public Pokemon getPokemon(Player player, int pokeChoice)
	{
		return player.teamPokemon.get(pokeChoice);
	}
	
	/**
	 * Returns false if the player still has Pokemon that aren't fainted left. Otherwise return true.
	 * @param player
	 * @return boolean
	 */
	public boolean checkPokemonAvailability()
	{
		for (Pokemon pokemon : this.teamPokemon)
		{
			if(pokemon.status != Status.FAINTED) return false;
		}
		return true;
	}
	
	/**
	 * Heals all of the player's Pokemon.
	 * @param player
	 */
	public void healPokemon()
	{
		for (int i = 0; i < teamPokemon.size(); i++)
		{
			teamPokemon.get(i).currentHP = teamPokemon.get(i).stats.get(0);
			teamPokemon.get(i).status = Status.NORMAL;
			teamPokemon.get(i).protect = false;
			teamPokemon.get(i).confusion = null;
			teamPokemon.get(i).previousTurnProtect = false;
			teamPokemon.get(i).confusionTimer = 1;
			teamPokemon.get(i).sleepTimer = 1;
			teamPokemon.get(i).trapped = false;
			teamPokemon.get(i).continualDamageTimer = 1;
			teamPokemon.get(i).flinched = false;
			teamPokemon.get(i).currentAbility = teamPokemon.get(i).ability;
			teamPokemon.get(i).leechSeed = false;
			teamPokemon.get(i).flashFire = false;
			teamPokemon.get(i).isCharging = false;
			teamPokemon.get(i).isRecharging = false;
			teamPokemon.get(i).previousMove = -1;
			teamPokemon.get(i).hasPriority = false;
			teamPokemon.get(i).resetStatStages();
		}
	}
}
