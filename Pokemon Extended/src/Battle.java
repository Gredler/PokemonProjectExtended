import java.io.File;
import java.util.Random;
import java.util.Scanner;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.FloatControl;

/**
 * Runs a Pokemon battle.
 * @author Andi
 * @version 9.7
 */
public class Battle
{
	Scanner input = new Scanner(System.in);
	private static int escapeAttempts;
	private static Weather weather = Weather.NONE;
	private static boolean lightScreenPl, lightScreenOp, reflectPl, reflectOp;
	private static int lightScreenPlTimer, lightScreenOpTimer, reflectOpTimer, reflectPlTimer;
	
	/**
	 * Sets the static field variables to 0/false.
	 */
	private static void setFieldVariables()
	{
		lightScreenPl = false;
		lightScreenOp = false;
		reflectOp = false;
		reflectPl = false;
		lightScreenPlTimer = 0;
		lightScreenOpTimer = 0;
		reflectOpTimer = 0;
		reflectPlTimer = 0;
	}
	
	/**
	 * Generates a wild Pokemon, initiates and manages combat. 
	 * @param player
	 */
	@SuppressWarnings("resource")
	public void combat(Player player)
	{	
		Clip clip = null;
		clip = playBattleMusic();
		setFieldVariables();
		boolean run = false;
		boolean plAttack = false;
		boolean outOfPokemon = false;

		Pokemon opponent;
		
		Random randomNumber = new Random();
		int rE = randomNumber.nextInt(5);
		int opLvl = 20;
		if (rE == 0)
		{
			opponent = new Pikachu();
			opponent.generatePokemon(opLvl);
		}
		else if (rE == 1)
		{
			opponent = new Bulbasaur();
			opponent.generatePokemon(opLvl);
		}
		else if (rE == 2)
		{
			opponent = new Squirtle();
			opponent.generatePokemon(opLvl);
		}
		else if (rE == 3)
		{
			opponent = new Eevee();
			opponent.generatePokemon(opLvl);
		}
		else 
		{
			opponent = new Charmander();
			opponent.generatePokemon(opLvl);
		}
		
		double rI = randomNumber.nextDouble();
		if(rI >= 0.99) opponent.heldItem = Items.MASTERBALL;
		else if (rI >= 0.94) opponent.heldItem = Items.THUNDERSTONE;
		else if (rI >= 0.89) opponent.heldItem = Items.WATERSTONE;
		else if (rI >= 0.84) opponent.heldItem = Items.FIRESTONE;
		else if (rI >= 0.79) opponent.heldItem = Items.FULLRESTORE;
		else if (rI >= 0.7) opponent.heldItem = Items.HYPERPOTION;
		else opponent.heldItem = Items.NONE;
		
		Pokemon playerPokemon = player.getLeadPokemon(player);
		
		playerPokemon.LvlMoves();
		
		int plSPE = playerPokemon.stats.get(5);
		int opSPE = opponent.stats.get(5);
		
		if (opponent.name.equals("Pikachu")) pikachuASCII();
		
		System.out.println("A wild " + opponent.name + " appeared!");
		
		System.out.println(player.getName() + " sent out " + playerPokemon.name + "!\n");
		
		checkStartWeather();
		
		plSPE = (int)(playerPokemon.stats.get(5) * getStatModifier(playerPokemon, 4));
		if(playerPokemon.status == Status.PARALYSIS) plSPE /= 2; 
		
		opSPE = (int)(opponent.stats.get(5) * getStatModifier(opponent, 4));
		if(opponent.status == Status.PARALYSIS) opSPE /= 2; 
		
		if (plSPE > opSPE)
		{
			checkAbility(opponent, playerPokemon, false);
			checkAbility(playerPokemon, opponent, true);
		}
		else if (plSPE > opSPE)
		{
			checkAbility(playerPokemon, opponent, true);
			checkAbility(opponent, playerPokemon, false);
		}
		else
		{
			double startSpCheck = randomNumber.nextDouble();
			if (startSpCheck > 0.5)
			{
				checkAbility(playerPokemon, opponent, true);
				checkAbility(opponent, playerPokemon, false);
			}
			else 
			{
				checkAbility(opponent, playerPokemon, false);
				checkAbility(playerPokemon, opponent, true);
			}
		}
		
		do
		{
			if (playerPokemon.isCharging == false && playerPokemon.isRecharging == false)
			{
				System.out.println("What will you do? ");
				System.out.print("	[A]ttack");
				System.out.println("	[B]ag");
				System.out.print("	[S]witch");
				System.out.println("	[R]un");
				System.out.println("	[F]ield\n");
				
				String text = input.next().toLowerCase();
				
				playerPokemon.flinched = false;
				opponent.flinched = false;
				
				plSPE = (int)(playerPokemon.stats.get(5) * getStatModifier(playerPokemon, 4));
				if(playerPokemon.status == Status.PARALYSIS) plSPE /= 2; 
				
				opSPE = (int)(opponent.stats.get(5) * getStatModifier(opponent, 4));
				if(opponent.status == Status.PARALYSIS) opSPE /= 2; 
				
				if (text.equals("a"))
				{	
					attackChosen(playerPokemon, opponent, plAttack);
				}
				else if (text.equals("b"))
				{
					bagChosen(player, playerPokemon, opponent, plAttack);
				}
				else if (text.equals("s"))
				{
					playerPokemon = switchChosen(playerPokemon, opponent, player, plAttack);
				}
				else if (text.equals("r")) 
				{
					run = runAway(playerPokemon, opponent, plSPE, opSPE, plAttack, run);
				}
				else if (text.equals("f"))
				{
					if (weather != Weather.NONE) System.out.println("Weather: " + weather.getName());
					System.out.println("Player: " + playerPokemon.name + " | Status: " + playerPokemon.status + " | " + playerPokemon.currentHP + "/" + playerPokemon.stats.get(0) + " HP");
					playerPokemon.showStatChanges();
					if (reflectPl) System.out.println("	Reflect: " + reflectPlTimer + "/5");
					if (lightScreenPl) System.out.println("	Light Screen: " + lightScreenPlTimer + "/5");
					if (playerPokemon.leechSeed) System.out.println("	Leech Seed");
					System.out.println("Opponent: " + opponent.name + " | Status: " + opponent.status + " | " + opponent.currentHP + "/" + opponent.stats.get(0) + " HP");
					opponent.showStatChanges();
					if (reflectOp) System.out.println("	Reflect: " + reflectOpTimer + "/5");
					if (lightScreenOp) System.out.println("	Light Screen: " + lightScreenOpTimer + "/5");
					if (opponent.leechSeed) System.out.println("	Leech Seed");
				}
				
	
				if (playerPokemon.currentHP <= 0 && opponent.currentHP > 0)
				{
					playerPokemon.status = Status.FAINTED;
					playerPokemon.currentHP = 0;
					outOfPokemon = player.checkPokemonAvailability();
					if (outOfPokemon == false)
					{
						Pokemon chosenPokemon = null;
						do
						{
							chosenPokemon = player.switchPokemon(player);
							if (chosenPokemon == playerPokemon)
							{
								System.out.println("This Pokemon is already in battle!");
								chosenPokemon = null;
							}
						}
						while (chosenPokemon == null);
						
						System.out.println("Return " + playerPokemon.name);
						System.out.println(player.getName() + " sent out " + chosenPokemon.name + "!\n");
						playerPokemon = chosenPokemon;
						
						checkAbility(playerPokemon, opponent, true);
					}
				}
				
				if (opponent.currentHP <= 0)
				{
					clip.stop();
					
					clip = playVictoryMusic();
					
					if (playerPokemon.level <= 100)
					{
						playerPokemon.level++;
						System.out.println();
						System.out.println(playerPokemon.name + " grew to Level " + playerPokemon.level + "!");
						
						for (int i = 0; i < player.getTeamPokemon().size(); i++)
						{
							if(player.getPokemon(player, i) == playerPokemon) 
							{
								player.setPokemon(playerPokemon.levelUp(), i);
							}
						}
					}
					
					if(playerPokemon.evSum < 510)
					{
						for(int i = 0; i < playerPokemon.ev.size(); i++)
						{
							int plEvs = playerPokemon.ev.get(i);
							int evGrowth = opponent.effortPoints.get(i);
							int newEv = plEvs + evGrowth;
							int newEvSum = playerPokemon.evSum + newEv;
							
							if (newEv <= 252 && newEvSum <= 510) playerPokemon.ev.set(i, newEv);
							else if (newEv <= 252 && newEvSum > 510)
							{
								playerPokemon.ev.set(i, newEvSum-playerPokemon.evSum);
							}
							else if (newEv > 252 && newEvSum <= 510)
							{
								playerPokemon.ev.set(i, 252);
							}
							else;
						}
						
						playerPokemon.calculateNewStats();
					}
					
					for (int i = 0; i < playerPokemon.pokemonStatusStages.size(); i++)
					{
						playerPokemon.pokemonStatusStages.set(i, 0);
					}
				}
			}
			else if (playerPokemon.isCharging)
			{
				int oppMove = opponentChooseMove(opponent);
				int moveChoice = playerPokemon.previousMove;
				
				opponent.previousMove = oppMove;
				
				setPriority(playerPokemon, opponent, playerPokemon.moves.get(moveChoice), opponent.moves.get(oppMove));
				
				if(playerPokemon.hasPriority)
				{
					plAttack = true;
					if(playerPokemon.moves.get(moveChoice).getExtra() != MovesExtra.DOUBLE_HIT && playerPokemon.moves.get(moveChoice).getExtra() != MovesExtra.MULTI_HIT) pokemonAttack(playerPokemon, opponent, moveChoice, plAttack);
					else useMultiHittingMove(playerPokemon, opponent, moveChoice, plAttack);
					plAttack = false;
					
					if (opponent.currentHP > 0) 
					{
						if(opponent.moves.get(oppMove).getExtra() != MovesExtra.DOUBLE_HIT && opponent.moves.get(oppMove).getExtra() != MovesExtra.MULTI_HIT) pokemonAttack(opponent, playerPokemon, oppMove, plAttack);
						else useMultiHittingMove(opponent, playerPokemon, oppMove, plAttack);
					}
				}
				else
				{
					if(opponent.moves.get(oppMove).getExtra() != MovesExtra.DOUBLE_HIT && opponent.moves.get(oppMove).getExtra() != MovesExtra.MULTI_HIT) pokemonAttack(opponent, playerPokemon, oppMove, plAttack);
					else useMultiHittingMove(opponent, playerPokemon, oppMove, plAttack);
					
					if (playerPokemon.currentHP > 0) 
					{
						plAttack = true;
						if(playerPokemon.moves.get(moveChoice).getExtra() != MovesExtra.DOUBLE_HIT && playerPokemon.moves.get(moveChoice).getExtra() != MovesExtra.MULTI_HIT) pokemonAttack(playerPokemon, opponent, moveChoice, plAttack);
						else useMultiHittingMove(playerPokemon, opponent, moveChoice, plAttack);
						plAttack = false;
					}
				}
				if (playerPokemon.currentHP > 0) endOfTurn(playerPokemon, opponent, true);
				if (opponent.currentHP > 0) endOfTurn(opponent, playerPokemon, false);
				playerPokemon.isCharging = false;
			}
			else
			{
				playerPokemon.previousMove = -1;
				plAttack = false;
				int oppMove = opponentChooseMove(opponent);
				
				setPriority(playerPokemon, opponent, MoveList.HYPER_BEAM, opponent.moves.get(oppMove));
				
				if(playerPokemon.hasPriority)
				{
					System.out.println(playerPokemon.name + " is recharging!");
					if(opponent.moves.get(oppMove).getExtra() != MovesExtra.DOUBLE_HIT && opponent.moves.get(oppMove).getExtra() != MovesExtra.MULTI_HIT) pokemonAttack(opponent, playerPokemon, oppMove, plAttack);
					else useMultiHittingMove(opponent, playerPokemon, oppMove, plAttack);
				}
				else
				{
					if(opponent.moves.get(oppMove).getExtra() != MovesExtra.DOUBLE_HIT && opponent.moves.get(oppMove).getExtra() != MovesExtra.MULTI_HIT) pokemonAttack(opponent, playerPokemon, oppMove, plAttack);
					else useMultiHittingMove(opponent, playerPokemon, oppMove, plAttack);
					
					if (playerPokemon.currentHP > 0) if (playerPokemon.currentHP >= 0) System.out.println(playerPokemon.name + " is recharging!");
				}
				
				if (playerPokemon.currentHP > 0) endOfTurn(playerPokemon, opponent, true);
				if (opponent.currentHP > 0) endOfTurn(opponent, playerPokemon, false);
				playerPokemon.isRecharging = false;
			}
		}
		while (opponent.currentHP > 0 && playerPokemon.currentHP > 0 && run == false);
		clip.close();
	}
	
	/**
	 * Performs an attack, if the player gives a valid number as their chosen move.
	 * @param pokemon - the Pokemon the player is controlling
	 * @param opponent - the opposing Pokemon
	 * @param plAttack
	 */
	private static void attackChosen(Pokemon pokemon, Pokemon opponent, boolean plAttack)
	{
		@SuppressWarnings("resource")
		Scanner input = new Scanner(System.in);
		
		int moveChoice = 4;
		pokemon.showMoves(pokemon);
		try
		{
			moveChoice = input.nextInt();
		}
		catch (Exception exc)
		{
			exc.getMessage();
		}
		
		int oppMove = opponentChooseMove(opponent);
		
		if (moveChoice == 0 || moveChoice == 1 || moveChoice == 2 || moveChoice == 3)
		{	
			opponent.previousMove = oppMove;
			pokemon.previousMove = moveChoice;
			
			setPriority(pokemon, opponent, pokemon.moves.get(moveChoice), opponent.moves.get(oppMove));
			
			if(pokemon.hasPriority)
			{
				plAttack = true;
				if(pokemon.moves.get(moveChoice).getExtra() != MovesExtra.DOUBLE_HIT && pokemon.moves.get(moveChoice).getExtra() != MovesExtra.MULTI_HIT) pokemonAttack(pokemon, opponent, moveChoice, plAttack);
				else useMultiHittingMove(pokemon, opponent, moveChoice, plAttack);
				plAttack = false;
				
				if (opponent.currentHP > 0) 
				{
					if(opponent.moves.get(oppMove).getExtra() != MovesExtra.DOUBLE_HIT && opponent.moves.get(oppMove).getExtra() != MovesExtra.MULTI_HIT) pokemonAttack(opponent, pokemon, oppMove, plAttack);
					else useMultiHittingMove(opponent, pokemon, oppMove, plAttack);
				}
			}
			else
			{
				if(opponent.moves.get(oppMove).getExtra() != MovesExtra.DOUBLE_HIT && opponent.moves.get(oppMove).getExtra() != MovesExtra.MULTI_HIT) pokemonAttack(opponent, pokemon, oppMove, plAttack);
				else useMultiHittingMove(opponent, pokemon, oppMove, plAttack);
				
				if (pokemon.currentHP > 0) 
				{
					plAttack = true;
					if(pokemon.moves.get(moveChoice).getExtra() != MovesExtra.DOUBLE_HIT && pokemon.moves.get(moveChoice).getExtra() != MovesExtra.MULTI_HIT) pokemonAttack(pokemon, opponent, moveChoice, plAttack);
					else useMultiHittingMove(pokemon, opponent, moveChoice, plAttack);
					plAttack = false;
				}
			}
			if (pokemon.currentHP > 0) endOfTurn(pokemon, opponent, true);
			if (opponent.currentHP > 0) endOfTurn(opponent, pokemon, false);
		}
		else input.nextLine(); // consume input, so I don't get asked what i want to do twice!
	}
		
	/**
	 * Makes the pokemon attack with its "moveChoice".
	 * @param pokemon - the attacking Pokemon
	 * @param opponent - the opposing Pokemon
	 * @param moveChoice
	 * @param plAttack
	 */
	private static void pokemonAttack(Pokemon pokemon, Pokemon opponent, int moveChoice, boolean plAttack)
	{
		double damageCalc;
		boolean crit = false;
		boolean fullyParalyzed = false;
		int move;
		double m = (Math.random());
		double weatherModifier;
		double abilityMoveModifier;
		MoveList chosenMove;
		chosenMove = pokemon.moves.get(moveChoice);
		
		move = calculateMoveDamage(pokemon, opponent, chosenMove);
				
		abilityMoveModifier = getAbilityMoveModifier(pokemon, chosenMove);
		move *= abilityMoveModifier;
		
		
		weatherModifier = getWeatherModifier(chosenMove.getElement());
		if (weather == Weather.SANDSTORM && chosenMove == MoveList.SOLAR_BEAM) weatherModifier = 0.5;
		
		move *= weatherModifier;
		
		Random rc = new Random();
		if (pokemon.status == Status.FROZEN)
		{
			int thawOut = rc.nextInt(100);
			if (thawOut >= 80)
			{
				if (plAttack) System.out.println(pokemon.name + " thawed out!");
				else System.out.println("The opposing " + pokemon.name + " thawed out!");
				pokemon.status = Status.NORMAL;
			}
			else
			{
				if (plAttack) System.out.println(pokemon.name + " is frozen!\n");
				else System.out.println("The opposing " + pokemon.name + " is frozen!\n");
			}
		}
		
		if (pokemon.status == Status.PARALYSIS)
		{
			int para = rc.nextInt(100) + 1;
			if (para >= 25) fullyParalyzed = false;
			else 
			{
				fullyParalyzed = true;
				
				if (plAttack) System.out.println(pokemon.name + " is fully paralyzed!\n");
				else System.out.println("The opposing " + pokemon.name + " is fully paralyzed!\n");
			}
		}
		
		if (pokemon.status == Status.SLEEP)
		{
			int sleep = rc.nextInt(3) + 1;
			if (sleep > pokemon.sleepTimer)
			{
				if (plAttack) System.out.println(pokemon.name + " is asleep!\n");
				else System.out.println("The opposing " + pokemon.name + " is asleep!\n");
				
				pokemon.sleepTimer++;
			}
			else
			{
				if (plAttack) System.out.println(pokemon.name + " woke up!");
				else System.out.println("The opposing " + pokemon.name + " woke up!");
				
				pokemon.status = Status.NORMAL;
				pokemon.sleepTimer = 0;
			}
		}
		
		if (pokemon.status == Status.REST)
		{
			if (pokemon.sleepTimer < 3)
			{
				if (plAttack) System.out.println(pokemon.name + " is asleep!\n");
				else System.out.println("The opposing " + pokemon.name + " is asleep!\n");
				
				pokemon.sleepTimer++;
			}
			else
			{
				if (plAttack) System.out.println(pokemon.name + " woke up!");
				else System.out.println("The opposing " + pokemon.name + " woke up!");
				
				pokemon.status = Status.NORMAL;
			}
		}
		
		if (pokemon.status != Status.FROZEN && pokemon.status != Status.SLEEP && fullyParalyzed == false && pokemon.status != Status.REST)
		{
			if(pokemon.confusion == Status.CONFUSION)
			{
				pokemon.confusionTimer++;
				
				if (plAttack) System.out.println(pokemon.name + " is confused.");
				else System.out.println("The opposing " + pokemon.name + " is confused.");
			}
			
			int snapOut = rc.nextInt(4);
			if (pokemon.confusionTimer > snapOut && pokemon.confusion == Status.CONFUSION)
			{
				if (plAttack) System.out.println(pokemon.name + " snapped out of confusion!");
				else System.out.println("The opposing" + pokemon.name + " snapped ouf of confusion!");
				pokemon.confusion = null;
			}
			
			
			double confusion = Math.random();
			if (confusion < 0.5 && pokemon.confusion == Status.CONFUSION)
			{
				int r = (int) (Math.random() * (100 - 85)) + 85;
				double rnd = r / 100.0;
				damageCalc = (((2.0 * pokemon.level + 10.0)/250.0) * ((double)pokemon.stats.get(1) / (double)pokemon.stats.get(2)) * 40 + 2.0) * rnd;
				int damage = (int)damageCalc;
				
				pokemon.currentHP -= damage;
				
				if (plAttack)
				{
					System.out.println(pokemon.name + " hit itself in confusion!");
					System.out.println(pokemon.name + " took " + damage + " damage!");
					if (pokemon.currentHP <= 0) System.out.println(pokemon.name + " fainted!\n");
					else System.out.println(pokemon.name + " has " + pokemon.currentHP + " HP left!\n");
				}
				else
				{
					System.out.println("The opposing " + pokemon.name + " hit itself in confusion!");
					System.out.println("The opposing " + pokemon.name + " took " + damage + " damage!");
					if (pokemon.currentHP <= 0) System.out.println("The opposinig " + pokemon.name + " fainted!\n");
					else System.out.println("The opposing " + pokemon.name + " has " + pokemon.currentHP + " HP left!\n");
				}
			}
			else
			{
				if (pokemon.flinched)
				{
					if (plAttack) System.out.println(pokemon.name + " flinched!\n");
					else System.out.println("The opposing " + pokemon.name + " flinched!\n");
					pokemon.flinched = false;
				}
				else
				{
					if (pokemon.isCharging == false && chosenMove.getExtra() == MovesExtra.CHARGE_UP_TURN) 
					{
						pokemon.isCharging = true;
						moveExtra(chosenMove, pokemon, opponent, plAttack);
					}
					else
					{
						if (plAttack) System.out.println(pokemon.name + " used " + pokemon.getMoveName(moveChoice) + "!");
						else System.out.println("The opposing " + pokemon.name + " used " + pokemon.getMoveName(moveChoice) + "!");
						
						if (opponent.protect == true && chosenMove.attacksOpponent())
						{
							if (plAttack) System.out.println("The opposing " + opponent.name + " protected itself!\n");
							else System.out.println(opponent.name + " protected itself!\n");
						}
						else
						{
							boolean accuracyCheck;
							double moveAccuracy;
							
							if (weather == Weather.HAIL && (chosenMove == MoveList.BLIZZARD)) moveAccuracy = 10;
							else if (weather == Weather.HARSH_SUNLIGHT && (chosenMove == MoveList.THUNDER || chosenMove == MoveList.HURRICANE)) moveAccuracy = 0.5;
							else if (weather == Weather.RAIN && (chosenMove == MoveList.THUNDER) || chosenMove == MoveList.HURRICANE) moveAccuracy = 10;
							else moveAccuracy = pokemon.getMoveAccuracy(moveChoice);
							
							if(moveAccuracy <= 1) accuracyCheck = true;
							else accuracyCheck = false;
							
							double accuracy = moveAccuracy;
							accuracy *= getStatModifier(pokemon, 5);
							accuracy *= getStatModifier(opponent, 6);
							
							if ((m > accuracy) && accuracyCheck) System.out.println("The attack missed!\n");
							else
							{
								if (!pokemon.getMoveType(moveChoice).equals("status"))
								{
									if (!chosenMove.getExtra().getEffect().equals("FlatDamage"))
									{
										int r = (int) (Math.random() * (100 - 85)) + 85;
										double rnd = r / 100.0;
										
										if (chosenMove.getExtra() == MovesExtra.HIGH_CRIT)
										{
											if (Math.random() < 0.125) crit = true;
										}
										else
										{
											if(Math.random() < 0.0625) crit = true;
										}
										
										if (pokemon.getMoveType(moveChoice).equals("physical"))
										{
											double abilityAttackModifier = getAbilityAttackModifier(pokemon, chosenMove);
											
											double attack; 
											if (crit && getStatModifier(pokemon, 0) < 0) attack = pokemon.stats.get(1) * abilityAttackModifier;
											else attack = pokemon.stats.get(1) * getStatModifier(pokemon, 0) * abilityAttackModifier;
											
											if (pokemon.status == Status.BURNED) attack *= 0.5;
											
											double defence; 
											if (crit && getStatModifier(opponent, 1) > 0) defence = opponent.stats.get(2);
											else defence = opponent.stats.get(2) * getStatModifier(opponent, 1);
											
											if ((plAttack && reflectOp) || (!plAttack && reflectPl)) defence *= 2;
											
											damageCalc = (((2.0 * pokemon.level + 10.0)/250.0) * (attack / defence) * move + 2.0) * rnd;
										}
										else
										{
											double attack;
											if (crit && getStatModifier(pokemon, 3) < 0) attack = pokemon.stats.get(3);
											else attack = pokemon.stats.get(3) * getStatModifier(pokemon, 2);
											
											double defence;
											if (crit && getStatModifier(opponent, 4) > 0) defence = opponent.stats.get(4);
											else defence = opponent.stats.get(4) * getStatModifier(opponent, 3);
											
											if ((plAttack && lightScreenOp ) || (!plAttack && lightScreenPl)) defence *= 2;
											
											if ((opponent.type1 == Element.ROCK || opponent.type2 == Element.ROCK) && weather == Weather.SANDSTORM) defence *= 1.5;
											
											damageCalc = (((2.0 * pokemon.level + 10.0)/250.0) * (attack / defence) * move + 2.0) * rnd;
										}
										
										double moveTypeMultiplier = getMoveTypeMultiplier(pokemon.getMoveElement(moveChoice), opponent.type1, opponent.type2);
										
										if (moveTypeMultiplier == 4.0)
										{
											damageCalc *= moveTypeMultiplier;
											System.out.println("It's super effective!");
										}
										else if (moveTypeMultiplier == 2.0)
										{
											damageCalc *= moveTypeMultiplier;
											System.out.println("It's super effective!");
										}
										else if (moveTypeMultiplier == 0.5)
										{
											damageCalc *= moveTypeMultiplier;
											System.out.println("It's not very effective!");
										}
										else if (moveTypeMultiplier == 0.25)
										{
											damageCalc *= moveTypeMultiplier;
											System.out.println("It's not very effective!");
										}
										else if (moveTypeMultiplier == 0)
										{
											damageCalc *= moveTypeMultiplier;
											System.out.println("The attack had no effect!");
										}
										else
											damageCalc *= moveTypeMultiplier;
									
										if (pokemon.type1 == pokemon.getMoveElement(moveChoice) || pokemon.type2 == pokemon.getMoveElement(moveChoice)) 
										{
											if (pokemon.ability == Abilities.ADAPTABILITY) damageCalc *= 2;
											else damageCalc *= 1.5;
										}
										
										if (crit == true) 
										{
											damageCalc *= 1.5; 
											System.out.println("A critical hit!");
										}
										
										int damage = (int) damageCalc;
										
										opponent.currentHP -= damage;
										
										int recoil = 0;
										if (chosenMove.getExtra() == MovesExtra.RECOIL_25_PERCENT) recoil = (int)(damage * 0.25);
										
										if (plAttack == true)
										{
											System.out.println("The opposing " + opponent.name + " took " + damage + " damage!");
											
											if (recoil > 0) 
											{
												pokemon.currentHP -= recoil;
												System.out.println(pokemon.name + " took " + recoil + " damage from the recoil!");
												if (pokemon.currentHP <= 0) System.out.println(pokemon.name + " fainted!\n");
												else System.out.println(pokemon.name + " has " + pokemon.currentHP + " HP left!");
											}
											
											if (opponent.currentHP > 0) 
											{							
												if (chosenMove.getExtra() != MovesExtra.CHARGE_UP_TURN) moveExtra(chosenMove, pokemon, opponent, plAttack);
												System.out.println("The opposing " + opponent.name + " has " + opponent.currentHP + " HP left!\n");
											}
											else System.out.println("The opposing " + opponent.name + " fainted!\n");
										}
										else
										{
											System.out.println(opponent.name + " took " + damage + " damage!");
											
											if (recoil > 0) 
											{
												pokemon.currentHP -= recoil;
												System.out.println("The opposing " + pokemon.name + " took " + recoil + " damage from the recoil!");
												if (pokemon.currentHP <= 0) System.out.println("The opposing " + pokemon.name + " fainted!\n");
												else System.out.println("The opposing " + pokemon.name + " has " + pokemon.currentHP + " HP left!");
											}
											
											if (opponent.currentHP > 0) 
											{
												if (chosenMove.getExtra() != MovesExtra.CHARGE_UP_TURN) moveExtra(chosenMove, pokemon, opponent, plAttack);
												System.out.println(opponent.name + " has " + opponent.currentHP + " HP left!\n");
											}
											else System.out.println(opponent.name + " fainted!\n");
										}
									}
									else
									{
										moveExtra(chosenMove, pokemon, opponent, plAttack);
										opponent.currentHP = opponent.currentHP;
									}
								}
								else
								{
									moveExtra(chosenMove, pokemon, opponent, plAttack);
								}
							}
						}
					}
				}
			}
		}
		
		if (opponent.currentHP < 0) opponent.currentHP = 0;
		opponent.protect = false;
	}
	
	/**
	 * Opens the player's bag and uses an item if a valid item has been chosen.
	 * @param player
	 * @param pokemon
	 * @param opponent
	 * @param plAttack
	 */
	private static void bagChosen(Player player, Pokemon pokemon, Pokemon opponent, boolean plAttack)
	{
		@SuppressWarnings("resource")
		Scanner input = new Scanner(System.in);
		
		player.showBag();
		int item = -1;
		try
		{
			item = input.nextInt();
		}
		catch(Exception exc)
		{
			input.nextLine();
		}
		Items itemChoice = null;
		if (item > -1 && item < player.getBagSize()) 				
		{
			itemChoice = player.getItem(item);

			if(itemChoice.isPokeball()) 
			{
				boolean capture = capturePokemon(player, opponent, itemChoice, opponent.currentHP);
				if(capture == true)
				{
					input.nextLine();
					String nick; 
					do
					{
						System.out.print("Would you like to give your newly caught " + opponent.speciesName + " a Nickname? ");
						nick = input.nextLine();
					}
					while (!nick.equals("yes") && !nick.equals("no"));
					if (nick.equals("yes")) 
					{
						System.out.print("Which name would you like to give it? ");
						nick = input.nextLine();
						opponent.name = nick;
					}
					player.addPokemon(opponent);
					opponent.currentHP = 0;
					player.removeItem(item);
					
					if (pokemon.currentHP > 0) endOfTurn(pokemon, opponent, true);
				}
				else
				{
					plAttack = false;
					int oppMove = opponentChooseMove(opponent);
					
					if(opponent.moves.get(oppMove).getExtra() != MovesExtra.DOUBLE_HIT && opponent.moves.get(oppMove).getExtra() != MovesExtra.MULTI_HIT) pokemonAttack(opponent, pokemon, oppMove, plAttack);
					else useMultiHittingMove(opponent, pokemon, oppMove, plAttack);
					
					if (pokemon.currentHP > 0) endOfTurn(pokemon, opponent, true);
					if (opponent.currentHP > 0) endOfTurn(opponent, pokemon, false);
				}
			}
			else if (itemChoice.getValue() >= 20)
			{
				useHealingItem(player, pokemon, opponent, item);
				
				plAttack = false;
				int oppMove = opponentChooseMove(opponent);
				
				if(opponent.moves.get(oppMove).getExtra() != MovesExtra.DOUBLE_HIT && opponent.moves.get(oppMove).getExtra() != MovesExtra.MULTI_HIT) pokemonAttack(opponent, pokemon, oppMove, plAttack);
				else useMultiHittingMove(opponent, pokemon, oppMove, plAttack);
				
				if (pokemon.currentHP > 0) endOfTurn(pokemon, opponent, true);
				if (opponent.currentHP > 0) endOfTurn(opponent, pokemon, false);
			}
			else
			{
				System.out.println("This won't have any effect!");
			}
		}
	}
	
	/**
	 * Performs a standard switch initiated by the player.
	 * @param pokemon
	 * @param opponent
	 * @param player
	 * @param plAttack
	 * @return Pokemon
	 * Returns the Pokemon that has been switched in, if a switch occured. Otherwise returns the original Pokemon.
	 */
	private static Pokemon switchChosen(Pokemon pokemon, Pokemon opponent, Player player, boolean plAttack)
	{
		if(pokemon.trapped) System.out.println("Your Pokemon is trapped in!\n");
		else
		{				
			Pokemon chosenPokemon = player.switchPokemon(player);
			if (chosenPokemon != null)
			{
				if (chosenPokemon == pokemon) System.out.println("This Pokemon is already in battle!");
				else
				{
					System.out.println("Return " + pokemon.name);
					System.out.println(player.getName() + " sent out " + chosenPokemon.name + "!\n");
					pokemon.toxicTimer = 1;
					pokemon.confusion = null;
					pokemon.confusionTimer = 1;
					pokemon.previousTurnProtect = false;
					pokemon.protect = false;
					pokemon.trapped = false;
					pokemon.trappingMove = null;
					pokemon.continualDamageTimer = 1;
					pokemon.flinched = false;
					pokemon.currentAbility = pokemon.ability;
					pokemon.leechSeed = false;
					pokemon.flashFire = false;
					pokemon.isCharging = false;
					pokemon.isRecharging = false;
					pokemon.previousMove = -1;
					pokemon.hasPriority = false;
					pokemon.resetStatStages();
					
					pokemon = chosenPokemon;
					
					plAttack = false;
					int oppMove = opponentChooseMove(opponent);
					
					if(opponent.moves.get(oppMove).getExtra() != MovesExtra.DOUBLE_HIT && opponent.moves.get(oppMove).getExtra() != MovesExtra.MULTI_HIT) pokemonAttack(opponent, pokemon, oppMove, plAttack);
					else useMultiHittingMove(opponent, pokemon, oppMove, plAttack);
					
					checkAbility(pokemon, opponent, true);
					
					endOfTurn(pokemon, opponent, true);
					endOfTurn(opponent, pokemon, false);
				}
			}
		}
		return pokemon;
	}
	
	/**
	 * Heals the Pokemon with the given item.
	 * @param player
	 * @param pokemon
	 * @param opponent
	 * @param item
	 */
	private static void useHealingItem(Player player, Pokemon pokemon, Pokemon opponent, int item)
	{
		int plHP = pokemon.currentHP;
		Items chosenItem = player.getItem(item);
		System.out.println(player.getName() + " used a " + chosenItem.getName() + "!");
		
		if (chosenItem.getName().equals("Full Restore"))
		{
			plHP = pokemon.stats.get(0);
			System.out.println(pokemon.name + " was fully healed!\n");
			pokemon.status = Status.NORMAL;
			pokemon.confusion = null;
			pokemon.confusionTimer = 0;
			pokemon.toxicTimer = 0;
			player.removeItem(item);
			
			pokemon.currentHP = plHP;
		}
		else
		{
			if ((plHP + chosenItem.getValue()) <= pokemon.stats.get(0))
			{
				plHP += chosenItem.getValue();
				System.out.println(pokemon.name + " was healed by " + (int)chosenItem.getValue() + " HP!\n");
				player.removeItem(item);
			}
			else
			{
				int heal = pokemon.stats.get(0) - plHP;
				plHP += heal;
				System.out.println(pokemon.name + " was healed by " + heal + " HP!\n");
				player.removeItem(item);
			}
			
			pokemon.currentHP = plHP;
		}
	}
	
	/**
	 * The player tries to run away. Returns true if they succeed.
	 * @param pokemon
	 * @param opponent
	 * @param plSPE
	 * @param opSPE
	 * @param plAttack
	 * @param run
	 * @return boolean
	 */
	private static boolean runAway(Pokemon pokemon, Pokemon opponent, int plSPE, int opSPE, boolean plAttack, boolean run)
	{
		if (pokemon.ability == Abilities.RUN_AWAY)
		{
			run = true;
			System.out.println("You got away safely!");
		}
		else
		{
			if (pokemon.trapped) System.out.println("You can't escape!\n");
			else
			{
				escapeAttempts++;
				double f = (((plSPE * 128) / opSPE) + 30 * escapeAttempts) % 256;
				Random rnd = new Random();
				int r = rnd.nextInt(256);
				
				if (r < f)
				{
					run = true;
					System.out.println("You got away safely!");
				}
				else
				{
					run = false;
					System.out.println("Can't Escape!");
					
					plAttack = false;
					int oppMove = opponentChooseMove(opponent);
					
					if(opponent.moves.get(oppMove).getExtra() != MovesExtra.DOUBLE_HIT && opponent.moves.get(oppMove).getExtra() != MovesExtra.MULTI_HIT) pokemonAttack(opponent, pokemon, oppMove, plAttack);
					else useMultiHittingMove(opponent, pokemon, oppMove, plAttack);
					
					endOfTurn(pokemon, opponent, true);
					endOfTurn(opponent, pokemon, false);
				}
			}
		}
		return run;
	}
	
	/**
	 * Tries to capture the opposing Pokemon.
	 * @param player
	 * @param opponent
	 * @param itemChoice
	 * @param opHP
	 * @return boolean
	 */
	private static boolean capturePokemon(Player player, Pokemon opponent, Items itemChoice, int opHP)
	{
		System.out.println(player.getName() + " threw a " + itemChoice.getName() + "!");
		
		int shakes = shakeCheck(opponent, itemChoice, opHP);
		
		for(int i = 0; i < shakes; i++)
		{
			if (i != 3) System.out.println("The Pokeball shook.");
		}
		if(shakes == 4) 
		{
			System.out.println("The Pokemon has been caught!");
			return true;
		}
		else
		{
			System.out.println("The Pokemon broke free!\n");
			return false;
		}
	}
	
	/**
	 * Returns the move multiplier depending on the Element of the move and opposing Pokemon. Returns: 4/2/1/0.5/0.25/0
	 * @param attack
	 * @param enemy1
	 * @param enemy2
	 * @return double
	 */
	private static double getMoveTypeMultiplier(Element attack, Element enemy1, Element enemy2)
	{
		if (attack.isSuperEffective(attack, enemy1) && attack.isSuperEffective(attack, enemy2)) return 4.0;
		else if ((attack.isSuperEffective(attack, enemy1) && enemy2 == Element.NONE) 
				|| (attack.isSuperEffective(attack, enemy1) && (attack.isNormalEffective(attack, enemy2)) 
				|| (attack.isNormalEffective(attack, enemy1) && (attack.isSuperEffective(attack, enemy2))))) return 2.0;
		else if ((attack.isNormalEffective(attack, enemy1) && enemy2 == Element.NONE) 
				|| (attack.isNormalEffective(attack, enemy1) && (attack.isNormalEffective(attack, enemy2))) 
				|| (attack.isSuperEffective(attack, enemy1) && attack.isResistant(attack, enemy2))
				|| (attack.isResistant(attack, enemy1) && (attack.isSuperEffective(attack, enemy2)))) return 1.0;
		else if ((attack.isResistant(attack, enemy1) && enemy2 == Element.NONE)
				|| (attack.isResistant(attack, enemy1) && (attack.isNormalEffective(attack, enemy2)))
				|| (attack.isNormalEffective(attack, enemy1) && (attack.isResistant(attack, enemy2)))) return 0.5;
		else if (attack.isResistant(attack, enemy1) && attack.isResistant(attack, enemy2)) return 0.25;
		else return 0;
	}
	
	/**
	 * Chooses a random move for the opponent.
	 * @param pokemon
	 * @return int
	 */
	private static int opponentChooseMove(Pokemon pokemon)
	{
		Random rnd = new Random();
		int r = rnd.nextInt(pokemon.moves.size());
		return r;
	}
	
	/**
	 * Checks how many times the Pokeball shakes.
	 * @param opponent
	 * @param ball
	 * @param opHP
	 * @return int
	 */
	private static int shakeCheck(Pokemon opponent, Items ball, int opHP)
	{
		double a, b, check;
		Random rnd = new Random();
		int shakes = 0;
		
		if (ball.getValue() == 255) shakes = 4;
		else
		{
			double bonusStatus;
			if (opponent.status == Status.SLEEP || opponent.status == Status.FROZEN) bonusStatus = 2;
			else if (opponent.status == Status.PARALYSIS || opponent.status == Status.POISON || opponent.status == Status.BURNED) bonusStatus = 1.5;
			else bonusStatus = 1;
			a = ((3 * opponent.stats.get(0) - 2 * opHP) * opponent.rareness * ball.getValue()) / (3 * opponent.stats.get(0)) * bonusStatus;
			b = 1048560 / Math.sqrt(Math.sqrt(16711680/a));
			do
			{
				check = rnd.nextDouble()*65535;
				if (check < b) shakes++;
				else break;
			}
			while(shakes < 4);
		}
		 
		return shakes;
	}
	
	/**
	 * Executes the extra effect of a pokemon's move.
	 * @param move
	 * @param pokemon
	 * @param opponent
	 * @param plAttack
	 */
	private static void moveExtra(MoveList move, Pokemon pokemon, Pokemon opponent, boolean plAttack)
	{
		if (move.getExtra() != null)
		{
			Random rnd = new Random();
			double r = rnd.nextDouble();
			double r2 = 0;
			if (move.getExtra().getEffect().equals("FangMove")) r2 = rnd.nextDouble();
			
			if ((move.getExtra().getValue() > r || move.getExtra().getValue() > r2) && !move.getMoveType().equals("status"))
			{
				switch(move.getExtra().getEffect())
				{
					case "Paralysis": 
						if (opponent.type1 != Element.ELECTRIC && opponent.type2 != Element.ELECTRIC)
						{
							opponent.status = Status.PARALYSIS;
							if (plAttack) System.out.println("The opposing " + opponent.name + " was paralyzed!");
							else System.out.println(opponent.name + " was paralyzed!");
						}
						break;
					case "Burn": 
						if (opponent.type1 != Element.FIRE && opponent.type2 != Element.FIRE)
						{
							opponent.status = Status.BURNED;
							if (plAttack) System.out.println("The opposing " + opponent.name + " was burned!");
							else System.out.println(opponent.name + " was burned!");
						}
						break;
					case "Poison": 
						if (opponent.type1 != Element.POISON && opponent.type2 != Element.POISON && opponent.type1 != Element.STEEL && opponent.type2 != Element.STEEL)
						{
							opponent.status = Status.POISON;
							if (plAttack) System.out.println("The opposing " + opponent.name + " was poisoned!");
							else System.out.println(opponent.name + " was poisoned!");
						}
						break;
					case "Toxic": 
						if (opponent.type1 != Element.POISON && opponent.type2 != Element.POISON && opponent.type1 != Element.STEEL && opponent.type2 != Element.STEEL)
						{
							opponent.status = Status.TOXIC;
							if (plAttack) System.out.println("The opposing " + opponent.name + " was badly poisoned!");
							else System.out.println(opponent.name + " was badly poisoned!");
							opponent.toxicTimer = 1;
						}
						break;
					case "Freeze": opponent.status = Status.FROZEN;
						if (plAttack) System.out.println("The opposing " + opponent.name + " was frozen!");
						else System.out.println(opponent.name + " was frozen!");
						break;
					case "Sleep": opponent.status = Status.SLEEP;
						if (plAttack) System.out.println("The opposing " + opponent.name + " feel asleep!");
						else System.out.println(opponent.name + " fell asleep!");
						opponent.sleepTimer = 1;
						break;
					case "Confusion": opponent.confusion = Status.CONFUSION;
						if (plAttack) System.out.println("The opposing " + opponent.name + " was confused!");
						else System.out.println(opponent.name + " was confused!");
						break;
					case "Flinch": opponent.flinched = true;
						break;
					case "FlatDamage": opponent.currentHP -= move.getExtra().getValue();
						if (plAttack) System.out.println("The opposing " + opponent.name + " took " + (int)move.getExtra().getValue() + " damage!");
						else System.out.println(opponent.name + " took " + (int)move.getExtra().getValue() + " damage!");
						
						if (opponent.currentHP <= 0)
						{
							if (plAttack) System.out.println("The opposing " + opponent.name + " fainted!\n");
							else System.out.println(opponent.name + " fainted!\n");
							
							opponent.status = Status.FAINTED;
						}
						else 
						{
							if (plAttack) System.out.println("The opposing " + opponent.name + " has " + opponent.currentHP + " HP left!\n");
							else System.out.println(opponent.name + " has " + opponent.currentHP + " HP left!\n");
						}
						break;
					case "Thief": if(!opponent.heldItem.equals(Items.NONE) && pokemon.heldItem.equals(Items.NONE))
						{
							pokemon.heldItem = opponent.heldItem;
							opponent.heldItem = Items.NONE;
							
							if (plAttack) System.out.println(pokemon.name + " stole the " + pokemon.heldItem.getName() + " from the opposing " + opponent.name + "!\n");
							else System.out.println("The opposing " + pokemon.name + " stole your " + opponent.name + ((opponent.name.endsWith("s")) ? "' " : "'s ") + pokemon.heldItem.getName() + "!\n");
						}
						break;
					case "ChargeUpTurn": 
						if (pokemon.isCharging)
						{
							if (move == MoveList.SOLAR_BEAM)
							{	
								if(plAttack) System.out.println(pokemon.name + " is absorbing sunlight!\n");
								else System.out.println("The opposinig " + pokemon.name + " is absorbing sunlight!\n");
								
								if (weather == Weather.HARSH_SUNLIGHT) 
								{
									pokemonAttack(pokemon, opponent, pokemon.previousMove, plAttack);
									
									pokemon.isCharging = false;
								}
							}
						}
						break;
					case "RechargeTurn": pokemon.isRecharging = true;
						break;
					case "Attack+1Attacker": pokemon.increaseStat(0, 1, plAttack);
						break;
					case "Speed-1Opponent": opponent.reduceStat(4, 1, !plAttack);
						break;
					case "SpD-1Opponent": opponent.reduceStat(3, 1, !plAttack);
						break;
					case "Feint": 
						if (opponent.protect)
						{
							if(plAttack) System.out.println(pokemon.name + " broke through the opposing " + opponent.name + ((opponent.name.endsWith("s")) ? "' " : "'s ") + " protect!");
							else System.out.println("The opposing " + pokemon.name + " broke through " + opponent.name + ((opponent.name.endsWith("s")) ? "' " : "'s ") + " Protect!");
							opponent.protect = false;
						}
					case "FangMove":
						switch(move)
						{
							case FIRE_FANG:
								if (move.getExtra().getValue() > r)
								{
									if (opponent.type1 != Element.FIRE && opponent.type2 != Element.FIRE)
									{
										opponent.status = Status.BURNED;
										if (plAttack) System.out.println("The opposing " + opponent.name + " was burned!");
										else System.out.println(opponent.name + " was burned!");
									}
								}
								break;
							case THUNDER_FANG:
								if (move.getExtra().getValue() > r)
								{
									if (opponent.type1 != Element.ELECTRIC && opponent.type2 != Element.ELECTRIC)
									{
										opponent.status = Status.PARALYSIS;
										if (plAttack) System.out.println("The opposing " + opponent.name + " was paralyzed!");
										else System.out.println(opponent.name + " was paralyzed!");
									}
								}
								break;
							case ICE_FANG:
								if (move.getExtra().getValue() > r)
								{
									opponent.status = Status.FROZEN;
									if (plAttack) System.out.println("The opposing " + opponent.name + " was frozen!");
									else System.out.println(opponent.name + " was frozen!");
								}
								break;
							default:
								break;
						}
						
						if (move.getExtra().getValue() > r2)
						{
							opponent.flinched = true;
						}
						break;
					default:
						break;
				}
			}
			else if (move.getMoveType().equals("status"))
			{
				switch(move.getExtra().getEffect())
				{
					case "Heal": 
						if (pokemon.currentHP < pokemon.stats.get(0))
						{
							int heal = (int)(pokemon.stats.get(0) * move.getExtra().getValue());
							
							if (heal + pokemon.currentHP > pokemon.stats.get(0)) heal = pokemon.stats.get(0) - pokemon.currentHP;
							
							pokemon.currentHP += heal;
							if (plAttack = false) 
							{
								System.out.println(pokemon.name + " restored " + heal + " HP!");
								System.out.println(pokemon.name + " has " + pokemon.currentHP + " HP left!\n");
							}
							else 
							{
								System.out.println("The opposing " + pokemon.name + " restored " + heal + " HP!");
								System.out.println("The opposing " + pokemon.name + " has " + pokemon.currentHP + " HP left!\n");
							}
						}
						else
						{
							if (plAttack) System.out.println(pokemon.name + " is already full HP!\n");
							else System.out.println("The opposing" + pokemon.name + " is already full HP!\n");
						}
						break;
					case "SunHeal": 
						if (pokemon.currentHP < pokemon.stats.get(0))
						{
							int heal;
							
							if (weather == Weather.NONE) heal = (int)(pokemon.stats.get(0) * move.getExtra().getValue());
							else if (weather == Weather.HARSH_SUNLIGHT || weather == Weather.DELOATE_LAND) heal = (int)(pokemon.stats.get(0) * 0.75);
							else heal = (int)(pokemon.stats.get(0) * 0.25);
							
							if (heal + pokemon.currentHP > pokemon.stats.get(0)) heal = pokemon.stats.get(0) - pokemon.currentHP;
							
							pokemon.currentHP += heal;
							if (plAttack = false) 
							{
								System.out.println(pokemon.name + " restored " + heal + " HP!");
								System.out.println(pokemon.name + " has " + pokemon.currentHP + " HP left!\n");
							}
							else 
							{
								System.out.println("The opposing " + pokemon.name + " restored " + heal + " HP!");
								System.out.println("The opposing " + pokemon.name + " has " + pokemon.currentHP + " HP left!\n");
							}
						}
						else
						{
							if (plAttack) System.out.println(pokemon.name + " is already full HP!\n");
							else System.out.println("The opposing" + pokemon.name + " is already full HP!\n");
						}
						break;
					case "Protect": 
						if (pokemon.previousTurnProtect == false)
						{
							pokemon.protect = true;
							if (plAttack) System.out.println(pokemon.name + " protected itself!\n");
							else System.out.println("The opposing " +  pokemon.name + " protected itself!\n");
							
							pokemon.previousTurnProtect = true;
						}
						else
						{
							r = rnd.nextDouble();
							if (r >= 0.5)
							{
								pokemon.protect = true;
								if (plAttack) System.out.println(pokemon.name + " protected itself!\n");
								else System.out.println("The opposing " +  pokemon.name + " protected itself!\n");
								
								pokemon.previousTurnProtect = true;
							}
							else
							{
								System.out.println("But it failed!\n");
								pokemon.previousTurnProtect = false;
							}
						}
						break;
					case "RainDance": weather = Weather.RAIN;
						System.out.println("It started to rain!\n");
						break;
					case "Paralysis": 
						if (move.getElement() == Element.ELECTRIC && (opponent.type1 == Element.ELECTRIC || opponent.type2 == Element.ELECTRIC))
							System.out.println("The attack had no effect!\n");
						else
						{
							opponent.status = Status.PARALYSIS;
							if (plAttack) System.out.println("The opposing " + opponent.name + " was paralyzed!\n");
							else System.out.println(opponent.name + " was paralyzed!\n");
						}
						break;
					case "Burn": 
						if (opponent.type1 != Element.FIRE && opponent.type2 != Element.FIRE)
						{
							opponent.status = Status.BURNED;
							if (plAttack) System.out.println("The opposing " + opponent.name + " was burned!\n");
							else System.out.println(opponent.name + " was burned!\n");
						}
						else System.out.println("The attack had no effect!\n");
						break;
					case "Poison": 
						if (opponent.type1 != Element.POISON && opponent.type2 != Element.POISON && opponent.type1 != Element.STEEL && opponent.type2 != Element.STEEL)
						{
							opponent.status = Status.POISON;
							if (plAttack) System.out.println("The opposing " + opponent.name + " was poisoned!\n");
							else System.out.println(opponent.name + " was poisoned!\n");
						}
						else System.out.println("The attack had no effect!\n");
						break;
					case "Toxic": 
						if (opponent.type1 != Element.POISON && opponent.type2 != Element.POISON && opponent.type1 != Element.STEEL && opponent.type2 != Element.STEEL)
						{
							opponent.status = Status.TOXIC;
							if (plAttack) System.out.println("The opposing " + opponent.name + " was badly poisoned!\n");
							else System.out.println(opponent.name + " was badly poisoned!\n");
							opponent.toxicTimer = 1;
						}
						else System.out.println("The attack had no effect!\n");
						break;
					case "Sleep": opponent.status = Status.SLEEP;
						if (plAttack) System.out.println("The opposing " + opponent.name + " feel asleep!\n");
						else System.out.println(opponent.name + " fell asleep!\n");
						opponent.sleepTimer = 1;
						break;
					case "Confusion": opponent.confusion = Status.CONFUSION;
						if (plAttack) System.out.println("The opposing " + opponent.name + " was confused!\n");
						else System.out.println(opponent.name + " was confused!\n");
						break;
						
					case "Attack-1Opponent": opponent.reduceStat(0, 1, !plAttack);
						System.out.println();
						break;
					case "Attack-2Opponent": opponent.reduceStat(0, 2, !plAttack);
						System.out.println();
						break;
					case "Defence+1Attacker": pokemon.increaseStat(1, 1, plAttack);
						System.out.println();
						break;
					case "Defence+2Attacker": pokemon.increaseStat(1, 2, plAttack);
					 	System.out.println();
						break;
					case "Defence-1Opponent": opponent.reduceStat(1, 1, !plAttack);
						System.out.println();
						break;
					case "Speed-2Opponent": opponent.reduceStat(4, 2, !plAttack);
						System.out.println();
						break;	
					case "Speed+2Attacker": pokemon.increaseStat(4, 2, plAttack);
						System.out.println();
						break;
					case "Accuracy-1Opponent": opponent.reduceStat(5, 1, !plAttack);
						System.out.println();
						break;
					case "Evasion+1Attacker": pokemon.increaseStat(6, 1, plAttack);
						System.out.println();
						break;
					case "Evasion-1Opponent": opponent.reduceStat(6, 1, !plAttack);
						System.out.println();
						break;
					case "Growth": 
						if (weather == Weather.HARSH_SUNLIGHT) 
						{
							pokemon.increaseStat(0, 2, plAttack); 
							pokemon.increaseStat(2, 2, plAttack);
						}
						else
						{
							pokemon.increaseStat(0, 1, plAttack);
							pokemon.increaseStat(2, 1, plAttack);
						}
						System.out.println();
						break;
						
					case "ClearStatusField": System.out.println("All stat changes have been eliminated!\n");
						for(int i = 0; i < pokemon.pokemonStatusStages.size(); i++)
						{
							pokemon.pokemonStatusStages.set(i, 0);
							opponent.pokemonStatusStages.set(i, 0);
						}
						break;
					case "Refresh": if(plAttack) System.out.println(pokemon.name + " was refreshed!\n");
						else System.out.println("The opposing " + pokemon.name + " was refreshed!\n");
						pokemon.status = Status.NORMAL;
						break;
					case "WorrySeed": if(plAttack) System.out.println("The opposing " + opponent.name + ((opponent.name.endsWith("s")) ? "' " : "'s ") + "ability was changed to Insomnia!");
						else System.out.println(opponent.name + ((opponent.name.endsWith("s")) ? "' " : "'s ") + "ability was changed to Insomnia!");
						opponent.currentAbility = Abilities.INSOMNIA;
						break;
					case "LeechSeed": 
						if (opponent.leechSeed == false)
						{
							if (opponent.type1 != Element.GRASS && opponent.type2 != Element.GRASS)
							{
								if(plAttack) System.out.println("The opposing " + opponent.name + " was seeded!\n");
								else System.out.println(opponent.name + " was seeded!\n");
								
								opponent.leechSeed = true;
							}
							else System.out.println("The attack had no effect!\n");
						}
						else
						{
							if(plAttack) System.out.println("The opposing " + opponent.name  + " is already seeded!\n");
							else System.out.println(opponent.name + " is already seeded!\n");
						}
						break;
					case "ClearStatusChangesField": pokemon.resetStatStages();
						opponent.resetStatStages();
						pokemon.leechSeed = false;
						opponent.leechSeed = false;
						break;
					case "LightScreen": 
						if (plAttack)
						{
							if (lightScreenPl) System.out.println("But it failed!\n");
							else
							{
								System.out.println("Light Screen raised your team's special defence!\n");
								lightScreenPl = true;
								lightScreenPlTimer = 5;
							}
						}
						else
						{
							if(lightScreenOp) System.out.println("But it failed!\n");
							else
							{
								System.out.println("Light Screen raised the opposing team's special defence!\n");
								lightScreenOp = true;
								lightScreenOpTimer = 5;
							}
						}
					default:
						break;
				}
			}
			else
			{
				switch(move.getExtra().getEffect())
				{
				case "FireSpin": if (plAttack) System.out.println("The opposing " + opponent.name + " was trapped in the fiery vortex!");
					else System.out.println(opponent.name + " was trapped in the fiery vortex!"); 
					opponent.continualDamageTimer = 1;
					opponent.trapped = true;
					opponent.trappingMove = MoveList.FIRE_SPIN;
					break;
				default: 
					break;
				}
			}
		}
	}
	
	/**
	 * Executes a multi hitting move.
	 * @param pokemon
	 * @param opponent
	 * @param moveChoice
	 * @param plAttack
	 */
	private static void useMultiHittingMove(Pokemon pokemon, Pokemon opponent, int moveChoice, boolean plAttack)
	{
		MoveList move = pokemon.moves.get(moveChoice);
		
		if (move.getExtra() == MovesExtra.DOUBLE_HIT)
		{
			int i = 0;
			do
			{
				pokemonAttack(pokemon, opponent, moveChoice, plAttack);
				i++;
			}
			while (i < 2 && opponent.currentHP > 0);
			System.out.println("Hit " + i + " times!\n");
		}
		else if (move.getExtra() == MovesExtra.MULTI_HIT)
		{
			int hits = 0;
			double random = Math.random();
			
			if (random > 0.666) hits = 2;
			else if (random > 0.333) hits = 3;
			else if (random >= 0.166) hits = 4;
			else hits = 5;
			
			int i = 0;
			do
			{
				pokemonAttack(pokemon, opponent, moveChoice, plAttack);
				i++;
			}
			while (i < hits && opponent.currentHP > 0);
			
			System.out.println("Hit " + i + " times!");
		}
	}
	
	/**
	 * Everything that happens at the end of a turn.
	 * @param pokemon
	 * @param opponent
	 * @param playerPokemon
	 * @return
	 */
	private static Pokemon endOfTurn(Pokemon pokemon, Pokemon opponent, boolean playerPokemon)
	{
		int damage;
		
		if(playerPokemon && lightScreenPl)
		{
			lightScreenPlTimer--;
			if (lightScreenPlTimer == 0)
			{
				System.out.println("Your team's Light Screen faded!");
				lightScreenPl = false;
			}
		}
		else if (playerPokemon && reflectPl)
		{
			reflectPlTimer--;
			if (reflectPlTimer == 0)
			{
				System.out.println("Your team's Reflect faded!");
				reflectPl = false;
			}
		}
		else if (!playerPokemon && lightScreenOp)
		{
			lightScreenOpTimer--;
			if (lightScreenOpTimer ==0)
			{
				System.out.println("The opposing team's Light Screen faded!");
				lightScreenOp = false;
			}
		}
		else if (!playerPokemon && reflectOp)
		{
			reflectOpTimer--;
			if(reflectOpTimer == 0)
			{
				System.out.println("The opposing team's Reflect faded!");
				reflectOp = false;
			}
		}
		
		switch(pokemon.status)
		{
			case BURNED:  damage = (int)(pokemon.stats.get(0) * (1.0 / 8.0));
				pokemon.currentHP -= damage;
				
				if (playerPokemon) System.out.println(pokemon.name + " took " + damage + " damage from the burn.");
				else System.out.println("The opposing " + pokemon.name + " took " + damage + " damage from the burn.");
				if (pokemon.currentHP <= 0) 
				{
					if (playerPokemon) System.out.println(pokemon.name + " fainted!\n");
					else System.out.println("The opposing " + pokemon.name + " fainted!\n");
				}
				else
				{
					if (playerPokemon) System.out.println(pokemon.name + " has " + pokemon.currentHP + " HP left!");
					else System.out.println("The opposing " + pokemon.name + " has " + pokemon.currentHP + " HP left!");
				}
				break;
			case POISON: damage = (int)(pokemon.stats.get(0) * (1.0 / 16.0));
				pokemon.currentHP -= damage;
				
				if (playerPokemon) System.out.println(pokemon.name + " took " + damage + " damage from the poison.");
				else System.out.println("The opposing " + pokemon.name + " took " + damage + " damage from the poison.");
				if (pokemon.currentHP <= 0) 
				{
					if (playerPokemon) System.out.println(pokemon.name + " fainted!\n");
					else System.out.println("The opposing " + pokemon.name + " fainted!\n");
				}
				else
				{
					if (playerPokemon) System.out.println(pokemon.name + " has " + pokemon.currentHP + " HP left!");
					else System.out.println("The opposing " + pokemon.name + " has " + pokemon.currentHP + " HP left!");
				}
				break;
			case TOXIC: damage = (int)(pokemon.stats.get(0) * (1.0 / 16.0));
				for (int i = 2; i <= pokemon.toxicTimer; i++) damage += (int)(pokemon.stats.get(0) * (1.0 / 16.0));
				
				pokemon.toxicTimer++;
				pokemon.currentHP -= damage;
				
				if (playerPokemon) System.out.println(pokemon.name + " took " + damage + " damage from the poison.");
				else System.out.println("The opposing " + pokemon.name + " took " + damage + " damage from the poison.");
				if (pokemon.currentHP <= 0) 
				{
					if (playerPokemon) System.out.println(pokemon.name + " fainted!\n");
					else System.out.println("The opposing " + pokemon.name + " fainted!\n");
				}
				else
				{
					if (playerPokemon) System.out.println(pokemon.name + " has " + pokemon.currentHP + " HP left!");
					else System.out.println("The opposing " + pokemon.name + " has " + pokemon.currentHP + " HP left!");
				}
				break;
			default:
				break;
		}
		
		if (pokemon.trapped == true && pokemon.trappingMove != null)
		{
			damage = (int)(pokemon.stats.get(0) * pokemon.trappingMove.getExtra().getValue());
			
			if (pokemon.continualDamageTimer > 2)
			{
				Random rnd = new Random();
				double r = rnd.nextDouble();
				if (r > 0.4)
				{
					pokemon.continualDamageTimer++;
					pokemon.currentHP -= damage;
					if (playerPokemon) System.out.println(pokemon.name + " took " + damage + " damage from the " + pokemon.trappingMove.getName() + "!");
					else System.out.println("The opposing " + pokemon.name + " took " + damage + " damage from the " + pokemon.trappingMove.getName() + "!");
					
					if (pokemon.currentHP <= 0)
					{
						if (playerPokemon) System.out.println(pokemon.name + " fainted!\n");
						else System.out.println("The opposing " + pokemon.name + " fainted!\n");
						pokemon.status = Status.FAINTED;
					}
					else
					{
						if (playerPokemon) System.out.println(pokemon.name + " has " + pokemon.currentHP + " HP left!");
						else System.out.println("The opposing " + pokemon.name + " has " + pokemon.currentHP + " HP left!");
					}
				}
				else
				{
					if (playerPokemon) System.out.println(pokemon.name + " was freed from the " + pokemon.trappingMove.getName() + "!");
					else System.out.println("The opposing " + pokemon.name + " was freed from the " + pokemon.trappingMove.getName() + "!");
					
					pokemon.trappingMove = null;
					pokemon.trapped = false;					
					pokemon.continualDamageTimer = 0;
				}
			}
			else
			{
				pokemon.continualDamageTimer++;
				pokemon.currentHP -= damage;
				
				if (playerPokemon) System.out.println(pokemon.name + " took " + damage + " damage from the " + pokemon.trappingMove.getName() + "!");
				else System.out.println("The opposing " + pokemon.name + " took " + damage + " damage from the " + pokemon.trappingMove.getName() + "!");
				
				if (pokemon.currentHP <= 0)
				{
					if (playerPokemon) System.out.println(pokemon.name + " fainted!\n");
					else System.out.println("The opposing " + pokemon.name + " fainted!\n");
					pokemon.status = Status.FAINTED;
				}
				else
				{
					if (playerPokemon) System.out.println(pokemon.name + " has " + pokemon.currentHP + " HP left!");
					else System.out.println("The opposing " + pokemon.name + " has " + pokemon.currentHP + " HP left!");
				}
			}
		}
		if (pokemon.leechSeed)
		{
			damage = pokemon.stats.get(0) / 8;
			
			if (playerPokemon) 
			{
				System.out.println(pokemon.name + " took " + damage + " damage from the Leech Seed!");
				System.out.println("The opposing " + opponent.name + " restored " + damage + " HP!");
			}
			else
			{
				System.out.println("The opposing " + pokemon.name + " took " + damage + " damage from the Leech Seed!");
				System.out.println(opponent.name + " restored " + damage + " HP!");
			}
			pokemon.currentHP -= damage;
			
			if (opponent.currentHP + damage <= opponent.stats.get(0)) opponent.currentHP += damage;
			else opponent.currentHP = opponent.stats.get(0);
			
			if (playerPokemon) System.out.println("The opposing " + opponent.name + " has " + opponent.currentHP + " HP left!");
			else System.out.println(opponent.name + " has " + opponent.currentHP + " HP left!");
			
			if (pokemon.currentHP <= 0)
			{
				if (playerPokemon) System.out.println(pokemon.name + " fainted!\n");
				else System.out.println("The opposing " + pokemon.name + " fainted!\n");
				pokemon.status = Status.FAINTED;
			}
			else
			{
				if (playerPokemon) System.out.println(pokemon.name + " has " + pokemon.currentHP + " HP left!");
				else System.out.println("The opposing " + pokemon.name + " has " + pokemon.currentHP + " HP left!");
			}
		}
		
		if(weather != Weather.NONE) weatherDamage(pokemon, playerPokemon);
		
		if (pokemon.currentHP < 0) pokemon.currentHP = 0;
		
		return pokemon;
	}
	
	/**
	 * Deals damage to the Pokemon depending on the weather.
	 * @param pokemon
	 * @param plPokemon
	 */
	private static void weatherDamage(Pokemon pokemon, boolean plPokemon)
	{
		int damage;
		
		switch(weather)
		{
			case HAIL: 
				if (pokemon.type1 != Element.ICE && pokemon.type2 != Element.ICE)
				{
					damage = pokemon.stats.get(0)/16;
					pokemon.currentHP -= damage;
					if (plPokemon) System.out.println(pokemon.name + " is buffeted by the hail!");
					else System.out.println("The opposing " + pokemon.name + " is buffeted by the hail!");
					
					if (pokemon.currentHP <= 0)
					{
						if (plPokemon) System.out.println(pokemon.name + " fainted!\n");
						else System.out.println("The opposing " + pokemon.name + " fainted!\n");
						
						pokemon.status = Status.FAINTED;
						pokemon.currentHP = 0;
					}
					else
					{
						if (plPokemon) System.out.println(pokemon.name + " has " + pokemon.currentHP + " HP left!\n");
						else System.out.println("The opposing " + pokemon.name + " has " + pokemon.currentHP + " HP left!\n");
					}
				}
				break;
			case SANDSTORM:
				if ((pokemon.type1 != Element.GROUND && pokemon.type2 != Element.GROUND) && (pokemon.type1 != Element.ROCK && pokemon.type2 != Element.ROCK) && (pokemon.type1 != Element.STEEL && pokemon.type2 != Element.STEEL))
				{
					damage = pokemon.stats.get(0)/16;
					pokemon.currentHP -= damage;
					if (plPokemon) System.out.println(pokemon.name + " is buffeted by the sandstorm!");
					else System.out.println("The opposing " + pokemon.name + " is buffeted by the sandstorm!");
					
					if (pokemon.currentHP <= 0)
					{
						if (plPokemon) System.out.println(pokemon.name + " fainted!\n");
						else System.out.println("The opposing " + pokemon.name + " fainted!\n");
						
						pokemon.status = Status.FAINTED;
						pokemon.currentHP = 0;
					}
					else
					{
						if (plPokemon) System.out.println(pokemon.name + " has " + pokemon.currentHP + " HP left!\n");
						else System.out.println("The opposing " + pokemon.name + " has " + pokemon.currentHP + " HP left!\n");
					}
				}
				break;
			default: 
				break;
		}
	}
	
	/**
	 * Checks which weather is at the start of the battle.
	 */
	private static void checkStartWeather()
	{
		switch(weather)
		{
			case HARSH_SUNLIGHT: System.out.println("The sunlight is strong!");
				break;
			case HAIL: System.out.println("It started to hail!");
				break;
			case RAIN: System.out.println("It started to rain!");
				break;
			case SANDSTORM: System.out.println("A sandstorm kicked up!");
				break;
			case FOG: System.out.println("The fog is deep...");
				break;
			case CLOUDY: System.out.println("The sky is cloudy.");
				break;
			default: 
				break;
		}
	}
	
	/**
	 * Plays the battle music.
	 * Song: Pokemon Stadium Mewtwo Battle Remix by GlitchxCity
	 * @return Clip
	 */
	private static Clip playBattleMusic() 
	{
		Clip clip = null;
	    try 
	    {
	        AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File("bgm/wildbattle.wav").getAbsoluteFile());
	        clip = AudioSystem.getClip();
	        clip.open(audioInputStream);
	        clip.loop(Clip.LOOP_CONTINUOUSLY);
	        FloatControl gainControl = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
	        gainControl.setValue(-10.0f); 
	        clip.start();
	    } 
	    catch(Exception ex) 
	    {
	        System.out.println("Error with playing sound.");
	        ex.printStackTrace();
	    }
	    return clip;
	}
	
	/**
	 * Checks the Pokemon's ability.
	 * @param pokemon
	 * @param opponent
	 * @param plPokemon
	 */
	private static void checkAbility(Pokemon pokemon, Pokemon opponent, boolean plPokemon)
	{
		switch(pokemon.ability.getId())
		{
			case 19: weather = Weather.HARSH_SUNLIGHT;
				System.out.println("The sunlight turned harsh!\n");
				break;
			case 20: weather = Weather.RAIN;
				System.out.println("It started to rain!\n");
				break;
			case 21: weather = Weather.SANDSTORM;
				System.out.println("A sandstorm kicked up!\n");
				break;
			case 22: weather = Weather.HAIL;
				System.out.println("It started to hail!\n");
				break;
			case 23: weather = Weather.DELOATE_LAND;
				System.out.println("The sunlight turned extremely harsh!\n");
				break;
			case 24: weather = Weather.PRIMORDIAL_SEA;
				System.out.println("A heavy rain began to fall!\n");
				break;
			case 25: weather = Weather.DELTA_STREAM;
				System.out.println("A mysterious air current is protecting Flying-type Pok�mon!\n");
				break;
			case 17: boolean shudder = false;
				for (MoveList move : opponent.moves)
				{
					if (move.getElement().isSuperEffective(move.getElement(), opponent.type1) || move.getElement().isSuperEffective(move.getElement(), opponent.type2)) shudder = true;
				}
				if (shudder)
				{
					if (plPokemon) System.out.println(pokemon.name + " shuddered!");
					else System.out.println("The opposing " + pokemon.name + " shuddered!");
				}
				break;
			default:
				break;
		}
	}
	
	/**
	 * Plays the victory music.
	 * Song: Mystery Dungeon Primal Dialga Remix by GlitchxCity
	 * @return Clip
	 */
	private static Clip playVictoryMusic() 
	{
		Clip clip = null;
	    try 
	    {
	        AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File("bgm/victory.wav").getAbsoluteFile());
	        clip = AudioSystem.getClip();
	        clip.open(audioInputStream);
	        clip.loop(Clip.LOOP_CONTINUOUSLY);
	        FloatControl gainControl = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
	        gainControl.setValue(-10.0f); 
	        clip.start();
	    } 
	    catch(Exception ex) 
	    {
	        System.out.println("Error with playing sound.");
	        ex.printStackTrace();
	    }
	    return clip;
	}
	
	/**
	 * Returns the status modifier of the Pokemon. (For example: Returns 2.5 if the Pokemon has +3 in the stat)
	 * @param pokemon
	 * @param i
	 * @return double
	 */
	private static double getStatModifier(Pokemon pokemon, int i)
	{
		if (i < 6)
		{
			switch(pokemon.pokemonStatusStages.get(i))
			{
				case 0: return 1;
				case 1: return 1.5;
				case 2: return 2;
				case 3: return 2.5;
				case 4: return 3;
				case 5: return 3.5;
				case 6: return 4;
				case -1: return 0.66;
				case -2: return 0.5;
				case -3: return 0.4;
				case -4: return 0.33;
				case -5: return 0.28;
				case -6: return 0.25;
				default: return 1;
			}
		}
		else
		{
			switch(pokemon.pokemonStatusStages.get(i))
			{
				case 0: return 1;
				case 1: return 0.66;
				case 2: return 0.5;
				case 3: return 0.4;
				case 4: return 0.33;
				case 5: return 0.28;
				case 6: return 0.25;
				case -1: return 1.5;
				case -2: return 2;
				case -3: return 2.5;
				case -4: return 3;
				case -5: return 3.5;
				case -6: return 4;
				default: return 1;
			}
		}
	}
	
	/**
	 * Returns the additional damage modifiers to a Pokemon's move through a Pokemon's ability.
	 * @param pokemon
	 * @param move
	 * @return double
	 */
	private static double getAbilityMoveModifier(Pokemon pokemon, MoveList move)
	{
		switch(pokemon.ability.getId())
		{
			case 0:
			case 1:
			case 2:
			case 3: if ((pokemon.currentHP < (pokemon.stats.get(0) / 4)) && (pokemon.type1 == move.getElement() || pokemon.type2 == move.getElement())) return 1.5;
				else return 1;
			default:
				return 1;
		}		
	}
	
	/**
	 * Returns the additional damage modifiers to a Pokemon's attack/special attack trough a Pokemon's ability.
	 * @param pokemon
	 * @param move
	 * @return double
	 */
	private static double getAbilityAttackModifier(Pokemon pokemon, MoveList move)
	{
		switch(pokemon.ability.getId())
		{
			case 10: 
				if (pokemon.status != Status.PARALYSIS || pokemon.status == Status.POISON || pokemon.status == Status.TOXIC || pokemon.status == Status.FROZEN || pokemon.status == Status.SLEEP || pokemon.status == Status.SLEEP || pokemon.status == Status.REST)
					return 1.5;
				else return 1;
			default: return 1;
		}
	}
	
	/**
	 * Returns the weather modifiers for the given move.
	 * @param move
	 * @return double
	 */
	private static double getWeatherModifier(Element move)
	{
		switch(weather)
		{
			case HARSH_SUNLIGHT: if (move == Element.FIRE) return 1.5;
				else if (move == Element.WATER) return 0.5;
				else return 1;
			case RAIN: if (move == Element.WATER) return 1.5;
				else if (move == Element.FIRE) return 0.5;
				else return 1;
			default: return 1;
		}
	}
	
	/**
	 * Calculates the damage of "special" moves like Electro Ball, Gyro Ball, etc.
	 * @param pokemon
	 * @param opponent
	 * @param move
	 * @return int
	 */
	private static int calculateMoveDamage(Pokemon pokemon, Pokemon opponent, MoveList move)
	{
		switch (move)
		{
			case ELECTRO_BALL: double pokSpe, opSpe;
				pokSpe = (int)(pokemon.stats.get(5) * getStatModifier(pokemon, 4));
				if(pokemon.status == Status.PARALYSIS) pokSpe /= 2; 
				
				opSpe = (int)(opponent.stats.get(5) * getStatModifier(opponent, 4));
				if(opponent.status == Status.PARALYSIS) opSpe /= 2; 
				
				double speed = opSpe/pokSpe;
				
				if (speed > 0.5) return 60;
				else if (speed > 0.33) return 80;
				else if (speed > 0.25) return 120;
				else return 150;
			default: return move.getPower();
		}
	}
	
	/**
	 * Sets which Pokemon has priority this turn.
	 * @param pokemon1
	 * @param pokemon2
	 * @param pok1Move
	 * @param pok2Move
	 */
	private static void setPriority(Pokemon pokemon1, Pokemon pokemon2, MoveList pok1Move, MoveList pok2Move)
	{
		int pok1Speed, pok2Speed;
		
		pok1Speed = (int)(pokemon1.stats.get(5) * getStatModifier(pokemon1, 4));
		if(pokemon1.status == Status.PARALYSIS) pok1Speed /= 2; 
		
		pok2Speed = (int)(pokemon2.stats.get(5) * getStatModifier(pokemon2, 4));
		if(pokemon2.status == Status.PARALYSIS) pok2Speed /= 2; 
		
		pok1Speed = pokemon1.stats.get(5);
		
		if (pok1Move.getPriority() == pok2Move.getPriority())
		{
			if (pok1Speed > pok2Speed)
			{
				pokemon1.hasPriority = true;
				pokemon2.hasPriority = false;
			}
			else if (pok2Speed > pok1Speed)
			{
				pokemon1.hasPriority = false;
				pokemon2.hasPriority = true;
			}
			else
			{
				if (Math.random() > 0.5)
				{
					pokemon1.hasPriority = true;
					pokemon2.hasPriority = false;
				}
				else
				{
					pokemon1.hasPriority = false;
					pokemon2.hasPriority = true;
				}
			}
		}
		else if (pok1Move.getPriority() > pok2Move.getPriority())
		{
			pokemon1.hasPriority = true;
			pokemon2.hasPriority = false;
		}
		else
		{
			pokemon1.hasPriority = false;
			pokemon2.hasPriority = true;
		}
	}
	
	/**
	 * Prints out a Pikachu ASCII - because Pikachu is cute!
	 */
	private void pikachuASCII()
	{
		System.out.println("quu..__");
		System.out.println(" $$$b  `---.__");
		System.out.println("  $$b        `--.                          ___.---uuudP");
		System.out.println("   `$$b           `.__.------.__     __.---'      $$$$             .");
		System.out.println("     $b          -'            `-.-'            $$$              .'|");
		System.out.println("       .                                       d$             _.'  |");
		System.out.println("         `.   /                              ...             .'     |");
		System.out.println("           `./                           ..::-'            _.'       |");
		System.out.println("            /                         .:::-'            .-'         .'");
		System.out.println("           :                          ::''\\          _.'            |");
		System.out.println("          .' .-.             .-.           `.      .'               |");
		System.out.println("          : /'$$|           .@$\\           `.   .'              _.-'");
		System.out.println("         .'|$u$$|          |$$,$$|           |  <            _.-'");
		System.out.println("         | `:$$:'          :$$$$$:           `.  `.       .-'");
		System.out.println("         :                  `--'             |    `-.     \\");
		System.out.println("        :##.       ==             .###.       `.      `.    `\\");
		System.out.println("        |##:                      :###:        |        >     >");
		System.out.println("        |#'     `..'`..'          `###'        x:      /     /");
		System.out.println("         \\                                   xXX|     /    ./");
		System.out.println("          \\                                xXXX'|    /   ./");
		System.out.println("          /`-.                                  `.  /   /");
		System.out.println("         :    `-  ...........,                   | /  .'");
		System.out.println("         |         ``:::::::'       .            |<    `.");
		System.out.println("         |             ```          |           x| \\ `.:``.");
		System.out.println("         |                         .'    /'   xXX|  `:`M`M':.");
		System.out.println("         |    |                    ;    /:' xXXX'|  -'MMMMM:'");
		System.out.println("         `.  .'                   :    /:'       |-'MMMM.-'");
		System.out.println("          |  |                   .'   /'        .'MMM.-'");
		System.out.println("          `'`'                   :  ,'          |MMM<");
		System.out.println("            |                     `'            |tbap\\");
		System.out.println("             \\                                  :MM.-'");
		System.out.println("              \\                 |              .''");
		System.out.println("               \\.               `.            /");
		System.out.println("                /     .:::::::.. :           /");
		System.out.println("               |     .:::::::::::`.         /");
		System.out.println("               |   .:::------------\\       /");
		System.out.println("              /   .''               >::'  /");
		System.out.println("              `',:                 :    .'");
		System.out.println("                                   `:.:' ");
	}
}
