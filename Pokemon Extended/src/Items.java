/**
 * Enums for all the implemented Items in this game.
 * @author Andi
 * @version 2.1
 */
public enum Items 
{
//	Values:
//		-999 	NONE
//		>= 10 	Healing Items
//		5		Evolution Items
//		isPokeball boolean for Pokeballs
	
	NONE("None", -999, false, ""),
	
    POTION("Potion", 20, false, "A spray-type medicine for wounds. It restores the HP of one Pok�mon by just 20 points."),
    SUPERPOTION("Super Potion", 60, false, "A spray-type medicine for wounds. It restores the HP of one Pok�mon by 60 points."),
    ULTRAPOTION("Ultra Potion", 120, false, "A spray-type medicine for wounds. It restores the HP of one Pok�mon by 120 points."),
    HYPERPOTION("Hyper Potion", 200, false, "A spray-type medicine for wounds. It restores the HP of one Pok�mon by 200 points."),
    FULLRESTORE("Full Restore", 999, false, "A medicine that fully restores the HP and heals any status problems of a single Pok�mon."),
    POKEBALL("Pokeball", 1, true, "A device for catching wild Pok�mon. It is thrown like a ball at the target. It is designed as a capsule system."),
    GREATBALL("Great Ball", 1.5, true, "A good, high-performance Ball that provides a higher Pok�mon catch rate than a standard Pok� Ball."),
    ULTRABALL("Ultra Ball", 2, true, "An ultra-performance Ball that provides a higher Pok�mon catch rate than a Great Ball."),
    MASTERBALL("Master Ball", 255, true, "The best Ball with the ultimate level of performance. It will catch any wild Pok�mon without fail."),
    THUNDERSTONE("Thunderstone", 5, false, "A peculiar stone that makes certain species of Pok�mon evolve. It has a thunderbolt pattern."),
    WATERSTONE("Waterstone", 5, false, "A peculiar stone that makes certain species of Pok�mon evolve. It is a clear, light blue."),
    FIRESTONE("Firestone", 5, false, "A peculiar stone that makes certain species of Pok�mon evolve. It is colored orange."),
//    PrettyRibbon("Pretty Ribbon", 5, false, ""),
	;

    private final String name;
    private final double value;
    private final boolean isPokeball;
    private final String description;

    Items(String name, double value, boolean isPokeball, String description) 
    {
        this.name = name;
        this.value = value;
        this.isPokeball = isPokeball;
        this.description = description;
    }
    
    public String getName()
    {
    	return this.name;
    }
    
    public double getValue()
    {
    	return this.value;
    }
    
    /**
     * Returns true if the Item is a Pokeball.
     * @return boolean
     */
    public boolean isPokeball()
    {
    	return this.isPokeball;
    }
    
    public String getDescription()
    {
    	return this.description;
    }
}
