/**
 * Enum for all the possible weather conditions.
 * @author Andi
 * @version 2.1
 */
public enum Weather 
{
	NONE("None", 0),
	HARSH_SUNLIGHT("Harsh Sunlight", 1),
	RAIN("Rain", 2),
	HAIL("Hail", 3),
	SANDSTORM("Sandstorm", 4),
	FOG("Fog", 5),
	CLOUDY("Cloudy", 6),
	DELOATE_LAND("Desolate Land", 7),
	PRIMORDIAL_SEA("Primordial Sea", 8),
	DELTA_STREAM("Delta Stream", 9);
	
	private final String name;
	private final int id;
	
	Weather(String name, int id)
	{
		this.name = name;
		this.id = id;
	}
	
	public String getName()
	{
		return this.name;
	}
	
	public int getId()
	{
		return this.id;
	}
}
