import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * The Pokemon Venusaur.
 * @author Andi
 * @version 3.0
 */
public class Venusaur extends Pokemon 
{
	public Venusaur()
	{
		dexNumber = 3;
		
		speciesName = "Venusaur";
		name = speciesName;
		
		base.add(80);
		base.add(82);
		base.add(83);
		base.add(100);
		base.add(100);
		base.add(80);
		
		type1 = Element.GRASS;
		type2 = Element.POISON;
	
		genderRate = GenderRate.FEMALE_ONE_EIGHTH;
		growthRate = GrowthRate.PARABOLIC;
		baseEXP = 236;
		
		effortPoints.add(0);
		effortPoints.add(0);
		effortPoints.add(0);
		effortPoints.add(2);
		effortPoints.add(1);
		effortPoints.add(0);
		
		rareness = 45;
		happiness = 70;
		
		ability1 = Abilities.OVERGROW;
		ability2 = null;
		hiddenAbility1 = Abilities.CHLOROPHYLL;
		
		compat1 = EggGroups.MONSTER;
		compat2 = EggGroups.PLANT;
		
		stepsToHatch = 5355;
		height = 20;
		weight = 1000;
		
		color = PokemonColor.GREEN;
		kind = "Seed";
		habitat = PokemonHabitat.GRASSLAND;
		pokedex = "There is a large flower on Venusaur's back. The flower is said to take on vivid colors if it gets plenty of nutrition and sunlight. The flower�s aroma soothes the emotions of people.";
		
		wildItemCommon = null;
		wildItemUncommon = null;
		wildItemRare = null;
	}
	
	@Override
	public Map<Integer, List<MoveList>> LvlMoves() 
	{
		HashMap<Integer,List<MoveList>> returnMap = new HashMap<>();
		
		List<MoveList> moves = new LinkedList<>();
		moves.add(MoveList.TACKLE);
		moves.add(MoveList.LEECH_SEED);
		moves.add(MoveList.VINE_WHIP);
//		moves.add(MoveList.PetalDance);
		returnMap.put(1, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.GROWL);
		returnMap.put(3, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.POISON_POWDER);
		moves.add(MoveList.SLEEP_POWDER);
		returnMap.put(13, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.TAKE_DOWN);
		returnMap.put(15, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.RAZOR_LEAF);
		returnMap.put(20, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.SWEET_SCENT);
		returnMap.put(23, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.GROWTH);
		returnMap.put(28, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.DOUBLE_EDGE);
		returnMap.put(31, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.WORRY_SEED);
		returnMap.put(39, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.SYNTHESIS);
		returnMap.put(45, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.SEED_BOMB);
		returnMap.put(50, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.SOLAR_BEAM);
		returnMap.put(53, moves);
		
		return returnMap;
	}
}
