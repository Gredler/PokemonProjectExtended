import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * The Pokemon Squirtle.
 * @author Andi
 * @version 4.0
 */
public class Squirtle extends Pokemon 
{	
	public Squirtle()
	{
		dexNumber = 7;
		
		speciesName = "Squirtle";
		name = speciesName;
		
		base.add(44);
		base.add(48);
		base.add(65);
		base.add(50);
		base.add(64);
		base.add(43);
		
		type1 = Element.WATER;
		type2 = Element.NONE;
	
		genderRate = GenderRate.FEMALE_ONE_EIGHTH;
		growthRate = GrowthRate.PARABOLIC;
		baseEXP = 63;
		
		effortPoints.add(0);
		effortPoints.add(0);
		effortPoints.add(1);
		effortPoints.add(0);
		effortPoints.add(0);
		effortPoints.add(0);
		
		rareness = 45;
		happiness = 70;
		
		ability1 = Abilities.TORRENT;
		ability2 = null;
		hiddenAbility1 = Abilities.RAIN_DISH;
		
		compat1 = EggGroups.MONSTER;
		compat2 = EggGroups.WATER_1;
		
		stepsToHatch = 5355;
		height = 5;
		weight = 90;
		
		color = PokemonColor.BLUE;
		kind = "Shellfish";
		habitat = PokemonHabitat.WATERS_EDGE;
		pokedex = "Squirtle�s shell is not merely used for protection. The shell�s rounded shape and the grooves on its surface help minimize resistance in water, enabling this Pok�mon to swim at high speeds.";
		
		wildItemCommon = null;
		wildItemUncommon = null;
		wildItemRare = null;
	}
	
	@Override
	public Map<Integer, List<MoveList>> LvlMoves() 
	{
		HashMap<Integer,List<MoveList>> returnMap = new HashMap<>();
		
		List<MoveList> moves = new LinkedList<>();
		moves.add(MoveList.TACKLE);
		returnMap.put(1, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.TAIL_WHIP);
		returnMap.put(4, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.WATER_GUN);
		returnMap.put(7, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.WITHDRAW);
		returnMap.put(10, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.BUBBLE);
		returnMap.put(13, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.BITE);
		returnMap.put(16, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.RAPID_SPIN);
		returnMap.put(19, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.PROTECT);
		returnMap.put(22, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.WATER_PULSE);
		returnMap.put(25, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.AQUA_TAIL);
		returnMap.put(28, moves);
		
//		moves = new LinkedList<>();
//		moves.add(MoveList.Skullbash);
//		returnMap.put(31, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.IRON_DEFENSE);
		returnMap.put(34, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.RAIN_DANCE);
		returnMap.put(37, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.HYDDRO_PUMP);
		returnMap.put(40, moves);
		
		return returnMap;
	}
	

	@Override
	public boolean canEvolve(int level)
	{
		if (level >= 16) return true;
		else return false;
	}
	
	@Override
	/**
	 * Evolves the Pokemon into a new Wartortle.
	 */
	public Pokemon doEvolve(Pokemon pokemon, Items item)
	{
		Pokemon evolution = new Wartortle();
		evolution.generatePokemon(pokemon.level);
		evolution.heldItem = pokemon.heldItem;
		if (!pokemon.name.equals(pokemon.speciesName)) evolution.name = pokemon.name;
		evolution.shininess = pokemon.shininess;
		evolution.nature = pokemon.nature;
		evolution.iv = pokemon.iv;
		evolution.ev = pokemon.ev;
		
		evolution.calculateNewStats();
		evolution.resetStatStages();
		
		evolution.moves = pokemon.moves;
		evolution.gender = pokemon.gender;
		evolution.ability = pokemon.ability;
		evolution.currentHP = evolution.stats.get(0);
		evolution.status = Status.NORMAL;
		
		System.out.println("Congratulation! Your " + pokemon.name + " evolved into a " + evolution.speciesName + "!");
		
		return evolution;
	}
}
