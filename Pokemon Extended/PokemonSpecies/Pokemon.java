import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;

/**
 * Sets all of the Pokemon's values and manages Pokemon. Every PokemonSpecies (like Pikachu) extends this class.
 * @author Andi
 *
 */
public abstract class Pokemon 
{
	protected int dexNumber;
	protected ArrayList<Integer> base = new ArrayList<>(6);
	protected ArrayList<Integer> effortPoints = new ArrayList<>(6);
	protected ArrayList<String> formNames = new ArrayList<>(6);
	protected String kind, pokedex, wildItemCommon, wildItemUncommon, wildItemRare, name, speciesName;
	protected Abilities ability1, ability2, hiddenAbility1;
	protected Element type1, type2;
	protected PokemonColor color;
	protected EggGroups compat1, compat2;
	protected PokemonHabitat habitat;
	protected GenderRate genderRate;
	protected GrowthRate growthRate;
	protected int baseEXP, rareness, happiness, stepsToHatch, weight, height;
	
	protected Nature nature;
	protected boolean shininess;
	protected ArrayList<Integer> stats = new ArrayList<>(6);
	
	protected String gender;
	protected Abilities ability, currentAbility;
	protected Items heldItem;
	protected ArrayList<Integer> ev = new ArrayList<>(6);
	protected int evSum;
	protected ArrayList<Integer> iv = new ArrayList<>(6);
	protected int level;
	protected ArrayList<MoveList> moves = new ArrayList<>(5);
	protected Status status;
	protected int currentHP, toxicTimer, confusionTimer, sleepTimer, continualDamageTimer;
	protected boolean trapped, flinched, leechSeed, flashFire;
	protected MoveList trappingMove;
	protected Status confusion;
	protected boolean protect, previousTurnProtect;
	protected boolean isCharging, isRecharging;
	protected boolean hasPriority;
	protected int previousMove;
	
	protected ArrayList<Integer> pokemonStatusStages = new ArrayList<>(7);
	
	/**
	 * Sets the moves this Pokemon can learn through level up.
	 * @return Map<Integer, List<MoveList>>
	 */
	public abstract Map<Integer, List<MoveList>> LvlMoves();
	
	/**
	 * Randomly generates IVs for a Pokemon. (between 0-31)
	 */
	private void generateIVs()
	{
		Pokemon pokemon = this;
		Random rnd = new Random();
		pokemon.iv.add(rnd.nextInt(32));		
		pokemon.iv.add(rnd.nextInt(32));		
		pokemon.iv.add(rnd.nextInt(32));		
		pokemon.iv.add(rnd.nextInt(32));		
		pokemon.iv.add(rnd.nextInt(32));		
		pokemon.iv.add(rnd.nextInt(32));
	}
	
	/**
	 * Generates a new Pokemon.
	 * @param this
	 * @param level
	 * @return Pokemon
	 */
	public void generatePokemon(int level)
	{
		if (level > 100) this.level = 100;
		else this.level = level;
		this.heldItem = Items.NONE;
		
		generateNature();
		setShininess();
		generateIVs();
		fillEvs();
		setStats();
		resetStatStages();
		
		generateNature();
		calculateNature();
		setGender();
		setAbility();
		
		initMoves();
		
		this.currentHP = this.stats.get(0);
		this.status = Status.NORMAL;
		this.currentAbility = this.ability;
		this.leechSeed = false;
		this.flashFire = false;
		this.isCharging = false;
		this.isRecharging = false;
		this.previousMove = -1;
		this.hasPriority = false;		
	}
	
	/**
	 * Calculates the Pokemon's new stats. (Used for Level Up, Evolution, gaining EVs, etc.)
	 */
	public void calculateNewStats()
	{
		Pokemon pokemon = this;
		
		double x;
		
		x = ((2 * pokemon.base.get(0) + pokemon.iv.get(0) + (pokemon.ev.get(0) / 4)) * pokemon.level) / 100 + pokemon.level + 10;
		pokemon.stats.set(0, (int)x);
		
		for (int i = 1; i < 6; i++)
		{
			x = (((2 * pokemon.base.get(i) + pokemon.iv.get(i) + (pokemon.ev.get(i) / 4)) * pokemon.level) / 100) + 5;
			stats.set(i, (int)x);
		}
		
		calculateNature();
	}
	
	/**
	 * Fills the Pokemon's EVs with 0s.
	 */
	private void fillEvs()
	{
		Pokemon pokemon = this;
		pokemon.ev.add(0);
		pokemon.ev.add(0);
		pokemon.ev.add(0);
		pokemon.ev.add(0);
		pokemon.ev.add(0);
		pokemon.ev.add(0);
		
		evSum = 0;
	}
	
	/**
	 * Resets all Status Changes of a Pokemon. (like -1 Attack from a Growl for example)
	 */
	public void resetStatStages()
	{
		Pokemon pokemon = this;
		pokemon.pokemonStatusStages.add(0);
		pokemon.pokemonStatusStages.add(0);
		pokemon.pokemonStatusStages.add(0);
		pokemon.pokemonStatusStages.add(0);
		pokemon.pokemonStatusStages.add(0);
		pokemon.pokemonStatusStages.add(0);
		pokemon.pokemonStatusStages.add(0);
	}
	
	/**
	 * Prints out all the Status Changes of a Pokemon.
	 */
	public void showStatChanges()
	{
		Pokemon pokemon = this;
		for(int i = 0; i < 7; i++)
		{
			int stat = pokemon.pokemonStatusStages.get(i);
			if (stat != 0)
			{
				switch(i)
				{
					case 0: if(stat > 0) System.out.println("	Attack: +" + stat);
						else System.out.println("	Attack: " + stat);
						break;
					case 1: if(stat > 0) System.out.println("	Defence: +" + stat);
						else System.out.println("	Defence: " + stat);
						break;
					case 2: if(stat > 0) System.out.println("	Special Attack: +" + stat);
						else System.out.println("	Special Attack: " + stat);
						break;
					case 3: if(stat > 0) System.out.println("	Special Defence: +" + stat);
						else System.out.println("	Special Defence: ");
						break;
					case 4: if(stat > 0) System.out.println("	Speed: +" + stat);
						else System.out.println("	Speed " + stat);
						break;
					case 5: if(stat > 0) System.out.println("	Accuracy: +" + stat);
						else System.out.println("	Accuracy: " + stat);
						break;
					case 6: if(stat > 0) System.out.println("	Evasion: +" + stat);
						else System.out.println("	Evasion: " + stat);
						break;
					default:
						break;
				}
			}
		}
		if (pokemon.leechSeed)
			System.out.println("	Leech Seed");
	}
	
	/**
	 * Sets a Pokemon's initial stats.
	 */
	public void setStats()
	{
		Pokemon pokemon = this;
		double x;
		
		x = ((2 * pokemon.base.get(0) + pokemon.iv.get(0) + (Math.sqrt(pokemon.ev.get(0)) / 4)) * pokemon.level) / 100 + pokemon.level + 10;
		pokemon.stats.add((int)x);
		
		for (int i = 1; i < 6; i++)
		{
			x = (((2 * pokemon.base.get(i) + pokemon.iv.get(i) + (Math.sqrt(pokemon.ev.get(i)) / 4)) * pokemon.level) / 100) + 5;
			stats.add((int)x);
		}
	}
	
	/**
	 * Generates a Nature for the Pokemon.
	 */
	private void generateNature()
	{
		Pokemon pokemon = this;
		Random rnd = new Random();
		int n = rnd.nextInt(25);
		
		pokemon.nature = Nature.values()[n];
		
		
//		switch(n)
//		{
//		case 0: pokemon.nature = Nature.Hardy;
//			break;
//		case 1: pokemon.nature = Nature.Lonely;
//			break;
//		case 2: pokemon.nature = Nature.Brave;
//			break;
//		case 3: pokemon.nature = Nature.Adamant;
//			break;
//		case 4: pokemon.nature = Nature.Naughty;
//			break;
//		case 5: pokemon.nature = Nature.Bold;
//			break;
//		case 6: pokemon.nature = Nature.Docile;
//			break;
//		case 7: pokemon.nature = Nature.Relaxed;
//			break;
//		case 8: pokemon.nature = Nature.Impish;
//			break;
//		case 9: pokemon.nature = Nature.Lax;
//			break;
//		case 10: pokemon.nature = Nature.Timid;
//			break;
//		case 11: pokemon.nature = Nature.Hasty;
//			break;
//		case 12: pokemon.nature = Nature.Serious;
//			break;
//		case 13: pokemon.nature = Nature.Jolly;
//			break;
//		case 14: pokemon.nature = Nature.Naive;
//			break;
//		case 15: pokemon.nature = Nature.Modest;
//			break;
//		case 16: pokemon.nature = Nature.Mild;
//			break;
//		case 17: pokemon.nature = Nature.Quiet;
//			break;
//		case 18: pokemon.nature = Nature.Bashful;
//			break;
//		case 19: pokemon.nature = Nature.Rash;
//			break;
//		case 20: pokemon.nature = Nature.Calm;
//			break;
//		case 21: pokemon.nature = Nature.Gentle;
//			break;
//		case 22: pokemon.nature = Nature.Sassy;
//			break;
//		case 23: pokemon.nature = Nature.Careful;
//			break;
//		case 24: pokemon.nature = Nature.Quirky;
//			break;
//		default: pokemon.nature = Nature.Serious;
//		}
	}
	
	/**
	 * Calculates the changes that have to be made to the Pokemon's stats depending on its Nature.
	 */
	private void calculateNature()
	{		
		Pokemon pokemon = this;
		switch(pokemon.nature)
		{
		case LONELY: pokemon.stats.set(1, positiveNature(1));
			stats.set(2, negativeNature(2));
			break;
		case BRAVE: pokemon.stats.set(1, positiveNature(1));
			stats.set(5, negativeNature(5));
			break;
		case ADAMANT: stats.set(1, positiveNature(1));
			stats.set(3, negativeNature(3));
			break;
		case NAUGHTY: stats.set(1, positiveNature(1));
			stats.set(4, negativeNature(4));
			break;
		case BOLD: stats.set(2, positiveNature(2));
			stats.set(1, negativeNature(1));
			break;
		case RELAXED: stats.set(2, positiveNature(2));
			stats.set(5, negativeNature(5));
			break;
		case IMPISH: stats.set(2, positiveNature(2));
			stats.set(3, negativeNature(3));
			break;
		case LAX: stats.set(2, positiveNature(2));
			stats.set(4, negativeNature(4));
			break;
		case TIMID: stats.set(5, positiveNature(5));
			stats.set(1, negativeNature(1));
			break;
		case HASTY: stats.set(5, positiveNature(5));
			stats.set(2, negativeNature(2));
			break;
		case JOLLY: stats.set(5, positiveNature(5));
			stats.set(3, negativeNature(3));
			break;
		case NAIVE: stats.set(5, positiveNature(5));
			stats.set(4, negativeNature(4));
			break;
		case MODEST: stats.set(3, positiveNature(3));
			stats.set(1, negativeNature(1));
			break;
		case MILD: stats.set(3, positiveNature(3));
			stats.set(2, negativeNature(2));
			break;
		case QUIET: stats.set(3, positiveNature(3));
			stats.set(5, negativeNature(5));
			break;
		case RASH: stats.set(3, positiveNature(3));
			stats.set(4, negativeNature(4));
			break;
		case CALM: stats.set(4, positiveNature(4));
			stats.set(1, negativeNature(1));
			break;
		case GENTLE: stats.set(4, positiveNature(4));
			stats.set(2, negativeNature(2));
			break;
		case SASSY: stats.set(4, positiveNature(4));
			stats.set(5, negativeNature(5));
			break;
		case CAREFUL: stats.set(4, positiveNature(4));
			stats.set(3, negativeNature(3));
			break;
		default: 
		}
	}
	
	/**
	 * Returns the new Value of a Pokemon's stat that has a positive Nature.
	 * @param i
	 * @return int
	 */
	private int positiveNature(int i)
	{
		Pokemon pokemon = this;
		double x = ((((2 * pokemon.base.get(i) + pokemon.iv.get(i) + pokemon.ev.get(i) / 4.0) * pokemon.level) / 100) + 5) * 1.1;
		return (int)x;
	}
	
	/**
	 * Returns the new Value of a Pokemon's stat that has a negative Nature.
	 * @param i
	 * @return
	 */
	private int negativeNature(int i)
	{
		Pokemon pokemon = this;
		double x = ((((2 * pokemon.base.get(i) + pokemon.iv.get(i) + pokemon.ev.get(i) / 4.0) * pokemon.level) / 100) + 5) * 0.9;
		return (int)x;
	}
	
	/**
	 * Generates the Pokemon's shininess. Shiny Chance: 1/700
	 */
	private void setShininess()
	{
		Pokemon pokemon = this;
		Random rnd = new Random();
		int r = rnd.nextInt(700) + 1;
		if (r == 1) pokemon.shininess = true;
		else pokemon.shininess = false;
	}
	
	/**
	 * Generates the Pokemon's ability.
	 */
	private void setAbility()
	{
		Pokemon pokemon = this;
		Random rnd = new Random();
		double r = rnd.nextDouble();
		if (pokemon.ability2 == null && pokemon.hiddenAbility1 == null) pokemon.ability = pokemon.ability1;
		else if (pokemon.ability2 == null && pokemon.hiddenAbility1 != null)
		{

			if (r > 0.333333333) pokemon.ability = pokemon.ability1;
			else pokemon.ability = pokemon.hiddenAbility1;
		}
		else if (pokemon.ability2 != null & pokemon.hiddenAbility1 != null)
		{
			if (r > 0.666666666) pokemon.ability = pokemon.ability1;
			else if (r > 0.33333333) pokemon.ability = pokemon.ability2;
			else pokemon.ability = pokemon.hiddenAbility1;
		}
	}
	
	/**
	 * Generates the Pokemon's gender.
	 * @return
	 */
	private void setGender()
	{
		Pokemon pokemon = this;
		Random rnd = new Random();
		double r = rnd.nextDouble();
		if (pokemon.genderRate == GenderRate.ALWAYS_FEMALE) pokemon.gender = "female";
		else if (pokemon.genderRate == GenderRate.ALWAYS_MALE) pokemon.gender = "male";
		else if (pokemon.genderRate == GenderRate.GENDERLESS) pokemon.gender = "genderless";
		else if (pokemon.genderRate == GenderRate.FEMALE_25_PERCENT)
		{
			if (r > 0.25) pokemon.gender = "male";
			else pokemon.gender = "female";
		}
		else if (pokemon.genderRate == GenderRate.FEMALE_50_PERCENT)
		{
			if (r > 0.5) pokemon.gender = "male";
			else pokemon.gender = "female";
		}
		else if (pokemon.genderRate == GenderRate.FEMALE_75_PERCENT)
		{
			if (r > 0.75) pokemon.gender = "male";
			else pokemon.gender = "female";
		}
		else if (pokemon.genderRate == GenderRate.FEMALE_ONE_EIGHTH)
		{
			if (r > 0.125) pokemon.gender = "male";
			else pokemon.gender = "female";
		}
	}
	
	/**
	 * Prints out the Pokemon's stats. Including held item, Gender, Nickname, etc.
	 */
	public void getStats()
	{		
		System.out.println("\n==========================");
		if (!this.gender.contentEquals("genderless")) System.out.println(this.name + "(" + this.gender + ")" + " @ " + this.heldItem.getName());
		else  System.out.println(this.name + " @ " + this.heldItem.getName());
		System.out.println("Level: " + this.level);
		if (shininess == true) System.out.println("Shiny: yes");
		System.out.println("Ability: " + this.ability.getName());
		System.out.println(nature.getName() + " Nature");
		System.out.println("HP: 		" + stats.get(0) + " (" + ev.get(0) + "/" + iv.get(0) + ")");
		System.out.println("Attack: 	" + stats.get(1) + " (" + ev.get(1) + "/" + iv.get(1) + ")");
		System.out.println("Defence: 	" + stats.get(2) + " (" + ev.get(2) + "/" + iv.get(2) + ")");
		System.out.println("Sp. Attack: 	" + stats.get(3) + " (" + ev.get(3) + "/" + iv.get(3) + ")");
		System.out.println("Sp. Defence:	" + stats.get(4) + " (" + ev.get(4) + "/" + iv.get(4) + ")");
		System.out.println("Speed: 		" + stats.get(5) + " (" + ev.get(5) + "/" + iv.get(5) + ")");
		for(MoveList move : this.moves)
		{
			System.out.println("- " + move.getName());
		}
		System.out.println("==========================\n");
	}
	
	/**
	 * Sets a Pokemon's nickname.
	 * @param pokemon
	 * @param nick
	 */
	public void setNickname(Pokemon pokemon, String nick)
	{
		pokemon.name = nick;
	}
	
	/**
	 * Prints out a Pokemon's moves. They'll be shown with their index in brackets. [index]
	 * @param pokemon
	 */
	public void showMoves(Pokemon pokemon)
	{
		for(int i = 0; i < pokemon.moves.size(); i++)
		{
			System.out.println("[" + i + "] " + pokemon.moves.get(i).getName());
		}
	}
	
	/**
	 * Returns the damage of a move.
	 * @param moveChoice
	 * @return int
	 */
	public int getMoveDamage(int moveChoice)
	{
		return moves.get(moveChoice).getPower();
	}
	
	/**
	 * Returns the name of a move.
	 * @param moveChoice
	 * @return String
	 */
	public String getMoveName(int moveChoice)
	{
		return moves.get(moveChoice).getName();
	}
	
	/**
	 * Returns the accuracy of a move.
	 * @param moveChoice
	 * @return double
	 */
	public double getMoveAccuracy(int moveChoice)
	{
		return moves.get(moveChoice).getAccuracy();
	}
	
	/**
	 * Returns the MoveType of a move. (physical, special or status)
	 * @param moveChoice
	 * @return String
	 */
	public String getMoveType(int moveChoice)
	{
		return moves.get(moveChoice).getMoveType();
	}
	
	/**
	 * Returns the Element of a move.
	 * @param moveChoice
	 * @return Element
	 */
	public Element getMoveElement(int moveChoice)
	{
		return moves.get(moveChoice).getElement();
	}
	
	/**
	 * Sets a Pokemon's initial moves.
	 */
	private void initMoves()
	{
		Map<Integer, List<MoveList>> moves = LvlMoves();
		ArrayList<MoveList> returnList = new ArrayList<>();
		ArrayList<Integer> keySet = new ArrayList<>(moves.keySet());
		Collections.sort(keySet);
		
		for(Integer key : keySet)
		{			
			for (MoveList m : moves.get(key))
			{
				if(key > this.level)
				{
					break;
				}
				
				returnList.add(m);
				
				if (returnList.size() > 4)
				{
					returnList.remove(0);
				}
			}

		}
		
		this.moves = returnList;
	}
	
	/**
	 * Gives an item to a Pokemon.
	 * @param item
	 * @param player
	 */
	public void giveItem(Items item, Player player)
	{	
		if(this.heldItem.equals(Items.NONE))
		{
			this.heldItem = item;
			System.out.println(this.name + " is now holding " + item + ".");
		}
		else
		{
			@SuppressWarnings("resource")
			Scanner input = new Scanner(System.in);
			
			System.out.println(this.name + " is already holding " + this.heldItem + ".");
			System.out.println("Would you like to replace its held item?");
			
			String answer = input.nextLine().toLowerCase();
			if (answer.equals("yes"))
			{
				player.addItem(this.heldItem);
				this.heldItem = item;
				
				System.out.println(this.name + " is now holding " + item + ".");
			}
		}
	}
	
	/**
	 * Takes an item from a Pokemon.
	 * @param player
	 */
	public void takeItem(Player player)
	{
		if (this.heldItem.equals(Items.NONE)) System.out.println(this.name + " isn't holding anything!");
		else
		{
			System.out.println("Receiced the " + this.heldItem.getName() + " from " + this.name + ".");
			
			player.addItem(this.heldItem);
			this.heldItem = Items.NONE;
		}
	}
	
	/**
	 * Sets the new values after a Pokemon has leveled up.
	 * @return Pokemon
	 */
	public Pokemon levelUp()
	{	
		Pokemon pokemon = this;
		
		pokemon.calculateNewStats();

		pokemon.chekNewMoves();
		
		if(pokemon.canEvolve(pokemon.level))
		{
			System.out.println();
			pokemon = pokemon.evolve(null);
		}
		return pokemon;
	}
	
	/**
	 * Checks if a new move can be learned. (After level up or evolution)
	 */
	public void chekNewMoves()
	{
		boolean moveFound = false;
		Map<Integer, List<MoveList>> moves = LvlMoves();
		ArrayList<Integer> keySet = new ArrayList<>(moves.keySet());
		Collections.sort(keySet);
		
		for(Integer key : keySet)
		{
			if(key > this.level)
			{
				break;
			}
			else if (key == this.level)
			{
				for (MoveList m : moves.get(key))
				{
					for (int i = 0; i < this.moves.size(); i++)
					{
						if(m == this.moves.get(i)) moveFound = true;
					}
						
					if (moveFound == false)
					{
						int choice = -1;
						
						if(this.moves.size() < 4)
						{
							System.out.println(this.name + " learned " + moves.get(key) + "!");
							this.moves.add(m);
						}
						else
						{
							System.out.println(this.name + " is trying to learn " + m.getName() + "!");
							do
							{
								System.out.println("Which move would you like it to forget?");
								
								for(int j = 0; j <= this.moves.size(); j++)
								{
									if (j < this.moves.size()) System.out.println("	[" + j + "] " + this.moves.get(j).getName());
									else System.out.println("	[" + j + "] Cancel");
								}
								
								@SuppressWarnings("resource")
								Scanner input = new Scanner(System.in);
								try
								{
									choice = input.nextInt();
								}
								catch(Exception exc)
								{
									input.nextLine();
								}
							}
							while (choice > this.moves.size() || choice < 0);
								
							if (choice != this.moves.size())
							{
								System.out.println(this.name + " forgot " + this.moves.get(choice) + "!");
								this.moves.remove(choice);
									
								System.out.println(this.name + " learned " + m.getName() + "!");
								this.moves.add(m);
							}
							else
							{
								System.out.println(this.name + " stopped learning " + m.getName()+ "!");
							}
						}
					}
				}
			}
		}
	}
	
	/**
	 * Returns true if the Pokemon can evolve at this level.
	 * @param level
	 * @return boolean
	 */
	protected boolean canEvolve(int level) 
	{
		return false;
	}
	
	/**
	 * Returns true if the Pokemon can evolve by using this item.
	 * @param item
	 * @return boolean
	 */
	protected boolean canEvolve(Items item)
	{
		return false;
	}
	
	/**
	 * Asks the Player if they want to evolve their Pokemon.
	 * @param item
	 * @return Pokemon
	 */
	public Pokemon evolve(Items item)
	{
		Pokemon pokemon = this;
		@SuppressWarnings("resource")
		Scanner input = new Scanner(System.in);
		String answer;
		
		System.out.println(pokemon.name + " is evolving!");
		do
		{
			System.out.println("Would you like your " + pokemon.name + " to evolve?");
			answer = input.nextLine().toLowerCase();
		}
		while(!answer.equals("yes") && !answer.equals("no"));
		
		if(answer.equals("yes"))
		{
			pokemon = doEvolve(pokemon, item);
			pokemon.chekNewMoves();
			return pokemon;
		}
		else
		{
			System.out.println(pokemon.name + " stopped evolving!");		
		}
		return pokemon;
	}
	
	/**
	 * Evolves a Pokemon.
	 * @param pokemon
	 * @param item
	 * @return Pokemon
	 */
	protected Pokemon doEvolve(Pokemon pokemon, Items item)
	{
		return null;
	}
	
	/**
	 * Reduces the Pokemon's stat by a specific amount of stages. (Through moves like Growl)
	 * @param stat
	 * @param value - the amount of stages the stat will be reduced by
	 * @param plPokemon
	 */
	public void reduceStat(int stat, int value, boolean plPokemon)
	{		
		int a = this.pokemonStatusStages.get(stat);
		String statName = "";
		String text;
		
		switch(stat)
		{
			case 0: statName = "attack";
				break;
			case 1: statName = "defence";
				break;
			case 2: statName = "special attack";
				break;
			case 3: statName = "special defence";
				break;
			case 4: statName = "speed";
				break;
			case 5: statName = "accuracy";
				break;
			case 6: statName = "evasiveness";
				break;
			default: 
				break;
		}
		
		if (a > -6)
		{
			if (plPokemon == false) text = "The opposing " + this.name + ((this.name.endsWith("s")) ? "' " : "'s ") + statName;
			else text = this.name + ((this.name.endsWith("s")) ? "' " : "'s ") + statName;
			
			switch(value)
			{
				case 2: text += " harshly";
					break;
				case 3: text += " severely";
					break;
				default:
					break;
			}
			
			text += " fell!";
			
			System.out.println(text);
			
			if ((a - value) < -6) this.pokemonStatusStages.set(stat, -6);
			else 
			{
				int v = a - value;
				this.pokemonStatusStages.set(stat, v);
			}
		}
		else
		{
			System.out.println("Its " + statName + " won't go any lower!");
			this.pokemonStatusStages.set(stat, -6);
		}
	}
	
	/**
	 * Increases the Pokemon's stat by a specific amount of stages. (Through moves like Iron Defence)
	 * @param stat
	 * @param value - the amount of stages the stat will increase by
	 * @param plPokemon
	 */
	public void increaseStat(int stat, int value, boolean plPokemon)
	{		
		int a = this.pokemonStatusStages.get(stat);
		String statName = "";
		String text;
		
		switch(stat)
		{
			case 0: statName = "attack";
				break;
			case 1: statName = "defence";
				break;
			case 2: statName = "special attack";
				break;
			case 3: statName = "special defence";
				break;
			case 4: statName = "speed";
				break;
			case 5: statName = "accuracy";
				break;
			case 6: statName = "evasiveness";
				break;
			default: 
				break;
		}
		
		if (a > -6)
		{
			if (plPokemon == false) text = "The opposing " + this.name + ((this.name.endsWith("s")) ? "' " : "'s ") + statName;
			else text = this.name + ((this.name.endsWith("s")) ? "' " : "'s ") + statName;
			
			switch(value)
			{
				case 2: text += " sharply";
					break;
				case 3: text += " drastically";
					break;
				default:
					break;
			}
			
			text += " rose!";
			
			System.out.println(text);
			
			if ((a - value) > 6) this.pokemonStatusStages.set(stat, 6);
			else 
			{
				int v = a + value;
				this.pokemonStatusStages.set(stat, v);
			}
		}
		else
		{
			System.out.println("Its " + statName + " won't go any higher!");
			this.pokemonStatusStages.set(stat, 6);
		}
	}
}
