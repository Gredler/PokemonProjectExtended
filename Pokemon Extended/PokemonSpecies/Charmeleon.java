import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * The Pokemon Charmeleon.
 * @author Andi
 * @version 4.0
 */
public class Charmeleon extends Pokemon
{
	public Charmeleon()
	{
		dexNumber = 5;
		
		speciesName = "Charmeleon";
		name = speciesName;
		
		base.add(58);
		base.add(64);
		base.add(58);
		base.add(80);
		base.add(65);
		base.add(80);
		
		type1 = Element.FIRE;
		type2 = Element.NONE;
	
		genderRate = GenderRate.FEMALE_ONE_EIGHTH;
		growthRate = GrowthRate.PARABOLIC;
		baseEXP = 142;
		
		effortPoints.add(0);
		effortPoints.add(0);
		effortPoints.add(0);
		effortPoints.add(1);
		effortPoints.add(0);
		effortPoints.add(1);
		
		rareness = 45;
		happiness = 70;
		
		ability1 = Abilities.BLAZE;
		ability2 = null;
		hiddenAbility1 = Abilities.SOLAR_POWER;
		
		compat1 = EggGroups.MONSTER;
		compat2 = EggGroups.DRAGON;
		
		stepsToHatch = 5355;
		height = 11;
		weight = 190;
		
		color = PokemonColor.RED;
		kind = "Flame";
		habitat = PokemonHabitat.MOUNTAIN;
		pokedex = "Charmeleon mercilessly destroys its foes using its sharp claws. If it encounters a strong foe, it turns aggressive. In this excited state, the flame at the tip of its tail flares with a bluish white color.";
		
		wildItemCommon = null;
		wildItemUncommon = null;
		wildItemRare = null;
	}
	
	@Override
	public Map<Integer, List<MoveList>> LvlMoves() 
	{
		HashMap<Integer,List<MoveList>> returnMap = new HashMap<>();
		
		List<MoveList> moves = new LinkedList<>();
		moves.add(MoveList.SCRATCH);
		moves.add(MoveList.GROWL);
		moves.add(MoveList.EMBER);
		returnMap.put(1, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.SMOKESCREEN);
		returnMap.put(10, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.DRAGON_RAGE);
		returnMap.put(17, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.SCARY_FACE);
		returnMap.put(21, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.FIRE_FANG);
		returnMap.put(28, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.FLAME_BURST);
		returnMap.put(32, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.SLASH);
		returnMap.put(39, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.FIRE_SPIN);
		returnMap.put(50, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.INFERNO);
		returnMap.put(54, moves);
		
		return returnMap;
	}
	@Override
	public boolean canEvolve(int level)
	{
		if (level >= 36) return true;
		else return false;
	}
	
	@Override
	/**
	 * Evolves the Pokemon into a new Charizard.
	 */
	public Pokemon doEvolve(Pokemon pokemon, Items item)
	{
		Pokemon evolution = new Charizard();
		evolution.generatePokemon(pokemon.level);
		evolution.heldItem = pokemon.heldItem;
		if (!pokemon.name.equals(pokemon.speciesName)) evolution.name = pokemon.name;
		evolution.shininess = pokemon.shininess;
		evolution.nature = pokemon.nature;
		evolution.iv = pokemon.iv;
		evolution.ev = pokemon.ev;
		
		evolution.calculateNewStats();
		evolution.resetStatStages();
		
		evolution.moves = pokemon.moves;
		evolution.gender = pokemon.gender;
		evolution.ability = pokemon.ability;
		evolution.currentHP = evolution.stats.get(0);
		evolution.status = Status.NORMAL;
		
		System.out.println("Congratulation! Your " + pokemon.name + " evolved into a " + evolution.speciesName + "!");
		
		return evolution;
	}
}
