import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * The Pokemon Ivysaur.
 * @author Andi
 * @version 4.0
 */
public class Ivysaur extends Pokemon 
{
	public Ivysaur()
	{
		dexNumber = 2;
		
		speciesName = "Ivysaur";
		name = speciesName;
		
		base.add(60);
		base.add(62);
		base.add(63);
		base.add(80);
		base.add(80);
		base.add(60);
		
		type1 = Element.GRASS;
		type2 = Element.POISON;
	
		genderRate = GenderRate.FEMALE_ONE_EIGHTH;
		growthRate = GrowthRate.PARABOLIC;
		baseEXP = 142;
		
		effortPoints.add(0);
		effortPoints.add(0);
		effortPoints.add(0);
		effortPoints.add(1);
		effortPoints.add(1);
		effortPoints.add(0);
		
		rareness = 45;
		happiness = 70;
		
		ability1 = Abilities.OVERGROW;
		ability2 = null;
		hiddenAbility1 = Abilities.CHLOROPHYLL;
		
		compat1 = EggGroups.MONSTER;
		compat2 = EggGroups.PLANT;
		
		stepsToHatch = 5355;
		height = 10;
		weight = 130;
		
		color = PokemonColor.GREEN;
		kind = "Seed";
		habitat = PokemonHabitat.GRASSLAND;
		pokedex = "There is a bud on this Pok�mon's back. To support its weight, Ivysaur's legs and trunk grow thick and strong. If it starts spending more time lying in the sunlight, it�s a sign that the bud will bloom into a large flower soon.";
		
		wildItemCommon = null;
		wildItemUncommon = null;
		wildItemRare = null;
	}
	
	@Override
	public Map<Integer, List<MoveList>> LvlMoves() 
	{
		HashMap<Integer,List<MoveList>> returnMap = new HashMap<>();
		
		List<MoveList> moves = new LinkedList<>();
		moves.add(MoveList.TACKLE);
		moves.add(MoveList.LEECH_SEED);
		returnMap.put(1, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.GROWL);
		returnMap.put(3, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.VINE_WHIP);
		returnMap.put(9, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.POISON_POWDER);
		moves.add(MoveList.SLEEP_POWDER);
		returnMap.put(13, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.TAKE_DOWN);
		returnMap.put(15, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.RAZOR_LEAF);
		returnMap.put(20, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.SWEET_SCENT);
		returnMap.put(23, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.DOUBLE_EDGE);
		returnMap.put(31, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.WORRY_SEED);
		returnMap.put(36, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.SYNTHESIS);
		returnMap.put(39, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.SEED_BOMB);
		returnMap.put(44, moves);
		
		return returnMap;
	}
	

	@Override
	public boolean canEvolve(int level)
	{
		if (level >= 32) return true;
		else return false;
	}
	
	@Override
	/**
	 * Evolves the Pokemon into a new Venusaur.
	 */
	public Pokemon doEvolve(Pokemon pokemon, Items item)
	{
		Pokemon evolution = new Venusaur();
		evolution.generatePokemon(pokemon.level);
		evolution.heldItem = pokemon.heldItem;
		if (!pokemon.name.equals(pokemon.speciesName)) evolution.name = pokemon.name;
		evolution.shininess = pokemon.shininess;
		evolution.nature = pokemon.nature;
		evolution.iv = pokemon.iv;
		evolution.ev = pokemon.ev;
		
		evolution.calculateNewStats();
		evolution.resetStatStages();
		
		evolution.moves = pokemon.moves;
		evolution.gender = pokemon.gender;
		evolution.ability = pokemon.ability;
		evolution.currentHP = evolution.stats.get(0);
		evolution.status = Status.NORMAL;
		
		System.out.println("Congratulation! Your " + pokemon.name + " evolved into a " + evolution.speciesName + "!");
		
		return evolution;
	}
}
