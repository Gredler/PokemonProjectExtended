import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * The Pokemon Blastoise.
 * @author Andi
 * @version 3.0
 */
public class Blastoise extends Pokemon 
{	
	public Blastoise()
	{
		dexNumber = 9;
		
		speciesName = "Blastoise";
		name = speciesName;
		
		base.add(79);
		base.add(83);
		base.add(100);
		base.add(85);
		base.add(105);
		base.add(78);
		
		type1 = Element.WATER;
		type2 = Element.NONE;
	
		genderRate = GenderRate.FEMALE_ONE_EIGHTH;
		growthRate = GrowthRate.PARABOLIC;
		baseEXP = 239;
		
		effortPoints.add(0);
		effortPoints.add(0);
		effortPoints.add(0);
		effortPoints.add(0);
		effortPoints.add(3);
		effortPoints.add(0);
		
		rareness = 45;
		happiness = 70;
		
		ability1 = Abilities.TORRENT;
		ability2 = null;
		hiddenAbility1 = Abilities.RAIN_DISH;
		
		compat1 = EggGroups.MONSTER;
		compat2 = EggGroups.WATER_1;
		
		stepsToHatch = 5355;
		height = 16;
		weight = 855;
		
		color = PokemonColor.BLUE;
		kind = "Shellfish";
		habitat = PokemonHabitat.WATERS_EDGE;
		pokedex = "Blastoise has water spouts that protrude from its shell. The water spouts are very accurate. They can shoot bullets of water with enough accuracy to strike empty cans from a distance of over 160 feet.";
		
		wildItemCommon = null;
		wildItemUncommon = null;
		wildItemRare = null;
	}
	
	@Override
	public Map<Integer, List<MoveList>> LvlMoves() 
	{
		HashMap<Integer,List<MoveList>> returnMap = new HashMap<>();
		
		List<MoveList> moves = new LinkedList<>();
		moves.add(MoveList.FLASH_CANNON);
		moves.add(MoveList.TACKLE);
		moves.add(MoveList.WITHDRAW);
		moves.add(MoveList.WATER_GUN);
		returnMap.put(1, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.TAIL_WHIP);
		returnMap.put(4, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.WATER_GUN);
		returnMap.put(7, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.WITHDRAW);
		returnMap.put(10, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.BUBBLE);
		returnMap.put(13, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.BITE);
		returnMap.put(17, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.RAPID_SPIN);
		returnMap.put(21, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.PROTECT);
		returnMap.put(25, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.WATER_PULSE);
		returnMap.put(29, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.AQUA_TAIL);
		returnMap.put(33, moves);
		
//		moves = new LinkedList<>();
//		moves.add(MoveList.Skullbash);
//		returnMap.put(40, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.IRON_DEFENSE);
		returnMap.put(47, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.RAIN_DANCE);
		returnMap.put(54, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.HYDDRO_PUMP);
		returnMap.put(60, moves);

		return returnMap;
	}
}
