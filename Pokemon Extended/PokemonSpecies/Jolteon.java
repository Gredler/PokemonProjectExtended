import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * The Pokemon Jolteon.
 * @author Andi
 * @version 2.0
 */
public class Jolteon extends Pokemon 
{
	public Jolteon()
	{
		dexNumber = 135;
		
		speciesName = "Jolteon";
		name = speciesName;
		
		base.add(65);
		base.add(65);
		base.add(60);
		base.add(110);
		base.add(95);
		base.add(130);
		
		type1 = Element.ELECTRIC;
		type2 = Element.NONE;
	
		genderRate = GenderRate.FEMALE_25_PERCENT;
		growthRate = GrowthRate.MEDIUM;
		baseEXP = 184;
		
		effortPoints.add(0);
		effortPoints.add(0);
		effortPoints.add(0);
		effortPoints.add(0);
		effortPoints.add(0);
		effortPoints.add(2);
		
		rareness = 45;
		happiness = 70;
		
		ability1 = Abilities.VOLT_ABSORB;
		ability2 = null;
		hiddenAbility1 = Abilities.QUICK_FEET;
		
		compat1 = EggGroups.GROUND;
		compat2 = null;
		
		stepsToHatch = 9252;
		height = 8;
		weight = 245;
		
		color = PokemonColor.YELLOW;
		kind = "Lightning";
		habitat = PokemonHabitat.URBAN;
		pokedex = "Jolteon's cells generate a low level of electricity. This power is amplified by the static electricity of its fur, enabling the Pok�mon to drop thunderbolts. The bristling fur is made of electrically charged needles.";
		
		wildItemCommon = null;
		wildItemUncommon = null;
		wildItemRare = null;
	}

	@Override
	public Map<Integer, List<MoveList>> LvlMoves() 
	{
		HashMap<Integer,List<MoveList>> returnMap = new HashMap<>();
		
		List<MoveList> moves = new LinkedList<>();
		moves.add(MoveList.THUNDER_SHOCK);
//		moves.add(MoveList.HelpingHand);
		moves.add(MoveList.TACKLE);
		moves.add(MoveList.TAIL_WHIP);
		returnMap.put(1, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.SAND_ATTACK);
		returnMap.put(5, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.BABY_DOLL_EYES);
		returnMap.put(9, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.QUICK_ATTACK);
		returnMap.put(13, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.DOUBLE_KICK);
		returnMap.put(17, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.THUNDER_FANG);
		returnMap.put(20, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.PIN_MISSILE);
		returnMap.put(25, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.AGILITY);
		returnMap.put(29, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.THUNDER_WAVE);
		returnMap.put(33, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.DISCHARGE);
		returnMap.put(37, moves);
		
//		moves = new LinkedList<>();
//		moves.add(MoveList.LastResort);
//		returnMap.put(41, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.THUNDER);
		returnMap.put(45, moves);
		
		return returnMap;
	}
}
