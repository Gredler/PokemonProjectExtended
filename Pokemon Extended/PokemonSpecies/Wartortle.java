import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * The Pokemon Wartortle.
 * @author Andi
 * @version 4.0
 */
public class Wartortle extends Pokemon 
{	
	public Wartortle()
	{
		dexNumber = 8;
		
		speciesName = "Wartortle";
		name = speciesName;
		
		base.add(59);
		base.add(63);
		base.add(80);
		base.add(65);
		base.add(80);
		base.add(58);
		
		type1 = Element.WATER;
		type2 = Element.NONE;
	
		genderRate = GenderRate.FEMALE_ONE_EIGHTH;
		growthRate = GrowthRate.PARABOLIC;
		baseEXP = 142;
		
		effortPoints.add(0);
		effortPoints.add(0);
		effortPoints.add(1);
		effortPoints.add(0);
		effortPoints.add(1);
		effortPoints.add(0);
		
		rareness = 45;
		happiness = 70;
		
		ability1 = Abilities.TORRENT;
		ability2 = null;
		hiddenAbility1 = Abilities.RAIN_DISH;
		
		compat1 = EggGroups.MONSTER;
		compat2 = EggGroups.WATER_1;
		
		stepsToHatch = 5355;
		height = 10;
		weight = 225;
		
		color = PokemonColor.BLUE;
		kind = "Shellfish";
		habitat = PokemonHabitat.WATERS_EDGE;
		pokedex = "Its tail is large and covered with a rich, thick fur. The tail becomes increasingly deeper in color as Wartortle ages. The scratches on its shell are evidence of this Pok�mon�s toughness as a battler.";
		
		wildItemCommon = null;
		wildItemUncommon = null;
		wildItemRare = null;
	}
	
	@Override
	public Map<Integer, List<MoveList>> LvlMoves() 
	{
		HashMap<Integer,List<MoveList>> returnMap = new HashMap<>();
		
		List<MoveList> moves = new LinkedList<>();
		moves.add(MoveList.TACKLE);
		returnMap.put(1, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.TAIL_WHIP);
		returnMap.put(4, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.WATER_GUN);
		returnMap.put(7, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.WITHDRAW);
		returnMap.put(10, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.BUBBLE);
		returnMap.put(13, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.BITE);
		returnMap.put(17, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.RAPID_SPIN);
		returnMap.put(21, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.PROTECT);
		returnMap.put(25, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.WATER_PULSE);
		returnMap.put(29, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.AQUA_TAIL);
		returnMap.put(33, moves);
		
//		moves = new LinkedList<>();
//		moves.add(MoveList.Skullbash);
//		returnMap.put(37, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.IRON_DEFENSE);
		returnMap.put(41, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.RAIN_DANCE);
		returnMap.put(45, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.HYDDRO_PUMP);
		returnMap.put(49, moves);
		
		return returnMap;
	}
	

	@Override
	public boolean canEvolve(int level)
	{
		if (level >= 36) return true;
		else return false;
	}
	
	@Override
	public Pokemon doEvolve(Pokemon pokemon, Items item)
	{
		Pokemon evolution = new Blastoise();
		evolution.generatePokemon(pokemon.level);
		evolution.heldItem = pokemon.heldItem;
		if (!pokemon.name.equals(pokemon.speciesName)) evolution.name = pokemon.name;
		evolution.shininess = pokemon.shininess;
		evolution.nature = pokemon.nature;
		evolution.iv = pokemon.iv;
		evolution.ev = pokemon.ev;
		
		evolution.calculateNewStats();
		evolution.resetStatStages();
		
		evolution.moves = pokemon.moves;
		evolution.gender = pokemon.gender;
		evolution.ability = pokemon.ability;
		evolution.currentHP = evolution.stats.get(0);
		evolution.status = Status.NORMAL;
		
		System.out.println("Congratulation! Your " + pokemon.name + " evolved into a " + evolution.speciesName + "!");
		
		return evolution;
	}
}
