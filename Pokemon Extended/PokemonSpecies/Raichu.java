import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * The Pokemon Raichu.
 * @author Andi
 * @version 4.0
 */
public class Raichu extends Pokemon 
{
	public Raichu()
	{
		dexNumber = 26;
		
		speciesName = "Raichu";
		name = speciesName;
		
		base.add(60);
		base.add(90);
		base.add(55);
		base.add(90);
		base.add(80);
		base.add(110);
		
		type1 = Element.ELECTRIC;
		type2 = Element.NONE;
	
		genderRate = GenderRate.FEMALE_50_PERCENT;
		growthRate = GrowthRate.MEDIUM;
		baseEXP = 241;
		
		effortPoints.add(0);
		effortPoints.add(0);
		effortPoints.add(0);
		effortPoints.add(0);
		effortPoints.add(0);
		effortPoints.add(2);
		
		rareness = 75;
		happiness = 70;
		
		ability1 = Abilities.STATIC;
		ability2 = null;
		hiddenAbility1 = Abilities.LIGHTNING_ROD;
		
		compat1 = EggGroups.GROUND;
		compat2 = EggGroups.FAIRY;
		
		stepsToHatch = 2805;
		height = 8;
		weight = 300;
		
		color = PokemonColor.YELLOW;
		kind = "Mouse";
		habitat = PokemonHabitat.FOREST;
		pokedex = "It becomes aggressive when it has electricity stored up. At such times, even its Trainer has to take care to avoid being attacked.";
		
		wildItemCommon = null;
		wildItemUncommon = null;
		wildItemRare = null;
	}

	@Override
	public Map<Integer, List<MoveList>> LvlMoves() 
	{
		HashMap<Integer,List<MoveList>> returnMap = new HashMap<>();
		
		List<MoveList> moves = new LinkedList<>();
		moves.add(MoveList.THUNDER_SHOCK);
		moves.add(MoveList.TAIL_WHIP);
		returnMap.put(1, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.GROWL);
		returnMap.put(5, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.PLAY_NICE);
		returnMap.put(7, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.QUICK_ATTACK);
		moves.add(MoveList.THUNDER_WAVE);
		returnMap.put(10, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.ELECTRO_BALL);
		returnMap.put(13, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.FEINT);
		returnMap.put(21, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.DOUBLE_TEAM);
		returnMap.put(23, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.SPARK);
		returnMap.put(26, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.NUZZLE);
		returnMap.put(29, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.DISCHARGE);
		returnMap.put(34, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.SLAM);
		returnMap.put(37, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.THUNDERBOLT);
		returnMap.put(42, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.AGILITY);
		returnMap.put(45, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.WILD_CHARGE);
		returnMap.put(50, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.LIGHT_SCREEN);
		returnMap.put(53, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.THUNDER);
		returnMap.put(58, moves);
		
		return returnMap;
	}
}
