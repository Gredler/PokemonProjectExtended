import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * The Pokemon Vaporeon.
 * @author Andi
 * @version 2.0
 */
public class Vaporeon extends Pokemon 
{
	public Vaporeon()
	{
		dexNumber = 134;
		
		speciesName = "Vaporeon";
		name = speciesName;
		
		base.add(130);
		base.add(65);
		base.add(60);
		base.add(110);
		base.add(95);
		base.add(65);
		
		type1 = Element.WATER;
		type2 = Element.NONE;
	
		genderRate = GenderRate.FEMALE_25_PERCENT;
		growthRate = GrowthRate.MEDIUM;
		baseEXP = 184;
		
		effortPoints.add(2);
		effortPoints.add(0);
		effortPoints.add(0);
		effortPoints.add(0);
		effortPoints.add(0);
		effortPoints.add(0);
		
		rareness = 45;
		happiness = 70;
		
		ability1 = Abilities.WATER_ABSORB;
		ability2 = null;
		hiddenAbility1 = Abilities.HYDRATION;
		
		compat1 = EggGroups.GROUND;
		compat2 = null;
		
		stepsToHatch = 9252;
		height = 10;
		weight = 290;
		
		color = PokemonColor.BLUE;
		kind = "Bubble Jet";
		habitat = PokemonHabitat.URBAN;
		pokedex = "Vaporeon underwent a spontaneous mutation and grew fins and gills that allow it to live underwater. This Pok�mon has the ability to freely control water.";
		
		wildItemCommon = null;
		wildItemUncommon = null;
		wildItemRare = null;
	}

	@Override
	public Map<Integer, List<MoveList>> LvlMoves() 
	{
		HashMap<Integer,List<MoveList>> returnMap = new HashMap<>();
		
		List<MoveList> moves = new LinkedList<>();
		moves.add(MoveList.WATER_GUN);
//		moves.add(MoveList.HelpingHand);
		moves.add(MoveList.TACKLE);
		moves.add(MoveList.TAIL_WHIP);
		returnMap.put(1, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.SAND_ATTACK);
		returnMap.put(5, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.BABY_DOLL_EYES);
		returnMap.put(9, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.QUICK_ATTACK);
		returnMap.put(13, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.WATER_PULSE);
		returnMap.put(17, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.AURORA_BEAM);
		returnMap.put(20, moves);
		
//		moves = new LinkedList<>();
//		moves.add(MoveList.AquaRing);
//		returnMap.put(25, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.ACID_ARMOR);
		returnMap.put(29, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.HAZE);
		returnMap.put(33, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.MUDDY_WATER);
		returnMap.put(37, moves);
		
//		moves = new LinkedList<>();
//		moves.add(MoveList.LastResort);
//		returnMap.put(41, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.HYDDRO_PUMP);
		returnMap.put(45, moves);
		
		return returnMap;
	}
}
