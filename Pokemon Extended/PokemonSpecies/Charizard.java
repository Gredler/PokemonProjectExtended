import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * The Pokemon Charizard.
 * @author Andi
 * @version 3.0
 */
public class Charizard extends Pokemon
{	
	public Charizard()
	{
		dexNumber = 6;
		
		speciesName = "Charizard";
		name = speciesName;
		
		base.add(78);
		base.add(84);
		base.add(78);
		base.add(109);
		base.add(85);
		base.add(100);
		
		type1 = Element.FIRE;
		type2 = Element.FLYING;
	
		genderRate = GenderRate.FEMALE_ONE_EIGHTH;
		growthRate = GrowthRate.PARABOLIC;
		baseEXP = 240;
		
		effortPoints.add(0);
		effortPoints.add(0);
		effortPoints.add(0);
		effortPoints.add(3);
		effortPoints.add(0);
		effortPoints.add(0);
		
		rareness = 45;
		happiness = 70;
		
		ability1 = Abilities.BLAZE;
		ability2 = null;
		hiddenAbility1 = Abilities.SOLAR_POWER;
		
		compat1 = EggGroups.MONSTER;
		compat2 = EggGroups.DRAGON;
		
		stepsToHatch = 5355;
		height = 17;
		weight = 905;
		
		color = PokemonColor.RED;
		kind = "Flame";
		habitat = PokemonHabitat.MOUNTAIN;
		pokedex = "Charizard flies around the sky in search of powerful opponents. It breathes fire of such great heat that it melts anything. However, it never turns its fiery breath on any opponent weaker than itself.";		
		
		wildItemCommon = null;
		wildItemUncommon = null;
		wildItemRare = null;
	}
	
	@Override
	public Map<Integer, List<MoveList>> LvlMoves() 
	{
		HashMap<Integer,List<MoveList>> returnMap = new HashMap<>();
		
		List<MoveList> moves = new LinkedList<>();
		moves.add(MoveList.WING_ATTACK);
		moves.add(MoveList.FLARE_BLITZ);
		moves.add(MoveList.HEAT_WAVE);
		moves.add(MoveList.DRAGON_CLAW);
		moves.add(MoveList.AIR_SLASH);
		moves.add(MoveList.SCRATCH);
		moves.add(MoveList.GROWL);
		moves.add(MoveList.EMBER);
		returnMap.put(1, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.SMOKESCREEN);
		returnMap.put(10, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.DRAGON_RAGE);
		returnMap.put(17, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.SCARY_FACE);
		returnMap.put(21, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.FIRE_FANG);
		returnMap.put(28, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.FLAME_BURST);
		returnMap.put(32, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.SLASH);
		returnMap.put(41, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.FIRE_SPIN);
		returnMap.put(56, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.INFERNO);
		returnMap.put(62, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.HEAT_WAVE);
		returnMap.put(71, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.FLARE_BLITZ);
		returnMap.put(77, moves);
		
		return returnMap;
	}
}
