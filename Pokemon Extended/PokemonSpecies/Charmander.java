import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * The Pokemon Charmander.
 * @author Andi
 * @version 8.0
 */
public class Charmander extends Pokemon 
{
	public Charmander()
	{
		dexNumber = 4;
		
		speciesName = "Charmander";
		name = speciesName;
		
		base.add(39);
		base.add(52);
		base.add(43);
		base.add(60);
		base.add(50);
		base.add(65);
		
		type1 = Element.FIRE;
		type2 = Element.NONE;
	
		genderRate = GenderRate.FEMALE_ONE_EIGHTH;
		growthRate = GrowthRate.PARABOLIC;
		baseEXP = 62;
		
		effortPoints.add(0);
		effortPoints.add(0);
		effortPoints.add(0);
		effortPoints.add(0);
		effortPoints.add(0);
		effortPoints.add(1);
		
		rareness = 45;
		happiness = 70;
		
		ability1 = Abilities.BLAZE;
		ability2 = null;
		hiddenAbility1 = Abilities.SOLAR_POWER;
		
		compat1 = EggGroups.MONSTER;
		compat2 = EggGroups.DRAGON;
		
		stepsToHatch = 5355;
		height = 6;
		weight = 85;
		
		color = PokemonColor.RED;
		kind = "Lizard";
		habitat = PokemonHabitat.MOUNTAIN;
		pokedex = "The flame that burns at the tip of its tail is an indication of its emotions. The flame wavers when Charmander is enjoying itself. If the Pok�mon becomes enraged, the flame burns fiercely.";
		
		wildItemCommon = null;
		wildItemUncommon = null;
		wildItemRare = null;
	}
	
	@Override
	public Map<Integer, List<MoveList>> LvlMoves() 
	{
		HashMap<Integer,List<MoveList>> returnMap = new HashMap<>();
		
		List<MoveList> moves = new LinkedList<>();
		moves.add(MoveList.SCRATCH);
		moves.add(MoveList.GROWL);
		returnMap.put(1, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.EMBER);
		returnMap.put(7, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.SMOKESCREEN);
		returnMap.put(10, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.DRAGON_RAGE);
		returnMap.put(16, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.SCARY_FACE);
		returnMap.put(19, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.FIRE_FANG);
		returnMap.put(25, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.FLAME_BURST);
		returnMap.put(25, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.SLASH);
		returnMap.put(34, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.FIRE_SPIN);
		returnMap.put(43, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.INFERNO);
		returnMap.put(46, moves);
		
		return returnMap;
	}
	
	@Override
	public boolean canEvolve(int level)
	{
		if (level >= 16) return true;
		else return false;
	}
	
	@Override
	/**
	 * Evolves the Pokemon into a new Charmeleon.
	 */
	public Pokemon doEvolve(Pokemon pokemon, Items item)
	{
		Pokemon evolution = new Charmeleon();
		evolution.generatePokemon(pokemon.level);
		evolution.heldItem = pokemon.heldItem;
		if (!pokemon.name.equals(pokemon.speciesName)) evolution.name = pokemon.name;
		evolution.shininess = pokemon.shininess;
		evolution.nature = pokemon.nature;
		evolution.iv = pokemon.iv;
		evolution.ev = pokemon.ev;
		
		evolution.calculateNewStats();
		evolution.resetStatStages();
		
		evolution.moves = pokemon.moves;
		evolution.gender = pokemon.gender;
		evolution.ability = pokemon.ability;
		evolution.currentHP = evolution.stats.get(0);
		evolution.status = Status.NORMAL;
		
		System.out.println("Congratulation! Your " + pokemon.name + " evolved into a " + evolution.speciesName + "!");
		
		return evolution;
	}

}
