import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * The Pokemon Eevee
 * @author Andi
 * @version 2.0
 */
public class Eevee extends Pokemon 
{
	public Eevee()
	{
		dexNumber = 133;
		
		speciesName = "Eevee";
		name = speciesName;
		
		base.add(55);
		base.add(55);
		base.add(50);
		base.add(45);
		base.add(65);
		base.add(55);
		
		type1 = Element.NORMAL;
		type2 = Element.NONE;
	
		genderRate = GenderRate.FEMALE_25_PERCENT;
		growthRate = GrowthRate.MEDIUM;
		baseEXP = 65;
		
		effortPoints.add(0);
		effortPoints.add(0);
		effortPoints.add(0);
		effortPoints.add(0);
		effortPoints.add(1);
		effortPoints.add(0);
		
		rareness = 45;
		happiness = 70;
		
		ability1 = Abilities.RUN_AWAY;
		ability2 = Abilities.ADAPTABILITY;
		hiddenAbility1 = Abilities.ANTICIPATION;
		
		compat1 = EggGroups.GROUND;
		compat2 = null;
		
		stepsToHatch = 9252;
		height = 3;
		weight = 65;
		
		color = PokemonColor.BROWN;
		kind = "Evolution";
		habitat = PokemonHabitat.URBAN;
		pokedex = "Eevee has an unstable genetic makeup that suddenly mutates due to the environment in which it lives. Radiation from various stones causes this Pok�mon to evolve.";
		
		wildItemCommon = null;
		wildItemUncommon = null;
		wildItemRare = null;
	}

	@Override
	public Map<Integer, List<MoveList>> LvlMoves() 
	{
		HashMap<Integer,List<MoveList>> returnMap = new HashMap<>();
		
		List<MoveList> moves = new LinkedList<>();
		moves.add(MoveList.COVET);
//		moves.add(MoveList.HelpingHand);
		moves.add(MoveList.GROWL);
		moves.add(MoveList.TACKLE);
		moves.add(MoveList.TAIL_WHIP);
		returnMap.put(1, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.SAND_ATTACK);
		returnMap.put(5, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.BABY_DOLL_EYES);
		returnMap.put(9, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.QUICK_ATTACK);
		returnMap.put(13, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.BITE);
		moves.add(MoveList.SWIFT);
		returnMap.put(17, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.REFRESH);
		returnMap.put(20, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.TAKE_DOWN);
		returnMap.put(25, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.CHARM);
		returnMap.put(29, moves);
		
//		moves = new LinkedList<>();
//		moves.add(MoveList.BatonBass);
//		returnMap.put(33, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.DOUBLE_EDGE);
		returnMap.put(37, moves);
		
//		moves = new LinkedList<>();
//		moves.add(MoveList.LastResort);
//		returnMap.put(41, moves);
		
//		moves = new LinkedList<>();
//		moves.add(MoveList.TrumpCard);
//		returnMap.put(45, moves);
		
		return returnMap;
	}
	
	@Override
	public boolean canEvolve(Items item)
	{
		if (item == Items.THUNDERSTONE) return true;
		else if (item == Items.FIRESTONE) return true;
		else if (item == Items.WATERSTONE) return true;
		else return false;
	}
	
	@Override
	/**
	 * Evolves the Pokemon depending on the item.
	 */
	public Pokemon doEvolve(Pokemon pokemon, Items item)
	{
		Pokemon evolution;
		
		if (item == Items.THUNDERSTONE) evolution = new Jolteon();
		else if (item == Items.WATERSTONE) evolution = new Vaporeon();
		else if (item == Items.FIRESTONE) evolution = new Flareon();
		else evolution = null;
		
		evolution.generatePokemon(pokemon.level);
		evolution.heldItem = pokemon.heldItem;
		if (!pokemon.name.equals(pokemon.speciesName)) evolution.name = pokemon.name;
		evolution.shininess = pokemon.shininess;
		evolution.nature = pokemon.nature;
		evolution.iv = pokemon.iv;
		evolution.ev = pokemon.ev;
		
		evolution.calculateNewStats();
		evolution.resetStatStages();
		
		evolution.moves = pokemon.moves;
		evolution.gender = pokemon.gender;
		
		if (pokemon.ability.equals(pokemon.ability1)) evolution.ability = evolution.ability1;
		else if (pokemon.ability.equals(pokemon.ability2)) evolution.ability = evolution.ability2;
		else evolution.ability = evolution.hiddenAbility1;
		
		evolution.currentHP = evolution.stats.get(0);
		evolution.status = Status.NORMAL;
		
		if (evolution.name.startsWith("A") || evolution.name.startsWith("E") || evolution.name.startsWith("I") || evolution.name.startsWith("O") || evolution.name.startsWith("U"))
			System.out.println("Congratulation! Your " + pokemon.name + " evolved into an " + evolution.speciesName + "!");
		else
			System.out.println("Congratulation! Your " + pokemon.name + " evolved into a " + evolution.speciesName + "!");
		return evolution;
	}

}
