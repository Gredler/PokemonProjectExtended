import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * The Pokemon Bulbasaur.
 * @author Andi
 * @version 4.0
 */
public class Bulbasaur extends Pokemon 
{
	public Bulbasaur()
	{
		dexNumber = 1;
		
		speciesName = "Bulbasaur";
		name = speciesName;
		
		base.add(45);
		base.add(49);
		base.add(49);
		base.add(65);
		base.add(65);
		base.add(45);
		
		type1 = Element.GRASS;
		type2 = Element.POISON;
	
		genderRate = GenderRate.FEMALE_ONE_EIGHTH;
		growthRate = GrowthRate.PARABOLIC;
		baseEXP = 64;
		
		effortPoints.add(0);
		effortPoints.add(0);
		effortPoints.add(0);
		effortPoints.add(1);
		effortPoints.add(0);
		effortPoints.add(0);
		
		rareness = 45;
		happiness = 70;
		
		ability1 = Abilities.OVERGROW;
		ability2 = null;
		hiddenAbility1 = Abilities.CHLOROPHYLL;
		
		compat1 = EggGroups.MONSTER;
		compat2 = EggGroups.PLANT;
		
		stepsToHatch = 5355;
		height = 7;
		weight = 69;
		
		color = PokemonColor.GREEN;
		kind = "Seed";
		habitat = PokemonHabitat.GRASSLAND;
		pokedex = "Bulbasaur can be seen napping in bright sunlight. There is a seed on its back. By soaking up the sun�s rays, the seed grows progressively larger.";
		
		wildItemCommon = null;
		wildItemUncommon = null;
		wildItemRare = null;
	}
	
	@Override
	public Map<Integer, List<MoveList>> LvlMoves() 
	{
		HashMap<Integer,List<MoveList>> returnMap = new HashMap<>();
		
		List<MoveList> moves = new LinkedList<>();
		moves.add(MoveList.TACKLE);
		returnMap.put(1, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.GROWL);
		returnMap.put(3, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.VINE_WHIP);
		returnMap.put(7, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.POISON_POWDER);
		moves.add(MoveList.SLEEP_POWDER);
		returnMap.put(13, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.TAKE_DOWN);
		returnMap.put(15, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.RAZOR_LEAF);
		returnMap.put(19, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.SWEET_SCENT);
		returnMap.put(21, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.DOUBLE_EDGE);
		returnMap.put(27, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.WORRY_SEED);
		returnMap.put(31, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.SYNTHESIS);
		returnMap.put(33, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.SEED_BOMB);
		returnMap.put(37, moves);
		
		return returnMap;
	}
	

	@Override
	public boolean canEvolve(int level)
	{
		if (level >= 16) return true;
		else return false;
	}
	
	@Override
	/**
	 * Evolves the pokemon into a new Ivysaur.
	 */
	public Pokemon doEvolve(Pokemon pokemon, Items item)
	{
		Pokemon evolution = new Ivysaur();
		evolution.generatePokemon(pokemon.level);
		evolution.heldItem = pokemon.heldItem;
		if (!pokemon.name.equals(pokemon.speciesName)) evolution.name = pokemon.name;
		evolution.shininess = pokemon.shininess;
		evolution.nature = pokemon.nature;
		evolution.iv = pokemon.iv;
		evolution.ev = pokemon.ev;
		
		evolution.calculateNewStats();
		evolution.resetStatStages();
		
		evolution.moves = pokemon.moves;
		evolution.gender = pokemon.gender;
		evolution.ability = pokemon.ability;
		evolution.currentHP = evolution.stats.get(0);
		evolution.status = Status.NORMAL;
		
		System.out.println("Congratulation! Your " + pokemon.name + " evolved into an " + evolution.speciesName + "!");
		
		return evolution;
	}
}
