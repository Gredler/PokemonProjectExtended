import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * The Pokemon Flareon.
 * @author Andi
 * @version 2.0
 */
public class Flareon extends Pokemon 
{
	public Flareon()
	{
		dexNumber = 136;
		
		speciesName = "Flareon";
		name = speciesName;
		
		base.add(60);
		base.add(130);
		base.add(60);
		base.add(95);
		base.add(110);
		base.add(65);
		
		type1 = Element.FIRE;
		type2 = Element.NONE;
	
		genderRate = GenderRate.FEMALE_25_PERCENT;
		growthRate = GrowthRate.MEDIUM;
		baseEXP = 184;
		
		effortPoints.add(0);
		effortPoints.add(2);
		effortPoints.add(0);
		effortPoints.add(0);
		effortPoints.add(0);
		effortPoints.add(0);
		
		rareness = 45;
		happiness = 70;
		
		ability1 = Abilities.FLASH_FIRE;
		ability2 = null;
		hiddenAbility1 = Abilities.GUTS;
		
		compat1 = EggGroups.GROUND;
		compat2 = null;
		
		stepsToHatch = 9252;
		height = 9;
		weight = 250;
		
		color = PokemonColor.RED;
		kind = "Flame";
		habitat = PokemonHabitat.URBAN;
		pokedex = "Flareon's fluffy fur has a functional purpose-it releases heat into the air so that its body does not get excessively hot. This Pok�mon's body temperature can rise to a maximum of 1,650 degrees Fahrenheit.";
		
		wildItemCommon = null;
		wildItemUncommon = null;
		wildItemRare = null;
	}

	@Override
	public Map<Integer, List<MoveList>> LvlMoves() 
	{
		HashMap<Integer,List<MoveList>> returnMap = new HashMap<>();
		
		List<MoveList> moves = new LinkedList<>();
		moves.add(MoveList.EMBER);
//		moves.add(MoveList.HelpingHand);
		moves.add(MoveList.TACKLE);
		moves.add(MoveList.TAIL_WHIP);
		returnMap.put(1, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.SAND_ATTACK);
		returnMap.put(5, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.BABY_DOLL_EYES);
		returnMap.put(9, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.QUICK_ATTACK);
		returnMap.put(13, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.BITE);
		returnMap.put(17, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.FIRE_FANG);
		returnMap.put(20, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.FIRE_SPIN);
		returnMap.put(25, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.SCARY_FACE);
		returnMap.put(29, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.SMOG);
		returnMap.put(33, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.LAVA_PLUME);
		returnMap.put(37, moves);
		
//		moves = new LinkedList<>();
//		moves.add(MoveList.LastResort);
//		returnMap.put(41, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.FLARE_BLITZ);
		returnMap.put(45, moves);
		
		return returnMap;
	}
}
