import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * The Pokemon Pikachu.
 * @author Andi
 * @version 7.0
 */
public class Pikachu extends Pokemon 
{	
	public Pikachu()
	{
		dexNumber = 25;
		
		speciesName = "Pikachu";
		name = speciesName;
		
		base.add(35);
		base.add(55);
		base.add(40);
		base.add(50);
		base.add(50);
		base.add(90);
		
		type1 = Element.ELECTRIC;
		type2 = Element.NONE;
	
		genderRate = GenderRate.FEMALE_50_PERCENT;
		growthRate = GrowthRate.MEDIUM;
		baseEXP = 105;
		
		effortPoints.add(0);
		effortPoints.add(0);
		effortPoints.add(0);
		effortPoints.add(0);
		effortPoints.add(0);
		effortPoints.add(2);
		
		rareness = 190;
		happiness = 70;
		
		ability1 = Abilities.STATIC;
		ability2 = null;
		hiddenAbility1 = Abilities.LIGHTNING_ROD;
		
		compat1 = EggGroups.GROUND;
		compat2 = EggGroups.FAIRY;
		
		stepsToHatch = 2805;
		height = 4;
		weight = 60;
		
		color = PokemonColor.YELLOW;
		kind = "Mouse";
		habitat = PokemonHabitat.FOREST;
		pokedex = "Whenever Pikachu comes across something new, it blasts it with a jolt of electricity. If you come across a blackened berry, it's evidence that this Pok�mon mistook the intensity of its charge.";
		
		wildItemCommon = null;
		wildItemUncommon = null;
		wildItemRare = null;
	}

	@Override
	public Map<Integer, List<MoveList>> LvlMoves() 
	{
		HashMap<Integer,List<MoveList>> returnMap = new HashMap<>();
		
		List<MoveList> moves = new LinkedList<>();
		moves.add(MoveList.THUNDER_SHOCK);
		moves.add(MoveList.TAIL_WHIP);
		returnMap.put(1, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.GROWL);
		returnMap.put(5, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.PLAY_NICE);
		returnMap.put(7, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.QUICK_ATTACK);
		moves.add(MoveList.THUNDER_WAVE);
		returnMap.put(10, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.ELECTRO_BALL);
		returnMap.put(13, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.FEINT);
		returnMap.put(21, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.DOUBLE_TEAM);
		returnMap.put(23, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.SPARK);
		returnMap.put(26, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.NUZZLE);
		returnMap.put(29, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.DISCHARGE);
		returnMap.put(34, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.SLAM);
		returnMap.put(37, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.THUNDERBOLT);
		returnMap.put(42, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.AGILITY);
		returnMap.put(45, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.WILD_CHARGE);
		returnMap.put(50, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.LIGHT_SCREEN);
		returnMap.put(53, moves);
		
		moves = new LinkedList<>();
		moves.add(MoveList.THUNDER);
		returnMap.put(58, moves);
		
		
		/*
		//Ein Entry entspricht einem Eintrag in der Map
		//Jeder Eintrag ist gleich dem definierten generischen Wert <A,B>
		//und der Eintrag MUSS ebenfalls den gleiche Haben <A,B>
		//die Werte des Eintrags k�nnen mittels .getKey() und .getValue()
		//gelesen werden
		for(Entry<Integer,MoveList> entry : returnMap.entrySet())
		{
		}
		//gehe alle Schl�ssel der HashMap durch
		for(Integer key : returnMap.keySet())
		{
			//der Wert (in diesem Fall MoveList) kann mittels get(Schl�ssel) geholt werden
			returnMap.get(key);
		}
		*/
		
		return returnMap;
	}
	
	@Override
	public boolean canEvolve(Items item)
	{
		if (item == Items.THUNDERSTONE) return true;
		else return false;
	}
	
	@Override
	/**
	 * Evolves the Pokemon into a new Raichu.
	 */
	public Pokemon doEvolve(Pokemon pokemon, Items item)
	{
		Pokemon evolution = new Raichu();
		evolution.generatePokemon(pokemon.level);
		evolution.heldItem = pokemon.heldItem;
		if (!pokemon.name.equals(pokemon.speciesName)) evolution.name = pokemon.name;
		evolution.shininess = pokemon.shininess;
		evolution.nature = pokemon.nature;
		evolution.iv = pokemon.iv;
		evolution.ev = pokemon.ev;
		
		evolution.calculateNewStats();
		evolution.resetStatStages();
		
		evolution.moves = pokemon.moves;
		evolution.gender = pokemon.gender;
		evolution.ability = pokemon.ability;
		evolution.currentHP = evolution.stats.get(0);
		evolution.status = Status.NORMAL;
		
		System.out.println("Congratulation! Your " + pokemon.name + " evolved into a " + evolution.speciesName + "!");
		
		return evolution;
	}

}
